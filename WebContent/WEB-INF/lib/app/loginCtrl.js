var loginCtrl = function ($scope, $http, $location, $window, $timeout, $cookies, $uibModal, $uibModalInstance, jwtHelper, config ,$crypto) {

    $scope.login = function(){
    
        $(window).resize(function() {
              $scope.$apply(function() {
                $scope.windowWidth = $( window ).width();
                if($scope.windowWidth < 767){
                    $uibModalInstance.close();
                }
                if($scope.windowWidth > 767 ){
                }
              });
            });
        if($window.innerWidth > 460){
        $scope.showlogin = true;
        $scope.inputType = 'password';
        window.altkey = false;
      
        /*AWSCognito.config.region = config.reg;
        AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId: config.iid
        });
       
        var poolData = { UserPoolId : config.uid,
                ClientId : config.cid
            };
        var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
        
        var cognitoUser = userPool.getCurrentUser();
        
        if (cognitoUser != null && localStorage.getItem("email") != null) {
            
              cognitoUser.getSession(function(err, session) {
                    if (err) {
                        localStorage.clear();
                        $window.location.href = '#page';
                    }else{
                        $scope.loading = true;
                        $window.location.href = '#home';
                        $uibModalInstance.cancel();
                        }
              });
            
        } else {
            
            
        }*/
        
        }else{
            $scope.showlogin = false;
        }
      
      
    };
   
    $scope.login();
    
    $scope.myFunct = function(keyEvent) {
          if (keyEvent.which === 13)
              {
                  $scope.logon();
              }
        }
    
    $scope.logon = function(){
       
         $scope.error = false;
         
         $scope.error1 = false;
         $scope.error2 = false;
         if( $scope.email == null || $scope.pwd == null || $scope.email == '' || $scope.pwd == '' || $scope.pwd.length < 8){
          
             if($scope.email == null || $scope.email == ''){
                 $scope.error = 1;
                 
             }
             if($scope.pwd == null || $scope.pwd == '' ){
                 $scope.error1 = 2;
             }else if( $scope.pwd.length < 8)
                 {
                 $scope.error2 = 2;
                 }
            
        }
        else{
          
            $scope.loading = true;
        if(localStorage.getItem("isUser") == undefined || localStorage.getItem("isUser") == null || localStorage.getItem("isUser") == false){
       
            localStorage.setItem("isUser", true);
            AWSCognito.config.region = config.reg;
            AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
                IdentityPoolId: config.iid
            });
           
            var poolData = { UserPoolId : config.uid,
                    ClientId : config.cid
                };
            var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
       
            var cognitoUser = userPool.getCurrentUser();
                if(localStorage.getItem("740a2y1e") != null)
                {
                 
                    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
                    $scope.decry=JSON.parse(decrypted);
                  
                    if (cognitoUser != null && $scope.decry.email != null) {
                        
                        cognitoUser.getSession(function(err, session) {
                              if (err) {
                                
                                  $scope.seucrelogin();
                              }else{
                                  $scope.loading = true;
                                  $window.location.href = '#dashboard';
                                  $uibModalInstance.close();
                                  }
                        });
                      
                  }else
                      {
                          $scope.seucrelogin();
                      }
                }else {
              
                    $scope.seucrelogin();
                
            }
        }else{
            
            $timeout( function(){$scope.alreadyUser() }, 3000); 
        }
        }
        
    }
    
    $scope.alreadyUser = function(){
        AWSCognito.config.region = config.reg;
        AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId: config.iid
        });
       
        var poolData = { UserPoolId : config.uid,
                ClientId : config.cid
            };
        var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
        
        var cognitoUser = userPool.getCurrentUser();
     
        if (cognitoUser != null && localStorage.getItem("740a2y1e") != null) {
            
            var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
            $scope.decry=JSON.parse(decrypted);
              cognitoUser.getSession(function(err, session) {
                    if (err) {
                        $scope.seucrelogin();
                    }else{
                        $scope.loading = true;
                        $window.location.href = '#dashboard';
                        $uibModalInstance.close();
                        }
              });
            
        } else {
            $scope.seucrelogin();
            
        }
        
    }
    
    $scope.seucrelogin = function(){
            $scope.loading = true;

             AWSCognito.config.region = config.reg;
                AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
                    IdentityPoolId: config.iid
                });
       
            var authenticationData = {
                Username : $scope.email,
                Password : $scope.pwd
            };
            var authenticationDetails = new AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);
            var poolData = { UserPoolId : config.uid,
                ClientId : config.cid
            };
           
            var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
          
            var userData = {
                Username :  $scope.email,
                Pool :  userPool
            };
        
            var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
          
            cognitoUser.authenticateUser(authenticationDetails, {
                
                onSuccess: function (result) {
                    
                    if ($scope.pwdreset == true){
                        $scope.loading = false;
                        $scope.pwd = '';
                        localStorage.removeItem('isUser');
                        $scope.pwdreset = false;
                        $scope.$apply();
                        swal({title: "",type:'success', text: "Password reset completed.", width: '400px',showConfirmButton: true, confirmButtonText: 'Login Now', confirmButtonColor: "orange"});
                    }else{
                        
                    $scope.result = result;
                    
                    cognitoUser.getUserAttributes(function(err, result1) {
                        if (err) {
                           
                            //swal({title: "Oooops!", text: "Something went wrong. Please try to logging again!!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "orange"});
                            $scope.error = 5;
                            $scope.$apply();
                            localStorage.clear();
                            //$window.location.href = '#page';
                           // return;
                        }
                        
                        else{
                         
                            for (var i = 0; i < result1.length; i++) {
                                if (result1[i].getName() === 'name'){
                                    $scope.username = result1[i].getValue();                                   
                                    //localStorage.setItem("username", $scope.username);
                                    
                                    }
                                if (result1[i].getName() === 'sub'){
                                    $scope.useremail = result1[i].getValue();        
                                    
                                    }
                                
                            }
                            
                                if (result1 != null){
                                    
                                    AWS.config.region = config.reg;
                                    var cognitoidentity = new AWS.CognitoIdentity();
                                    
                                   
                                    var decoded = jwtHelper.decodeToken(result.getIdToken().getJwtToken());
                                    var userrole = decoded['cognito:groups'];
                                    
                                    if (decoded['cognito:groups'] == null) {
                                       
                                        localStorage.setItem("JwtToken", result.getIdToken().getJwtToken());
                                        var logins = 'cognito-idp.us-east-1.amazonaws.com/'+config.uid;
                                        $scope.logins = {};
                                   
                                        $scope.logins[logins] = result.getIdToken().getJwtToken();
                                        var params = {
                                                  IdentityPoolId: config.iid, 
                                                 Logins: $scope.logins
                                                };
                                        
                                                cognitoidentity.getId(params, function(err, data) {
                                                  if (err)
                                                      {
                                                
                                                      $scope.loading = false;
                                                      $scope.$apply();
                                                       if(err == "NetworkingError: Network Failure")
                                                        {
                                                            $scope.error = 6;
                                                            $scope.$apply();
                                                            
                                                        }else {
                                                          
                                                            $scope.error = 5;
                                                            $scope.$apply();
                                                            
                                                        }
                                                     // swal({title: "Oooops!", text: "Something went wrong. Please try to logging again!!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "orange"});
                                                    /*  $scope.error = 5;
                                                    $scope.$apply();*/
                                                     
                                                    localStorage.clear();
                                                      }// an error occurred
                                                  else    { 
                                                      //alert(data.IdentityId);
                                                        	  var data='{ "oid": "ENHANZED","cid": "'+config.cid+'","emailid": "'+$scope.email+'","iid": "'+config.iid+'","uid": "'+config.uid+'" ,"email" : "'+ $scope.useremail +'","id" : "'+ data.IdentityId +'","username" : "'+ $scope.username +'","ctype": "'+config.ctype+'" } ';  
                                                              var encrypted = $crypto.encrypt(data, config.key);
                                                              localStorage.setItem("740a2y1e",encrypted);
                                                              $scope.updatelogin('1');
                                                              localStorage.removeItem("fbuser");
                                                              $window.location.href = '#dashboard';
                                                          
                                                         
                                                          $uibModalInstance.close(true);
                                                    
                                                        
                                                      }         // successful response
                                                });
                                        
                                    }
                                    else {
                                        
                                        
                                        localStorage.setItem("JwtToken", result.getIdToken().getJwtToken());
                                        var logins = 'cognito-idp.us-east-1.amazonaws.com/'+config.uid;
                                        $scope.logins = {};
                                        $scope.logins[logins] = result.getIdToken().getJwtToken();
                                        var params = {
                                                  IdentityPoolId: config.iid, 
                                                 Logins: $scope.logins
                                                };
                                                cognitoidentity.getId(params, function(err, data) {
                                                  if (err)
                                                      { 
                                                    
                                                      $scope.loading = false;
                                                      $scope.$apply();
                                                       //swal({title: "Oooops!", text: "Something went wrong. Please try to logging again!!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "orange"});
                                                      $scope.error = 5;
                                                    $scope.$apply();
                                                    localStorage.clear();
                                                    }// an error occurred
                                                  else    { 
                                                     
                                                          var data='{ "oid": "ENHANZED","cid": "'+config.cid+'","emailid": "'+$scope.email+'","iid": "'+config.iid+'","uid": "'+config.uid+'" ,"email" : "'+ $scope.useremail +'","id" : "'+ data.IdentityId +'","username" : "'+ $scope.username +'","ctype": "'+config.ctype+'" } ';
                                                          var encrypted = $crypto.encrypt(data, config.key);
                                                          localStorage.setItem("740a2y1e",encrypted);
                                                          
                                                          $scope.updatelogin('1');
                                                         
                                                          $window.location.href = '#dashboard';
	                                                      $uibModalInstance.close(true);
                                                    
                                                        
                                                      }         // successful response
                                                });
                                        
                                    /*
                                        $scope.userrole = [];
                                        for (var i =0; i < decoded['cognito:groups'].length; i++) {
                                            var role = decoded['cognito:groups'][i];
                                            var value1 = role.indexOf("-");
                                            $scope.role = role.substring((value1+1), (role.length));
                                            alert($scope.role)
                                            if ($scope.role == "Admin" || $scope.role == "Instructors" || $scope.role == "Users") {
                                                if ($scope.role == "Admin"){
                                                    $scope.obj = {role: "Admin"};
                                                    $scope.userrole.push($scope.obj);
                                                }
                                                if ($scope.role == "Instructors"){
                                                    $scope.obj = {role: "Instructors"};
                                                    $scope.userrole.push($scope.obj);
                                                }
                                                if ($scope.role == "Users"){
                                                    $scope.obj = {role: "Instructors"};
                                                    $scope.userrole.push($scope.obj);
                                                }
                                            }
                                            $scope.userrole = [];
                                            $scope.loading = false;
                                            $scope.$apply();
                                            swal({title: "Oooops!", text: "You are not Authorized to Enter!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "orange"});
                                        }
                                        
                                        if ($scope.userrole.length > 0){
                                            $scope.updatelogin();
                                            
                                             localStorage.setItem("JwtToken", result.getIdToken().getJwtToken());
                                            
                                                var params = {
                                                          IdentityPoolId: $scope.IdentityPoolId, 
                                                         Logins: {
                                                              'cognito-idp.us-east-1.amazonaws.com/us-east-1_EK31UTYeY': result.getIdToken().getJwtToken()
                                                            
                                                          }
                                                        };
                                                        cognitoidentity.getId(params, function(err, data) {
                                                          if (err)
                                                              { 
                                                              
                                                              alert("Cognito Error"+err);
                                                              console.log(err, err.stack); }// an error occurred
                                                          else    { 
                                                              
                                                                AWS.config.region = $scope.region;
                                                                var cognitoidentity = new AWS.CognitoIdentity();
                                                                 
                                                                var params2 = {
                                                                          IdentityId: data.IdentityId,
                                                                          
                                                                          Logins: {
                                                                              'cognito-idp.us-east-1.amazonaws.com/us-east-1_EK31UTYeY' : result.getIdToken().getJwtToken()
                                                                          }
                                                                        };
                                                                
                                                                cognitoidentity.getCredentialsForIdentity(params2, function(err, data2) {
                                                                      if (err) {
                                                                          alert("Cognito Error"+err);
                                                                      } // an error occurred
                                                                      else   {  
                                                                          $uibModalInstance.dismiss('cancel');
                                                                          var accessKeyId = data2.Credentials.AccessKeyId;
                                                                          var secretAccessKey = data2.Credentials.SecretKey;
                                                                          var sessionToken = data2.Credentials.SessionToken;
                                                                          localStorage.setItem("email", $scope.email);
                                                                          //$window.location.href = '#dashboard';
                                                                          
                                                                                
                                                                      };
                                                                });
                                                            
                                                                
                                                              }         // successful response
                                                        });
                                        
                                        }
                                        else {
                                            $scope.loading = false;
                                            $scope.$apply();
                                            swal({title: "Oooops!", text: "1You are not Authorized to Enter!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "orange"});
                                        }
                                    */  
                                    }
                                    
                                }
                          }
                        
                    });
                    }       
                    //console.log('access token + ' + result.getAccessToken().getJwtToken());
                    // Use the idToken for Logins Map when Federating User Pools with Cognito Identity or when passing through an Authorization Header to an API Gateway Authorizer
                    //console.log('idToken + ' + result.idToken.jwtToken);
                },

                onFailure: function(err) {
          
                    $scope.loading = false;
                    $scope.$apply();
                    if (err == "UserNotFoundException: User does not exist.") 
                    {
                        $scope.error = 3;
                        $scope.$apply();
                        //swal({title: "Oooops!", text: "Invalid Usermail or password!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                    }
                    else if(err == "NetworkingError: Network Failure")
                    {
                        $scope.error = 6;
                        $scope.$apply();
                        //swal({title: "Oooops!", text: "Please check your internet connection!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                    }else if(err == "NotAuthorizedException: Incorrect username or password."){
                        $scope.error = 4;
                        $scope.$apply();
                    }else if(err=="NotAuthorizedException: User is disabled"){
                        $scope.error = 7;
                        $scope.$apply();
                    } else if(err="NotAuthorizedException: User account has expired, it must be reset by an administrator."){
                        $scope.error = 8;
                        $scope.$apply();
                    }else{
                      
                        $scope.error = 5;
                        $scope.$apply();
                        //swal({title: "Oooops!", text: "Invalid Usermail or password!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                    }
                      
                },
                
                newPasswordRequired: function(userAttributes, requiredAttributes) {
                    
                    $scope.loading = false;
                    $scope.$apply();
                    $scope.obj = this;
                    swal({
                          title: "Change Password",
                          html:
                            
                            '<p style="text-align:left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;">Password</p>' +
                            '<p style="color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;">:</p>' +
                            '<input type="password" id="pwd" name="nduration" rows="1" wrap="soft" ng-model="nduration" style="border: 1px solid #ccc;float:left;width:65%;overflow:hidden;line-height:25px;margin-bottom:10px;font-size:14px;"></input>'+
                            '</br>'+
                            '<p style="text-align:left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;">Confirm Password</p>' +
                            '<p style="color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;">:</p>' +
                            '<input type="password" id="cpwd" name="nduration" rows="1" wrap="soft" ng-model="nduration" style="border: 1px solid #ccc;float:left;width:65%;overflow:hidden;line-height:25px;font-size:14px;margin-bottom:10px;"></input>'
                            +'</br>'+
                            '</br>'+
                            '<span style="width:100%;color:black;font-family:MyWebFont1;">* Password should be at least 8 characters, contain at least one lowercase letter & one number & one special character.</span>' +
                            '</br>',

                            closeOnCancel: false,
                            allowOutsideClick: false,
                            allowEscapeKey:false,
                            showCancelButton: true, cancelButtonText: 'Cancel',
                            showConfirmButton: true, confirmButtonText: 'Next', confirmButtonColor: "orange",
                            width: '450px',
                            height: '450px',
                            preConfirm: function () {
                            return new Promise(function (resolve) {

                                 if (  $('#cpwd').val().replace(/\s/g, '').length === 0 || $('#pwd').val().replace(/\s/g, '').length === 0 || $('#cpwd').val() === '' ||  $('#pwd').val() === '') {
                                     swal.showValidationError('Please enter the required fields');
                                      resolve();
                                     
                                  
                                } else {
                                    
                                if($('#cpwd').val().indexOf(' ') >= 0 ||  $('#cpwd').val().indexOf(' ') >= 0 ){
                                      swal.showValidationError('Password should not contain space');
                                      resolve();
                                }else if ($('#cpwd').val() ===  $('#pwd').val() ){
                                        
                                         resolve([
                                             $('#name').val(),
                                             $('#pwd').val()
                                             ]);
                                    }else {
                                        
                                          swal.showValidationError('Password do not match');
                                          resolve();
                                          }
                                      
                                }
                            
                            });
                          }
                        }).then(function (result) {
                            
                            if(result.dismiss == 'cancel' || result.dismiss == 'close' ){
                                //$scope.modalclose();
                                }else{
                            var json = JSON.stringify(result);
                            $scope.ndesc = JSON.parse(json);
                            $scope.code = $scope.ndesc.value[0];;
                            $scope.npwd = $scope.ndesc.value[1];;
                            $scope.pwdreset = true;
                            $scope.$apply();
                            var attributesData = {
                                   
                                };
                            
                            cognitoUser.completeNewPasswordChallenge($scope.npwd, attributesData, $scope.obj);
                            
                                }
                         
                        });
                    
                    
                },

            }); 
        
      };
    
    
    $scope.modalclose = function(){
        $window.location.href = '#page';
    };
    
    $scope.forgotpwd = function(){
       
        $uibModalInstance.dismiss('cancel');   
        $scope.Instance = $uibModal.open({
        templateUrl: 'forgotpwd.html',
        controller: 'forgotpwdCtrl',
        backdrop: 'static',
        windowClass: 'forgotmodal',       
        keyboard: false
        });
       
};
$scope.signup = function(){
    
    $uibModalInstance.close('signup');
    /*$uibModalInstance.dismiss('cancel');
    $scope.Instance = $uibModal.open({
    templateUrl: 'signup.html',
    controller: 'signupCtrl',
    backdrop: 'static',
    windowClass: 'signupmodal',
    keyboard: false
    });*/

};
$scope.updatelogin = function(id){
    
    $scope.getcredentials(id);
};

$scope.getcredentials = function(id){
    
    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
    $scope.decry=JSON.parse(decrypted);
    
    var fbuser = localStorage.getItem("fbuser");
   AWSCognito.config.region =  config.reg;
   AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
       IdentityPoolId: $scope.decry.iid
   });
  
   var poolData = { UserPoolId : $scope.decry.uid,
           ClientId : $scope.decry.cid
       };
   
   var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
   
   var cognitoUser = userPool.getCurrentUser();
   
   if (cognitoUser != null && $scope.decry.id != null) {
       $scope.getsession(cognitoUser,id);

   } else if(fbuser != null){
       $scope.getfbsession();

}else {
   
       localStorage.clear();
       $window.location.href = '#page';
   }
   
   
};


$scope.getsession = function(cognitoUser,id){
   
   return new Promise((resolve, reject) => {
          cognitoUser.getSession((err, session) =>{
             if (err) {
                 swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                 localStorage.clear();
                 $window.location.href = '#page';
             }else{
                 
                 var token = session.idToken.jwtToken;
                 $scope.Credentialsweb('a', 'a', 'a',id,token); 
               /*  var apigClient = apigClientFactory.newClient({});
                 var params1 = {};
                 var body = {type: '0',token : token,oid : $scope.decry.oid};
                 var additionalParams = {};
                 
                 apigClient.getCredentialsWebPost(params1, body, additionalParams)
                 .then(function(result1){
                    var tjson = JSON.stringify(result1.data);
                    tjson = tjson.toString();
                    
                    tjson = JSON.parse(tjson);
                   
                    $scope.Credentialsweb(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken,id); 
                    resolve();   
                     }).catch( function(result){
                         swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                         localStorage.clear();
                         $window.location.href = '#page';
                     })*/
         
             }
         });
     })
}
$scope.Credentialsweb = function(accessKeyId, secretAccessKey, sessionToken,id,token){
  
   
   var params = {};
   var logintype;
   if($scope.decry.api === undefined){
       
       var apigClient = apigClientFactory.newClient({
           
       });
   }else{
       
       var apigClient = apigClientFactory.newClient({
           invokeUrl: $scope.decry.api,
       });
   }

   if(id == '1'){
       var eventtype = "AuthenticatedViaCognito";
   }else{
       var eventtype = "AuthenticatedViaFB";
   }
   var body = {
               oid: $scope.decry.oid,
               id: $scope.decry.id,
               eventtype: eventtype,
               email: $scope.useremail,
               emailid: $scope.email,
               name: $scope.username,
            };
   var additionalParams = {
           headers: {Authorization : token
           }
     };

   apigClient.analyticsWebAppPost(params, body, additionalParams)
   .then(function(result){ 
        
       }).catch( function(result){
           $scope.loading = false;
           $scope.$apply();
           var json = JSON.stringify(result);
           var json1 = json.toString();
           alert('ERROR while updatelogin'+json1);
           
           
       });

 
   
};

$scope.orglogin = function(){
    $uibModalInstance.close('org');
}

$scope.close = function(){
    $uibModalInstance.dismiss('cancel');
};


// FB Login




$scope.user = {};

// Defining user logged status
$scope.logged = false;

// And some fancy flags to display messages upon user status change
$scope.byebye = false;
$scope.salutation = false;

/**
 * Watch for Facebook to be ready.
 * There's also the event that could be used
 */
/*$scope.$watch(
  function() {
    return Facebook.isReady();
  },
  function(newVal) {
    if (newVal)
      $scope.facebookReady = true;
  }
);*/

/*var userIsConnected = false;

Facebook.getLoginStatus(function(response) {
  if (response.status == 'connected') {
    userIsConnected = true;
  }
});

$scope.check = function(){
      
      Facebook.getLoginStatus(function(response) {
          
  if (response.status == 'connected') {
    userIsConnected = true;
  }
});
};*/
/**
 * IntentLogin
 */
$scope.fbLogin = function() {
    
    $scope.fblogon();

};

/**
 * Login
 */
 $scope.fblogon = function() {
    
   Facebook.login(function(response) {
    if (response.status == 'connected') {
      $scope.logged = true;
      $uibModalInstance.close(true);
      var json = JSON.stringify(response);
       var topic = json.toString();
       $scope.fbtoken = topic.accessToken;
       
       if($scope.fbtoken == undefined){
           $scope.fbtoken1 = JSON.parse(topic);
           $scope.fbtoken = $scope.fbtoken1.authResponse.accessToken;
           localStorage.setItem("fbid",  $scope.fbtoken1.authResponse.userID);
       }
       localStorage.setItem("fbuser",  $scope.fbtoken);
      /* localStorage.setItem("oid", "ENHANZED");
       localStorage.setItem('cid', config.cid);
       localStorage.setItem('iid', config.iid);
       localStorage.setItem('uid', config.uid);*/
       var data='{ "oid": "ENHANZED","cid": "'+config.cid+'","iid": "'+config.iid+'","uid": "'+config.uid+'"  } ';
       var encrypted = $crypto.encrypt(data, config.key);
       localStorage.setItem("740a2y1e",encrypted);
       
       $scope.me();
       
    }
  
  });
 };
 
 /**
  * me 
  */
  $scope.me = function() {
    Facebook.api('/me?fields=email,name,id', function(response) {
      /**
       * Using $scope.$apply since this happens outside angular framework.
       * http://graph.facebook.com/id-of-user/picture?type=large
       */ 
        
         var json1 = JSON.stringify(response);
       var topic1 = json1.toString();
      topic1 = JSON.parse(topic1);
      var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
      $scope.decrypt=JSON.parse(decrypted);
      
      $scope.decrypt["email"] = topic1.email;
      $scope.decrypt["username"] =  topic1.name;
     $scope.fbid();
      $scope.$apply(function() {
      });
      
    });
  };

/**
 * Logout
 */
$scope.logoutgb = function() {
  Facebook.logout(function() {
    $scope.$apply(function() {
      $scope.user   = {};
      $scope.logged = false;  
    });
  });
}

/**
 * Taking approach of Events :D
 */
/*$scope.$on('Facebook:statusChange', function(ev, data) {
  console.log('Status: ', data);
  if (data.status == 'connected') {
    $scope.$apply(function() {
      $scope.salutation = true;
      $scope.byebye     = false;    
    });
  } else {
    $scope.$apply(function() {
      $scope.salutation = false;
      $scope.byebye     = true;
      
      // Dismiss byebye message after two seconds
      $timeout(function() {
        $scope.byebye = false;
      }, 2000)
    });
  }
  
  
});*/


$scope.fbid = function(){
    
    AWS.config.region = config.reg;
    var cognitoidentity = new AWS.CognitoIdentity()
    var logins = 'graph.facebook.com';
    $scope.logins = {};
    $scope.logins[logins] = $scope.fbtoken;
    //alert('4'+$scope.fbtoken)
    var params = {
              IdentityPoolId: config.iid, 
             Logins: $scope.logins
            };
    
            cognitoidentity.getId(params, function(err, data) {
              if (err)
                  {  
                  swal({title: "Oooops!", text: "Something went wrong. Please try to logging again!!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "orange"});
                  localStorage.clear();
                  $window.location.href = '#page';
                  console.log(err, err.stack); }// an error occurred
              else    { 
                 
                    AWS.config.region = config.reg;
                    var cognitoidentity = new AWS.CognitoIdentity();
                    
                    //localStorage.setItem("id", data.IdentityId);
                    $scope.decrypt["id"] = data.IdentityId;
                    $scope.IdentityId = data.IdentityId;
                    
                    var params2 = {
                              IdentityId: data.IdentityId,
                              
                              Logins: $scope.logins
                            };
                    
                    cognitoidentity.getCredentialsForIdentity(params2, function(err, data2) {
                          if (err) {
                              swal({title: "Oooops!", text: "Something went wrong. Please try to logging again!!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "orange"});
                              localStorage.clear();
                              $window.location.href = '#page';
                          } // an error occurred
                          else   { $scope.navtopic = localStorage.getItem("navtopic");
                              if($scope.navtopic != undefined || $scope.navtopic != null){
                                  $scope.decrypt["otopicid"] =   $scope.navtopic;
                                  localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
                                 $window.location.href = '#topic';
                              }else{
                                  localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
                                 $window.location.href = '#dashboard';
                              }
                              $uibModalInstance.close(true);
                                    
                          };
                    });
                
                    
                  }         // successful response
            });
};

};

app.controller('loginCtrl', loginCtrl);
loginCtrl.$inject = ['$scope', '$http', '$location', '$window', '$timeout', '$cookies','$uibModal','$uibModalInstance','jwtHelper','config','$crypto'];


