var pageCtrl = function ($scope, $http, $location, $window, $cookies, toptopics, config, Facebook) {


$scope.home = function(){
	
	
	$scope.topics = toptopics;
	if($scope.topics == false){
		$scope.errorpage = true;
		
	}else{
		
		AWSCognito.config.region = config.reg;
    	AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
    	IdentityPoolId: config.iid
    });
   
	var poolData = { UserPoolId : config.uid,
	        ClientId : config.cid
	    };
		var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
		
		var cognitoUser = userPool.getCurrentUser();
		$scope.myObj = {'background-color':'grey','overflow':'hidden','background-image':'url(./lib/images/homepage.jpg)','background-size': 'cover','background-repeat': 'no-repeat'};
	    if (cognitoUser != null && localStorage.getItem("id") != null) {
	    	
	    	  cognitoUser.getSession(function(err, session) {
		            if (err) {
		            	
		            	$scope.errorpage = false;
		            	$scope.topics = toptopics;
		            	$scope.topics = JSON.parse($scope.topics);
		            	$scope.topics = $scope.topics.hits.hit;
		            	for(var i=0; i < $scope.topics.length; i++){
		            		
		            		$scope.topicimg = config.url+"ENHANZED/IMAGES/"+$scope.topics[i].id+'/'+$scope.topics[i].id+"-lg.png";
		            		$scope.topics[i].topicimg = $scope.topicimg;
		            	}
		            	if($window.innerWidth > 767){
		            		$scope.showapppage = false;
		            		$scope.showpage = true;
		                	
		            	}else{
		            		$scope.showpage = false;
		            		$scope.showapppage = true;
		            		
		            	}
		            	if($window.innerWidth > 1024){
		            		$scope.nooftopics = 4;
		                	
		            	}else{
		            		$scope.nooftopics = 3;
		            		
		            	}
		            	$(window).resize(function() {
		            		
		            	      $scope.$apply(function() {
		            	        $scope.windowWidth = $( window ).width();
		            	        if($scope.windowWidth < 767){
		            	        	$scope.showpage = false;
		            	        	$scope.showapppage = true;
		            	        }
		            	        if($scope.windowWidth > 767	){
		            	        	
		            	        	$scope.showpage = true;
		            	        	$scope.showapppage = false;
		            	        }
		            	        if($window.innerWidth > 1024){
		            	        	$scope.test1($window.innerWidth);
		            		    	
		            			}else{
		            				$scope.test1($window.innerWidth);
		            				
		            			}
		            	      });
		            	    });
		            }else{
		            	
		            	$scope.loading = true;
		            	$window.location.href = '#home';
		            	
		            	}
	    	  });
	    	
	    }else {
    		
    		$scope.$watch(
    				  function() {
    					  
    				    return Facebook.isReady();
    				  },
    				  function(newVal) {
    					 
    				    if (newVal){
    				    	  $scope.facebookReady = true;
    				    	  $scope.fbcheck();
    				    }
    				    
    				  }
    				);
	    	
	    		
    		
	    	$scope.errorpage = false;
	    	$scope.topics = toptopics;
	    	$scope.topics = JSON.parse($scope.topics);
	    	$scope.topics = $scope.topics.hits.hit;
	    	for(var i=0; i < $scope.topics.length; i++){
	    		
	    		$scope.topicimg = config.url+"ENHANZED/IMAGES/"+$scope.topics[i].id+'/'+$scope.topics[i].id+"-lg.png";
	    		$scope.topics[i].topicimg = $scope.topicimg;
	    	}
	    	if($window.innerWidth > 767){
	    		$scope.showapppage = false;
	    		$scope.showpage = true;
	        	
	    	}else{
	    		$scope.showpage = false;
	    		$scope.showapppage = true;
	    		
	    	}
	    	if($window.innerWidth > 1024){
	    		$scope.nooftopics = 4;
	        	
	    	}else{
	    		$scope.nooftopics = 3;
	    		
	    	}
	    	$(window).resize(function() {
	    		
	    	      $scope.$apply(function() {
	    	        $scope.windowWidth = $( window ).width();
	    	        if($scope.windowWidth < 767){
	    	        	$scope.showpage = false;
	    	        	$scope.showapppage = true;
	    	        }
	    	        if($scope.windowWidth > 767	){
	    	        	
	    	        	$scope.showpage = true;
	    	        	$scope.showapppage = false;
	    	        }
	    	        if($window.innerWidth > 1024){
	    	        	$scope.test1($window.innerWidth);
	    		    	
	    			}else{
	    				$scope.test1($window.innerWidth);
	    				
	    			}
	    	      });
	    	    });
	    	
    	
	    }
		
	}
	
};

$scope.home();

$scope.loadpage = function(){
	
};
$scope.fbcheck = function(){
	var userIsConnected = false;
	Facebook.getLoginStatus(function(response) {
		
		  if (response.status == 'connected') {
		   if(localStorage.getItem("id") != null){
			   $scope.loading = true;
			   var fbtoken = JSON.stringify(response);
	  			fbtoken = fbtoken.toString();
	  			
	  			fbtoken = JSON.parse(fbtoken);
	  			fbtoken = fbtoken.authResponse.accessToken;
			   localStorage.setItem("fbuser", fbtoken);
			  	$window.location.href = '#home';
		   }
		  }else{
			  
		  }
		});

}
$scope.test1 = function(width){

	if(width > 1024){
		$scope.nooftopics = 4;
		//$scope.$apply();
	}else{
		$scope.nooftopics = 3;
		//$scope.$apply();
	}
};

$scope.viewtopic = function(topic){
	$scope.loading = true;
	localStorage.setItem("vtopicid", topic.id);
	$window.location.href = '#viewtopic';
};




	
};
app.controller('pageCtrl', pageCtrl);
pageCtrl.$inject = ['$scope', '$http', '$location', '$window','$cookies','toptopics', 'config', 'Facebook'];

app.factory("gettopTopics", function($window, $q){
    return {
    	gettopTopics: function(){
			
			
			var apigClient = apigClientFactory.newClient({
						});
				
					var params = {};
					
					
					
						var body = {
								oid: "4129",
								 };
						
						var additionalParams = {};
						
						var toptopics =	apigClient.gettopTopicsPost(params, body, additionalParams)
						.then(function(result){
							 	var json = JSON.stringify(result.data);
							    var topic = json.toString();
							   
								return $q.when(topic);
							
						}).catch( function(result){
						    
						    	
						    	return $q.when(false);
						    	
						    });

		   
           return toptopics;
        }
    };
});

