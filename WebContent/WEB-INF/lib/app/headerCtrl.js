var headerCtrl = function ($scope, $http, $location, $window, $cookies, config, $interval ,$crypto) {
	
	
    var timer;
$scope.header = function(){
    
    if(localStorage.getItem("740a2y1e") != null)
        {
            var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
            $scope.decrypt=JSON.parse(decrypted);
        }else
        {
            $window.location.href = '#page';
        }
   
  
	$scope.username = $scope.decrypt.username;	
	if(localStorage.getItem("fbuser") != null ){
		$scope.imgsrc =  "https://graph.facebook.com/"+localStorage.getItem("fbid")+"/picture?type=large";
		
	}else{
		$scope.imgsrc =  $scope.decrypt.oid == 'ENHANZED' ? "lib/images/pic2.png" : "lib/images/pic.png";
	}

	$scope.orgimg = config.url+$scope.decrypt.oid.toLowerCase()+"-resources/images/org-images/"+$scope.decrypt.oid+".png";
	$scope.getCategories('cat');
	$scope.getnotifications('get');
	$scope.start();
	
	
	
	/*$scope.cats = [{"category":'Anesthesiology'},{"category":'Biochemistry'},{"category":'Cardiology'},
		{"category":'Critical Care'},{"category":'Dermatology'},{"category":'Emergency Medicine'},{"category":'Gastroenterology'},
		{"category":'Head and Neck'},{"category":'Infectious Diseases'},{"category":'OB-Gyn'},
		{"category":'Pulmonology'},{"category":'Internal Medicine'},{"category":'Neurology'},
		{"category":'Oncology'},{"category":'Orthopedics'},{"category":'Pediatrics'},
		{"category":'Radiology'},{"category":'Urology'}];*/
	
};

$scope.start = function(){
    $scope.stop();
      timer = $interval(function () {
          $scope.getnotifications('get');
      }, 600000);
  }

$scope.stop = function() {
    
    $interval.cancel(timer);
    console.log('Notification Canceled')
 };
 
$scope.$on('$destroy', function() {
     $scope.stop();
  });

$scope.myFunct = function(keyEvent, searchterm) {
	  if (keyEvent.which === 13)
		 if(searchterm == undefined || searchterm == ''){
     		swal({title: "Please enter a search term!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "orange"});
		 }else{
			 $scope.loading = true;
			   var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
			    $scope.decry=JSON.parse(decrypted);
			    $scope.decry["search"] = searchterm;
			    localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
				//localStorage.setItem("search", searchterm);
				$window.location.href = '#searchtopic';
		 }
	};
	
	$scope.notification = function(accessKeyId, secretAccessKey, sessionToken,token){
	   
		if($scope.notifications.read == 1){
			
		    if($scope.decrypt.api === undefined){
		        
		        var apigClient = apigClientFactory.newClient({
		            
		        });
		    }else{
		        
		        var apigClient = apigClientFactory.newClient({
		            invokeUrl: $scope.decrypt.api,
		        });
		    }

			var params = {};
		 
			var body = {
					oid: $scope.decrypt.oid,
					eid: $scope.decrypt.email
					};
			
			var additionalParams = {
                    headers: {Authorization : token
                    }
              };
		
			apigClient.postNotificationStatusWebPost(params, body, additionalParams)
				.then(function(result){
				   
					$scope.notifications.read = 0;
					$scope.$apply();
					 }).catch( function(result){
				    	
				    	return $q.when(false);
				    	
				    });
		}
	}
	
	$scope.getnotifications = function(tname){
	    var fbuser = localStorage.getItem("fbuser");
	    console.log('Notification')
	       AWSCognito.config.region =  config.reg;
	       AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
	           IdentityPoolId: $scope.decrypt.iid
	       });
	     
	       var poolData = { UserPoolId : $scope.decrypt.uid,
	               ClientId : $scope.decrypt.cid
	           };
	       
	       var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
	       
	       var cognitoUser = userPool.getCurrentUser();
	       
	       if (cognitoUser != null && $scope.decrypt.id != null) {
	           $scope.getsession(cognitoUser,tname);

	       } else if(fbuser != null){
	          
	           $scope.getfbsession(tname);

	   }else {
	       
	           localStorage.clear();
	           $window.location.href = '#page';
	       }
	       
	   
	};
	$scope.getfbsession= function(tname){
         return new Promise((resolve, reject) => {
            Facebook.getLoginStatus( (response)=> {
               if (response.status == 'connected') {
                  var fbjson = JSON.stringify(response);
                  fbjson = fbjson.toString();
                  var fbtoken = fbjson.accessToken;
                  
                  if(fbtoken == undefined){
                     var fbtoken1 = JSON.parse(fbjson);
                     fbtoken = fbtoken1.authResponse.accessToken;
                  }
                 
                  $scope.gettoken(fbtoken, '1',tname);
                  resolve();   
               }else{
                       swal({title: "Oooops!", text: "Facebook session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                       localStorage.clear();
                       $window.location.href = '#page';
               }
               }); 
         })
	       
      }
	 $scope.gettoken = function (token, id,tname){
	     
	     if($scope.decrypt.api === undefined){
	         
	         var apigClient = apigClientFactory.newClient({
	             
	         });
	     }else{
	         
	         var apigClient = apigClientFactory.newClient({
	             invokeUrl: $scope.decrypt.api,
	         });
	     }
           var params1 = {};
           var body = {type: id,token : token,oid : $scope.decrypt.oid};
           var additionalParams = {};
         
           var abcd = apigClient.getCredentialsWebPost(params1, body, additionalParams)
           .then(function(result1){
              var tjson = JSON.stringify(result1.data);
              tjson = tjson.toString();
              
              tjson = JSON.parse(tjson);
            
             if(tname == 'get')
             {
                 $scope.listnotifications(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken ); 
             }else if(tname == 'explore'){
                 
                 $scope.Credentialsweb(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken); 
             }
             else
                 {
               
                 $scope.notification(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken ); 
                 }
                   
               }).catch( function(result){
                   swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
                       allowEscapeKey:false, buttonsStyling: false});
                   localStorage.clear();
                   $window.location.href = '#page';
                   
               })
      }
	
	
	
	$scope.listnotifications = function(accessKeyId, secretAccessKey, sessionToken,token){
		
	    if($scope.decrypt.api === undefined){
	        
	        var apigClient = apigClientFactory.newClient({
	            
	        });
	    }else{
	        
	        var apigClient = apigClientFactory.newClient({
	            invokeUrl: $scope.decrypt.api,
	        });
	    }

		var params = {};
	 
		var body = {
				oid: $scope.decrypt.oid,
				eid: $scope.decrypt.email
				};
		
		var additionalParams = {
                headers: {Authorization : token
                }
          };
	
		apigClient.getNotificationsPost(params, body, additionalParams)
			.then(function(result){
			  
				$scope.notifications = JSON.stringify(result.data);
				$scope.notifications = $scope.notifications.toString();
				$scope.notifications = JSON.parse($scope.notifications); 
				// 0 is read, 1 is unread
			
				if($scope.notifications.notifications == false || $scope.notifications.notifications == "false"){
					
					$scope.notifications.notifications = [];
					$scope.obj = {
							msg: '',
							title:'No Notifications',
							type: 0,
							time: ''
					}
					
					$scope.notifications.notifications.push($scope.obj);
					$scope.notify = $scope.notifications.notifications;
					 $scope.$apply();
				}
				else{
				if($scope.notifications.notifications.length > 10){
					$scope.noti = $scope.notifications.notifications.slice(0, 10);
					$scope.notifications.notifications = $scope.noti
					
				}
			    for(var i=0; i<$scope.notifications.notifications.length;i++){
					var diff = moment.utc($scope.notifications.notifications[i].time).fromNow();
					$scope.notifications.notifications[i].time = diff;
				}
			    $scope.notify = $scope.notifications.notifications;
				 $scope.$apply();
			    }
				
				
				
			    }).catch( function(result){
			    	
			    	return $q.when(false);
			    	
			    });
		
	}
	$scope.getCategories = function(tname){
	   
	    var fbuser = localStorage.getItem("fbuser");
	       AWSCognito.config.region =  config.reg;
	       AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
	           IdentityPoolId: $scope.decrypt.iid
	       });
	      
	       var poolData = { UserPoolId : $scope.decrypt.uid,
	               ClientId : $scope.decrypt.cid
	           };
	       
	       var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
	       
	       var cognitoUser = userPool.getCurrentUser();
	       
	       if (cognitoUser != null && $scope.decrypt.id != null) {
	           $scope.getsession(cognitoUser,tname);

	       } else if(fbuser != null){
	          
	           $scope.getfbsession('explore');

	   }else {
	           localStorage.clear();
	           $window.location.href = '#page';
	       }
	       
	 
	}
	
	
   $scope.getsession = function(cognitoUser,tname){
       
       return new Promise((resolve, reject) => {
              cognitoUser.getSession((err, session) =>{
                 if (err) {
                     swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
                         allowEscapeKey:false, buttonsStyling: false});
                     localStorage.clear();
                     $window.location.href = '#page';
                 }else{
                     
                     var token = session.idToken.jwtToken;
                     if(tname == 'cat')
                     {
                     $scope.Credentialsweb('a', 'a', 'a',token); 
                     }else if(tname == 'get')
                         {
                         $scope.listnotifications('a', 'a', 'a',token ); 
                         }else {
                             
                             $scope.notification('a', 'a', 'a',token); 
                             }
                   /*  var apigClient = apigClientFactory.newClient({});
                     var params1 = {};
                     var body = {type: '0',token : token,oid : $scope.decrypt.oid};
                     var additionalParams = {};
                     
                     apigClient.getCredentialsWebPost(params1, body, additionalParams)
                     .then(function(result1){
                        var tjson = JSON.stringify(result1.data);
                        tjson = tjson.toString();
                        
                        tjson = JSON.parse(tjson);
                        if(tname == 'cat')
                            {
                            $scope.Credentialsweb(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken); 
                            }else if(tname == 'get')
                                {
                                $scope.listnotifications(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken ); 
                                }else {
                                    
                                    $scope.notification(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken ); 
                                    }
                       
                       
                         }).catch( function(result){
                             swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                             localStorage.clear();
                             $window.location.href = '#page';
                         })*/
             
                 }
             });
         })
 }
  
   $scope.Credentialsweb = function(accessKeyId, secretAccessKey, sessionToken,token){
 
       if($scope.decrypt.api === undefined){
           
           var apigClient = apigClientFactory.newClient({
               
           });
       }else{
           
           var apigClient = apigClientFactory.newClient({
               invokeUrl: $scope.decrypt.api,
           });
       }
       var params = {};
 
       var body = {
                 oid: $scope.decrypt.oid,
                 };
         
       var additionalParams = {
               headers: {Authorization : token
               }
         };
   
         categories = apigClient.getCategoriesPost(params, body, additionalParams)
             .then(function(result){
           
                var json = JSON.stringify(result.data);
                $scope.categorys= JSON.parse(json);
               
                 for(var m=0; m < $scope.categorys.Categories.length; m++){
                    
                     $scope.categoryimg = config.url+$scope.decrypt.oid.toLowerCase()+"-resources/images/category-images/"+$scope.categorys.Categories[m].id+".png";
                     $scope.categorys.Categories[m].categoryimg = $scope.categoryimg;
                 }
                $scope.cats = json.toString();
                $scope.cats = $scope.categorys; 
               
                 $scope.cats = $scope.cats.Categories;
                $scope.$apply();
                 }).catch( function(result){
              
                     return $q.when(false);
                     
                 });
   
     
       
   };
   
   
   
$scope.logout = function(){
	
	
	$scope.loading = true;
	$scope.ClientId = config.cid;
	$scope.UserPoolId = config.uid;        	
	var poolData = { UserPoolId : $scope.UserPoolId,
	        ClientId : $scope.ClientId 
	    };
	var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
	var cognitoUser = userPool.getCurrentUser();
		if ($scope.decrypt.email != null && cognitoUser != null){
		
			var cookies = $cookies.getAll();
			angular.forEach(cookies, function (v, k) {
			$cookies.remove(k);
			});
			$cookies.remove('CloudFront-Policy');
			$cookies.remove('CloudFront-Key-Pair-Id');
			$cookies.remove('CloudFront-Signature');
			
		
	    if (cognitoUser != null) {
	    	
	        cognitoUser.getSession(function(err, session) {
	            if (err) {
	                
	                return;
	            }
	           
	        });
	    }
		
		cognitoUser.globalSignOut( {
	    	
	        onSuccess: function (result) {
	        	localStorage.clear();
	        	$window.location.href = '#page';
	        },onFailure: function(err) {
	        	localStorage.clear();
	        	$window.location.href = '#page';
	        	
	        }});
		
	}else {
		var cookies = $cookies.getAll();
		angular.forEach(cookies, function (v, k) {
		$cookies.remove(k);
		});
		$cookies.remove('CloudFront-Policy');
		$cookies.remove('CloudFront-Key-Pair-Id');
		$cookies.remove('CloudFront-Signature');
		localStorage.clear();
		$window.location.href = '#page';}
		
	};
	$scope.header();
	
	$scope.dashpage = function(){
		if($scope.online == true){
			$scope.loading = true;
		$window.location.href = '#dashboard';}
	};
	$scope.progpage = function(){
		if($scope.online == true){
			$scope.loading = true;
		$window.location.href = '#myprogress';}
	};
	$scope.homepage = function(){
		if($scope.online == true){
			$scope.loading = true;
		$window.location.href = '#mytopics';}
	};
	$scope.profile = function(){
		if($scope.online == true){
			$scope.loading = true;
		$window.location.href = '#profile';}
	};
	$scope.search = function(searchterm){
		$scope.loading = true;
		 var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
         $scope.decry=JSON.parse(decrypted);
         $scope.decry["search"] = searchterm.category;
         $scope.decry["sid"] = searchterm.id;
         localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
		/*localStorage.setItem("search", searchterm.category);
		localStorage.setItem("sid", searchterm.id);*/
		
		$window.location.href = '#searchtopic';
	};
	
	$scope.test = function(){

        $window.location.href = '#viewcategory';
	}
	$scope.opennotification = function(msg){
	  
		if(msg.type == 1){
			$scope.loading = true;
		//	localStorage.setItem("otopicid", msg.tid);
			 var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
		        $scope.decry=JSON.parse(decrypted);
		        $scope.decry["otopicid"] = msg.tid;
		        localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
			$window.location.href = '#topic';
		}else if(msg.type == 2){
			 $window.open(msg.link, '_blank');
		}else{
		    swal({title: msg.title, text: msg.msg, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
                allowEscapeKey:false, buttonsStyling: false});
		   
			//do nothing
		}
	}
	

	
};
app.controller('headerCtrl', headerCtrl);
headerCtrl.$inject = ['$scope', '$http', '$location', '$window','$cookies', 'config','$interval','$crypto'];

