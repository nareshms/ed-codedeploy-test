var nuggetCtrl = function($scope, $http, $location, $window, $uibModal, topic, config,$crypto,NgTableParams) {
	$scope.defaultConfig = {
			
	};
	
	$scope.loadnugget = function(index, nid, object){
	    
        var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
       $scope.decry=JSON.parse(decrypted);
		if($scope.online == true){
		if(object == false && object != 0){
			object = 'a';
		}
		//alert('not again')
		 
		if(index < $scope.activenugget){
			$scope.objectprogress1 = 'showall';
		}
		if($scope.freenavigation == true || $scope.freenavigation == 'true'){
		  
			$scope.loading = true;

			$scope.decry["nid"]=nid;
            $scope.decry["object"]=object;
            $scope.decry["freenavigation"] =$scope.freenavigation;
            $scope.decry["tname"]= $scope.ttitle;
            $scope.decry["nuggets"]=$scope.noofnuggets;
            $scope.decry["tp"] = $scope.topic.tp.charAt(0);
            localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
			$window.location.href = '#player';
		}else{
			
			if(index == $scope.activenugget || index < $scope.activenugget){
			    $scope.decry["ttype"] =  $scope.topic.ctype;
				if(index == $scope.activenugget){
					if(object == $scope.objectprogress1){
				
					    $scope.loading = true;
						  $scope.decry["nid"]=nid;
	                        $scope.decry["object"]=object;
	                        $scope.decry["freenavigation"] =$scope.freenavigation;
	                        $scope.decry["tname"]= $scope.ttitle;
	                        $scope.decry["op"]=$scope.objectprogress1; 
	                        $scope.decry["nuggets"]=$scope.noofnuggets; 
	                        $scope.decry["tp"] = $scope.topic.tp.charAt(0);
	                        localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
						
						$window.location.href = '#player';
					}else if(object < $scope.objectprogress1){
					  
					    $scope.loading = true;
						$scope.decry["nid"]=nid;
	                    $scope.decry["object"]=object;
	                    $scope.decry["freenavigation"] =$scope.freenavigation;
	                    $scope.decry["tname"]= $scope.ttitle;
	                    $scope.decry["op"]=$scope.objectprogress1; 
	                    $scope.decry["nuggets"]=$scope.noofnuggets; 
	                    $scope.decry["tp"] = $scope.topic.tp.charAt(0);
	                    localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
						$window.location.href = '#player';
					}else{
						
					}
					  
				}else{
		
				    $scope.loading = true;
					$scope.decry["nid"]=nid;
					$scope.decry["object"]=object;
					$scope.decry["freenavigation"] =$scope.freenavigation;
					$scope.decry["tname"]= $scope.ttitle;
					 $scope.decry["op"]=$scope.objectprogress1; 
					$scope.decry["nuggets"]=$scope.noofnuggets;	
					$scope.decry["tp"] = $scope.topic.tp.charAt(0);
					localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
					$window.location.href = '#player';
				}
			  
			}else{
				
			}
			
		}
	}
		
	};
	
	$scope.getcredentials = function(){
	    
	    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
	       $scope.decry=JSON.parse(decrypted);
	       
	    var fbuser = localStorage.getItem("fbuser");
	   AWSCognito.config.region =  config.reg;
	   AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
	       IdentityPoolId: $scope.decry.iid
	   });
	  
	   var poolData = { UserPoolId : $scope.decry.uid,
	           ClientId : $scope.decry.cid
	       };
	   
	   var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
	   
	   var cognitoUser = userPool.getCurrentUser();
	   
	   if (cognitoUser != null && $scope.decry.id != null) {
	
	       $scope.getsessioncongnito(cognitoUser);

	   } else if(fbuser != null){
	     
	       $scope.getfbdatasession("Crdweb");
	      

	}else {
	  
	       localStorage.clear();
	       $window.location.href = '#page';
	   }
	   
	   
	};
	$scope.getfbdatasession = function(reDirect){
	   
        return new Promise((resolve, reject) => {
        Facebook.getLoginStatus( (response)=> {
              if (response.status == 'connected') {
                 var fbjson = JSON.stringify(response);
                 fbjson = fbjson.toString();
                 var fbtoken = fbjson.accessToken;
                 
                 if(fbtoken == undefined){
                     var fbtoken1 = JSON.parse(fbjson);
                    fbtoken = fbtoken1.authResponse.accessToken;
                 }
              
               var apigClient = apigClientFactory.newClient({});
                  var params1 = {};
                  var body = {type: '1',token : fbtoken,oid : $scope.decry.oid};
                  var additionalParams = {};
                   console.log(JSON.stringify(body));
                  apigClient.getCredentialsWebPost(params1, body, additionalParams)
                  .then(function(result1){
                     
                     var tjson = JSON.stringify(result1.data);
                     tjson = tjson.toString();
                     
                     tjson = JSON.parse(tjson);
             
                     if(reDirect == "subscribe"){
                         $scope.securesubscribeon(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken); 
                     }else if(reDirect == "certClaim"){
                         $scope.claimcertificateon(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken); 
                     }else if(reDirect == "viewCert"){
                         
                     }else if(reDirect == "Crdweb"){
                         $scope.Credentialsweb(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken); 
                     }else{
                         
                     }
                    
                     resolve();   
                      }).catch( function(result){
                          swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
                              allowEscapeKey:false, buttonsStyling: false});
                          localStorage.clear();
                          $window.location.href = '#page';
                      })
              }else{
                      swal({title: "Oooops!", text: "Facebook session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
                          allowEscapeKey:false, buttonsStyling: false});
                      localStorage.clear();
                      $window.location.href = '#page';
              }
              }); 
        })
}
	$scope.getsessioncongnito = function(cognitoUser){
	   
	   return new Promise((resolve, reject) => {
	          cognitoUser.getSession((err, session) =>{
	             if (err) {
	                 swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
	                     allowEscapeKey:false, buttonsStyling: false});
	                 localStorage.clear();
	                 $window.location.href = '#page';
	             }else{
	                 
	                 var token = session.idToken.jwtToken;
	                 $scope.Credentialsweb('a', 'a', 'a',token); 
	         
	         
	             }
	         });
	     })
	}
	$scope.Credentialsweb = function(accessKeyId, secretAccessKey, sessionToken,token){
	  
	    if($scope.decry.api === undefined){
	        
	        var apigClient = apigClientFactory.newClient({
	            
	        });
	    }else{
	        
	        var apigClient = apigClientFactory.newClient({
	            invokeUrl: $scope.decry.api,
	        });
	    }
       var params = {};
       var body = {
               oid: $scope.decry.oid,
               topicid: $scope.tid,
               eventtype: "Topic Viewed"
                };
       var additionalParams = {
               headers: {Authorization : token
               }
         };
   
       apigClient.analyticsWebAppPost(params, body, additionalParams)
       .then(function(result){ 
         
           }).catch( function(result){
               $scope.loading = false;
               $scope.$apply();
               
               
           });
	 
	   
	};
	
	
	
	$scope.nugget = function(){
		
		$scope.topic = topic;
		$scope.selected = -1;
		
		
		 var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
	       $scope.decry=JSON.parse(decrypted);
	       $scope.ctype = $scope.decry.ctype; 
		if($scope.topic == false){
			
    		$scope.errorpage = true;
    		$scope.othererrmsg = true;
            if($scope.online == false){
                $scope.othererrmsg = false;
            }
    		
    	}else{
    	
		if($scope.decry.email == null || $scope.decry.email ==  ''){
		    
			localStorage.clear();
			$window.location.href = '#page';
			
		}else{
		
			$scope.topic = JSON.parse($scope.topic);
			
			$scope.tid = $scope.topic.tid;
			$scope.tp =  $scope.topic.tp;
			$scope.newtopic =  $scope.topic.newtopic;
			$scope.payment =  $scope.topic.paymentrequired;
			$scope.price =  $scope.topic.price;
			$scope.progressmenu = [];
			if($scope.topic.tp!=undefined){
				$scope.progressmenu = $scope.topic.tp.split("-");
			}else
				{
				$scope.progressmenu = 0;
				}
			

			if($scope.newtopic == true){
			    $scope.getcredentials();
			    if($scope.topic.duedate == undefined || $scope.topic.duedate == false || $scope.topic.duedate == 'false'){
	                $scope.duedate = false;
	            }else{
	                $scope.duedate = $scope.topic.duedate;
	                var diff = moment.utc($scope.duedate).fromNow();
	                if(diff.includes("ago")){
	                    diff = 'Overdue';
	                }
	                $scope.duedate = diff;
	            }
			}
			
			
			
			if($scope.tp  == 0){
			
				$scope.completed = 0;
				$scope.comp = 0;
				$scope.objectprogress = 0;
				
				if($scope.topic.duedate == undefined || $scope.topic.duedate == false || $scope.topic.duedate == 'false'){
                    $scope.duedate = false;
                }
    		}else{
    			
    			$scope.tpc = $scope.tp.substring(0, 1);
    			$scope.claimcert = false;
    			$scope.hascert = $scope.topic.certification;
    			if($scope.hascert == 'true' || $scope.hascert == true){
    				$scope.hascert = true;
    			}else{
    				$scope.hascert = false;
    			}
    			
    			if($scope.topic.duedate == undefined || $scope.topic.duedate == false || $scope.topic.duedate == 'false'){
                    $scope.duedate = false;
                }else{
                    $scope.duedate = $scope.topic.duedate;
                    var diff = moment.utc($scope.duedate).fromNow();
                    if(diff.includes("ago")){
                        diff = 'Overdue';
                    }
                    $scope.duedate = diff;
                }
    			if( $scope.tpc == 3){
    				$scope.viewcert = true;
    			}
    			if($scope.tpc == 2 || $scope.tpc == 3){
        				if($scope.tpc == 2 && $scope.hascert == true){
        					
        					$scope.claimcert = true;
        				}
        				$scope.duedate=false;
    				$scope.tpc = 2;
    			}else{
    				$scope.tpc = 1;
    			}
    			
    			$scope.decry["tpc"] = $scope.tpc;
    			$scope.noofnuggets = ($scope.tp.match(/-/g)||[]).length;
    			$scope.comp = 0;
    			
    			for (var k = 2; k < $scope.tp.length; k++){
    				//alert($scope.topics[i].tp.substring(k, (+k + +1)));
    				if($scope.tp.substring(k, (+k + +1)) == 2){
    					$scope.comp++;
    				}
    				
    				k++;
    			}
    			
    			$scope.completed = (($scope.comp/$scope.noofnuggets)*100);
    			
    			if( ($scope.completed == Math.floor($scope.completed))  )
    			{
    				$scope.completed = $scope.completed;
    			}else
    			{
    				var n = $scope.completed.toFixed(2);
    				$scope.completed = n;
    			};
    			$scope.completed = $scope.completed;
    		}
			$scope.ttitle = $scope.topic.ttitle;
			
			$scope.tdescription = $scope.topic.tdescription;
			$scope.noofnuggets = $scope.topic.noofnuggets;
			$scope.tduration = $scope.topic.tduration;
			$scope.nuggets = $scope.topic.nuggets;
			
			$scope.freenavigation = $scope.topic.freenavigation;
			$scope.version = $scope.topic.version;
			$scope.cversion = $scope.topic.cversion;
			if($scope.cversion == true || $scope.cversion == "true"){
				swal({
					  title: 'Topic Updated',
					  text: "Since there was changes to the Topic, the progress for this topic has been reset.",
					  width: '400px',
					  showCancelButton: true, cancelButtonText: 'Ok',
					  showConfirmButton: false,
					});
			}
			if($scope.tp  != 0){
				
				if($scope.tp.substring(0, 1) == 2 && $scope.topic.certification == true){
					$scope.claimcert = true;
				}
				if($scope.tp.substring(0, 1) == 3 && $scope.topic.certification == true){
					$scope.viewcert = true;
				}
    		}
			
			if($scope.freenavigation == false || $scope.freenavigation == 'false'){
				
				$scope.op =  $scope.topic.op;
				if($scope.op == 0){
					$scope.objectprogress = 0;
				}else{
					$scope.noofobjects = ($scope.op.match(/-/g)||[]).length;
					$scope.objectprogress = 0;
	    			
	    			for (var m = 2; m < $scope.op.length; m++){
	    				
	    				if($scope.op.substring(m, (+m + +1)) == 2){
	    					$scope.objectprogress++;
	    				}
	    				
	    				m++;
	    			}
				}
				
			}else{
				$scope.comp = 0;
				$scope.objectprogress = 0;
			}
			$scope.content = true;
			$scope.overview = false;
			$scope.topicimg = config.url+$scope.decry.oid.toLowerCase()+"-resources/images/topic-images/"+$scope.tid+".png";
			$scope.bimage = "url('"+$scope.topicimg+"')";
			$scope.myObj = {'background-color':'grey','overflow':'hidden','background-image':$scope.bimage,'background-size': 'cover','background-repeat': 'no-repeat'};
			$scope.activenugget = $scope.comp;
			
			//$scope.objectprogress = 2;
			$scope.objectprogress1 = $scope.objectprogress;
				
            
            var progress = 0, ocomp = 0, ovid = 0, vprog = 0, sqquiz = 0, sqprog = 0, odoc = 0, dprog = 0, fqquiz = 0, fqprog = 0, asmt = 0, asmtprog = 0;
            
            var lastact;
            var tprogress = [];
            var qscores = [];
     
            $scope.quizscores = $scope.topic.quizscores == undefined || $scope.topic.quizscores == "" || $scope.topic.quizscores == null ? [] : JSON.parse($scope.topic.quizscores) ;
           
            if($scope.quizscores.length > 0){
               
                for(var l=0;l < $scope.quizscores.length;l++){
                    //alert($scope.myapplication.quizscores[l].score+" "+$scope.myapplication.myTopics[i].bpid)
                    Object.keys($scope.quizscores[l]).forEach(function(prop) {
                        if(prop.includes($scope.topic.tid))
                        {
                          qscores.push($scope.quizscores[l])
                        }
                    });
                   
                }
            }
     
            if($scope.topic !== undefined && $scope.topic.tp !== 0){
            
              
                $scope.noofnuggets = ($scope.topic.tp.match(/-/g)||[]).length;
                $scope.comp = 0;
            
               if(($scope.topic.tp.charAt(0) != 1 && ($scope.topic.freenavigation == true || $scope.topic.freenavigation == "true")) || ($scope.topic.freenavigation == false || $scope.topic.freenavigation == "false"))
                   {            
                   
                            for (var k = 2; k < $scope.topic.tp.length; k++){
                                //alert($scope.topics[i].tp.substring(k, (+k + +1)));
                                if($scope.topic.tp.substring(k, (+k + +1)) == 2){
                                    $scope.comp++;
                                }
                               
                                k++;
                            }
                   }
                
                progress = (($scope.topic.cobj/$scope.topic.tobj)*100);
                
                if( (progress == Math.floor(progress))  )
                {
                    progress = progress;
                }else
                {
                    var n = progress.toFixed(2);
                    progress = n;
                };
         
             
                lastact = $scope.topic.lact  ;
      
                if($scope.topic.tvid == 0 || $scope.topic.tvid == '0' || $scope.topic.tvid == undefined){
                    vprog = 'N/A';
                }else{
                    vprog = (+$scope.topic.cvid / +$scope.topic.tvid)*100; 
                     if( (vprog == Math.floor(vprog)) )
                     {
                        vprog = vprog;
                     }else
                     {
                         vprog = vprog.toFixed(2);
                     };
                }
        
                if($scope.topic.tasmt == 0 || $scope.topic.tasmt == '0' || $scope.topic.tasmt == undefined){
                    asmtprog = 'N/A';
                }else{
                    asmtprog = (+$scope.topic.casmt / +$scope.topic.tasmt)*100; 
                     if( (asmtprog == Math.floor(asmtprog)) )
                     {
                         asmtprog = asmtprog;
                     }else
                     {
                     asmtprog = asmtprog.toFixed(2);
                     };
                }
            
                if($scope.topic.tdoc == 0 || $scope.topic.tdoc == '0' || $scope.topic.tdoc == undefined){
                    dprog = 'N/A';
                }else{
                    dprog = (+$scope.topic.cdoc / +$scope.topic.tdoc)*100; 
                     if( (dprog == Math.floor(dprog)) )
                     {
                         dprog = dprog;
                     }else
                     {
                         dprog = dprog.toFixed(2);
                     };
                }
          
                if($scope.topic.tsq == 0 || $scope.topic.tsq == '0' || $scope.topic.tsq == undefined){
                    sqprog = 'N/A';
                }else{
                    sqprog = (+$scope.topic.csq / +$scope.topic.tsq)*100; 
                     if( (sqprog == Math.floor(sqprog)) )
                     {
                         sqprog = sqprog;
                     }else
                     {
                     sqprog = sqprog.toFixed(2);
                     };
                }

                if($scope.topic.tfq == 0 || $scope.topic.tfq == '0' || $scope.topic.tfq == undefined){
                    fqprog = 'N/A';
                }else{
                    fqprog = (+$scope.topic.cfq / +$scope.topic.tfq)*100; 
                     if( (fqprog == Math.floor(fqprog)) )
                     {
                         fqprog = fqprog;
                     }else
                     {
                     fqprog = fqprog.toFixed(2);
                     };
                }
                
             
            }
         
            tprogress.push({input: 'Completed',value: progress});
            var notcomp = 100 - +progress;
            tprogress.push({input: 'Not Completed',value: notcomp});
            if($scope.topic.tvid == 0 || $scope.topic.tvid == '0'){
                vprog = 'N/A';
            }
            if($scope.topic.tdoc == 0 || $scope.topic.tdoc == '0'){
                dprog = 'N/A';
            }
            if($scope.topic.tsq == 0 || $scope.topic.tsq == '0'){
                sqprog = 'N/A';
            }
            if($scope.topic.tfq == 0 || $scope.topic.tfq == '0'){
                fqprog = 'N/A';
            }
            if($scope.topic.tasmt == 0 || $scope.topic.tasmt == '0'){
                asmtprog = 'N/A';
            }
           
            $scope.topic.vprog = vprog;
            $scope.topic.dprog = dprog;
            $scope.topic.sqprog = sqprog;
            $scope.topic.fqprog = fqprog;
            $scope.topic.asmtprog = asmtprog;
            $scope.topic.progress = progress;
           
            var date = new Date($scope.topic.sd * 1000);
            var monthNames = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];
            var year = date.getFullYear();
            var month = monthNames[date.getMonth()];
            var date1 = date.getDate();
            var sdate = date1+" "+month+", "+year;
            $scope.topic.sdate = sdate;
            if(lastact == undefined){
                $scope.topic.ldate = '-';
            }else{
                date = new Date(lastact * 1000);
                year = date.getFullYear();
                month = date.getMonth() + 1;
                date1 = date.getDate();
                sdate = date1+" "+month+", "+year;
                
                $scope.topic.ldate = moment.utc(date).fromNow();;
            }
            
            $scope.topic.tableParams = new NgTableParams({
                page: 1,            // show first page
                count: 10          // count per page
            }, {
              dataset:   qscores
            });
            
            $scope.topic.options2 = {
                     chart: {

                         type: 'pieChart',
                         height:190,
                         donut: true,
                         donutRatio: 0.7,
                         x: function(d){return d.key;},
                         y: function(d){return d.y;},
                         showLabels: false,
                         showLegend:false,
                         pie: {
                             startAngle: function(d) { return d.startAngle },
                             endAngle: function(d) { return d.endAngle }
                         },
                         duration: 500,
                         legend: {
                             maxKeyLength: 100,
                             width: 100,
                             margin: {
                                 top: 5,
                                 right: 40,
                                 bottom: 0,
                                 left: 130
                             },
                             rightAlign: true, 
                             align: true
                         },
                         margin: {
                             top: 0,
                             right: 0,
                             bottom: 0,
                             left: 10
                         }, title:  progress + "%"
                             
                         ,
                         legendPosition: "bottom",
                         labelsOutside: false,
                         padAngle: 0.01
                     }
                 };
    
            $scope.topic.data2 = [{
                     key: 'Completed',
                     y: progress,
                     color: '#11316C'
                 },
                 {
                     key: 'Not Completed',
                     y: (100 - +progress),
                     color: "grey"
                 }];
            
          
            $scope.topic.options = {
                    chart: {
                        type: 'multiBarHorizontalChart',
                        height: 190,
                        x: function(d){return d.label;},
                        y: function(d){return d.value;},            
                        showControls: false,
                        showValues: false,       
                        showLegend: false,
                        groupSpacing: .4,
                        yDomain: [0,100],
                        margin: {
                            left: 140,
                            bottom: 60,
                            top:0
                          },
                        xAxis: {
                            showMaxMin: false
                        },
                        yAxis: {
                         
                            showMaxMin: true
                        }
                    }
                };
         
            $scope.topic.data  = [
                {              
                    "color": "#B3BCC1",
                    "values": [  {
                            "label" : "Videos/Audios" ,
                            "value" : vprog
                        }
                    ]
                
                },  {
                   
                    "color": "#D3B0B0",
                    "values": [  {
                        "label" : "Documents" ,
                        "value" : dprog
                        }
                    ]
                
                }
                ,  {
                   
                    "color": "#ABCCE8",
                    "values": [  {
                        "label" : "Knowledge Checks" ,
                        "value" : fqprog
                        }
                    ]
                
                }
                ,  {
                   
                    "color": "#626262",
                    "values": [  {
                        "label" : "Assessments" ,
                        "value" : sqprog
                        }
                    ]
                
                }
                ,  {
                   
                    "color": "#d62728",
                    "values": [  {
                        "label" : "Assignments" ,
                        "value" : asmtprog
                        }
                    ]
                
                }
                
                ]
          
          
			
		};
		$scope.prog = $scope.topic;
		$scope.moverview = true;
		  $scope.tableParams = new NgTableParams({
	            page: 1,            // show first page
	            count: 10          // count per page
	        }, {
	          dataset:  $scope.prog
	        });
		  
		if($window.innerWidth > 767){
			$scope.showapppage = false;
			$scope.showpage = true;
	    	
		}else{
			$scope.showpage = false;
			$scope.showapppage = true;
			
		}
		
		$(window).resize(function() {
		      $scope.$apply(function() {
		        $scope.windowWidth = $( window ).width();
		        if($scope.windowWidth < 767){
		        	$scope.showpage = false;
		        	$scope.showapppage = true;
		        	swal.close();
		        	 $scope.Instance.close(true);
		        }
		        if($scope.windowWidth > 767	){
		        	$scope.showpage = true;
		        	$scope.showapppage = false;
		        }
		      });
		    });
    	}
		  $scope.$watch('online', function() {
		        if($scope.online == true){
		            if($scope.errorpage == true){
		                $scope.loading = true;
		                $window.location.href = '#topic';
		            }
		        }else{
		            $scope.loading = false;
		        }
		     });
		 window.navigating = false;
		 $scope.secondwidth = +$window.innerWidth - +272;
	        $scope.second = {'width':$scope.secondwidth};      
	        $scope.topicclass = { 'margin-left' : '15%' };
	        $scope.selected = 0;
	
	};
	
	
	$scope.nugget();
	
	$scope.tabfun = function(val)
	{
	    $scope.topicclass = { 'margin-left' : '0%' }
	    if(val == "c")
	        {
	          $scope.topicclass = { 'margin-left' : '18%' }
	        }
	            
	   
	}
	$window.onscroll = function (){
	    
	    for(var k=0;k< $scope.topic.nuggets.length; k++)
        {
	       
           if(window.pageYOffset >= document.getElementById($scope.topic.nuggets[k].ntitle).offsetTop - 80)
               {
                   $scope.selected = k;
                   $scope.$digest();
               }
           
         
        }
	
	  
	}
	$scope.scrollfunction =function(unitname,index) {
	       
        var testDiv = document.getElementById(unitname);     
         if($('#test1').css('display') == 'none')
         {
             window.scroll(0, testDiv.offsetTop - 10);
         }else
         {
             window.scroll(0, testDiv.offsetTop - 80);
         }

      $scope.selected = index;
    }
	$scope.mytopics = function(){
		if($scope.online == true){
		    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
	        $scope.decry=JSON.parse(decrypted);
	        
			if($scope.decry.osearch == true || $scope.decry.osearch == "true"){
				$scope.loading = true;
				delete $scope.decry["osearch"];
				localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
				$window.location.href = '#searchtopic';
			}else{
				$scope.loading = true;
				delete $scope.decry["otopicid"];
                localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
				$window.location.href = '#home';
			}
		
		}
	}
	$scope.subscribe = function(){
		
		if($scope.online == true){
		swal({
			  text: "Do you wish to start the topic?",
			  type: 'warning',
			  width: '400px',
			  showCancelButton: true, cancelButtonText: 'No, cancel !',
			  showConfirmButton: true, confirmButtonText: 'Yes, Start !', confirmButtonColor: "#f5a138", confirmButtonClass: 'warnclass',
			  customClass: 'sweetalert-lgs',
			  confirmButtonClass: 'confirmClass',
			  buttonsStyling: false,
			  cancelButtonClass: 'cancelClass',
			  allowOutsideClick: false
			}).then((result) => {
			  if (result.value) {
			      if($scope.online == true){
    				  $scope.loading = true;
    				  $scope.$apply();
    					$scope.subscribeon();
			      }
			  } else if (
			    result.dismiss === swal.DismissReason.cancel
			  ) {
			  }
			})
		}
	}
	$scope.subscribeon = function(){
	    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
        $scope.decry=JSON.parse(decrypted);
        
		 var fbuser = localStorage.getItem("fbuser");
		AWSCognito.config.region =  config.reg;
	    AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
	        IdentityPoolId: $scope.decry.iid
	    });
	   
		var poolData = { UserPoolId : $scope.decry.uid,
		        ClientId : $scope.decry.cid
		    };
		
		var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
		
		var cognitoUser = userPool.getCurrentUser();
		
	    if (cognitoUser != null && $scope.decry.id != null) {
	    	$scope.getsession(cognitoUser, "subscribe");

	    } else if(fbuser != null){
	    	$scope.getfbdatasession("subscribe");

    }else {
      
	    	localStorage.clear();
	    	$window.location.href = '#page';
	    }
	    
		
	}
	
	$scope.getsession = function(cognitoUser, reDirect){
		
		  return new Promise((resolve, reject) => {
	 			 cognitoUser.getSession((err, session) =>{
	 	            if (err) {
				    	swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
			                allowEscapeKey:false, buttonsStyling: false});
	 	            	localStorage.clear();
	 	            	$window.location.href = '#page';
	 	            }else{
	 	            	
	 	            	var token = session.idToken.jwtToken;
	 	               if(reDirect == "subscribe"){
                          
                           $scope.securesubscribeon('a', 'a', 'a',token); 
                           
                       }else if(reDirect == "certClaim"){
                           $scope.claimcertificateon('a', 'a', 'a',token); 
                       }else if(reDirect == "viewCert"){
                           
                       }else
                           {
                           
                           }
                       
                       resolve();
	 	        
	 	        
	 	            }
	  		  	});
	 		})
	}
	
	
	
	$scope.securesubscribeon = function(accessKeyId, secretAccessKey, sessionToken,token){
	    if($scope.decry.api === undefined){
	        
	        var apigClient = apigClientFactory.newClient({
	            
	        });
	    }else{
	        
	        var apigClient = apigClientFactory.newClient({
	            invokeUrl: $scope.decry.api,
	        });
	    }
    	var params = {};
    	var logintype;
    	if(localStorage.getItem("fbuser") != null){
    		logintype = "Facebook";
    	}else{
    		logintype = "Cognito";
    	}
		var body = {
				id: $scope.decry.id,
				iid: $scope.decry.iid,
				status: "new",
				key: $scope.tid,
				version: $scope.version,
				tnuggets: $scope.nuggets.length,
				nav: $scope.freenavigation,
				email: $scope.decry.email,
				logintype: "Cognito"
				 };
	
        if($scope.topic.tobj != undefined){
            body.tobj = $scope.topic.tobj;
            body.tvid = $scope.topic.tvid;
            body.tdoc = $scope.topic.tdoc;
            body.tfq = $scope.topic.tfq;
            body.tsq = $scope.topic.tsq;
            body.tasmt = $scope.topic.tasmt;
        }
		
		 var additionalParams = {
	               headers: {Authorization : token
	               }
	         };
	
		apigClient.syncUserDataWebPost(params, body, additionalParams)
		.then(function(result){
                		    if($scope.decry.api === undefined){
                		        
                		        var apigClient = apigClientFactory.newClient({
                		            
                		        });
                		    }else{
                		        
                		        var apigClient = apigClientFactory.newClient({
                		            invokeUrl: $scope.decry.api,
                		        });
                		    }
                				
							var params = {};
							var body = {
									oid: $scope.decry.oid,
									topicid: $scope.tid,
									eventtype: "Topic Subscribed",
									email: $scope.decry.email,
									id: $scope.decry.id,
									gender: "Unknown",
									logintype: "Cognito"
									 };
							var additionalParams = {
					                   headers: {Authorization : token
					                   }
					             };
							
							apigClient.analyticsWebAppPost(params, body, additionalParams)
							.then(function(result){	
							    
								$window.location.href = '#topic';
							    }).catch( function(result){
							    	$scope.loading = false;
							    	$scope.$apply();
							    	
							    });
			
			
		    	
		    }).catch( function(result){
		    	$scope.loading = false;
		    	$scope.$apply();
		    	var json = JSON.stringify(result);
		    	var json1 = json.toString();
		    	alert('ERROR while Sync operation');
		    	
		    	
		    });
		
	};
	
	$scope.subscribeevent = function(){
		
	};
	
	$scope.claimcertificate = function(){
	    
	    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
        $scope.decry=JSON.parse(decrypted);
        
	    $scope.loading = true;
        var fbuser = localStorage.getItem("fbuser");
       AWSCognito.config.region =  config.reg;
       AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
           IdentityPoolId: $scope.decry.iid
       });
      
       var poolData = { UserPoolId : $scope.decry.uid,
               ClientId : $scope.decry.cid
           };
       
       var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
       
       var cognitoUser = userPool.getCurrentUser();
       
       if (cognitoUser != null && $scope.decry.id != null) {
           $scope.getsession(cognitoUser, "certClaim");
         
       } else if(fbuser != null){
           $scope.getfbdatasession("certClaim");

   }else {
      
           localStorage.clear();
           $window.location.href = '#page';
       }
      
	}
	$scope.claimcertificateon = function(accessKeyId, secretAccessKey, sessionToken,token){
		
                	    if($scope.decry.api === undefined){
                	        
                	        var apigClient = apigClientFactory.newClient({
                	            
                	        });
                	    }else{
                	        
                	        var apigClient = apigClientFactory.newClient({
                	            invokeUrl: $scope.decry.api,
                	        });
                	    }
			
					var params = {};
					 var additionalParams = {
				               headers: {Authorization : token
				               }
				         };
					var timestamp = Math.round((new Date()).getTime() / 1000);  
					var body = {
							oid: $scope.decry.oid,
							userfullname : $scope.decry.username,
							topicid: $scope.tid,
							userid: $scope.decry.id,
							eid: $scope.decry.email,
							completiondate: timestamp
							};
					if($scope.ctype == 0 || $scope.ctype == '0'){
						apigClient.generateUserCertPost(params, body, additionalParams)
						.then(function(result){
						    	
							var body1 = {
									oid: $scope.decry.oid,
									iid: $scope.decry.iid,
									id: $scope.decry.id,
									key: $scope.tid
									 };
							var abc = apigClient.syncUserDataCertWebPost(params, body1, additionalParams)
							.then(function(result){
								 swal({title: "",type: "success", text: "Certificate has been claimed successfully.", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
						                allowEscapeKey:false, buttonsStyling: false});
								$window.location.href = '#topic';
								
							    	
							    }).catch( function(result){
							    	$scope.loading = false;
							    	$scope.$apply();
							    	var json = JSON.stringify(result);
							    	var json1 = json.toString();
							    
							    	alert('ERROR while sync operation');
							    	
							    	
							    });

						    	
						    }).catch( function(result){
						    	var json = JSON.stringify(result);
						    	var json1 = json.toString();
						   
						    	alert('ERROR'+json1);
						    	
						    });
					}else if($scope.ctype == 1 || $scope.ctype == '1'){
						apigClient.claimBadgePost(params, body, additionalParams)
						.then(function(result){
						    	
							var body1 = {
									oid: $scope.decry.oid,
									iid: $scope.decry.iid,
									id: $scope.decry.id,
									key: $scope.tid
									 };
							var abc = apigClient.syncUserDataCertWebPost(params, body1, additionalParams)
							.then(function(result){
								 swal({title: "",type: "success", text: "Badge has been claimed successfully.", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
						                allowEscapeKey:false, buttonsStyling: false});
								$window.location.href = '#topic';
								
							    	
							    }).catch( function(result){
							    	$scope.loading = false;
							    	$scope.$apply();
							    	var json = JSON.stringify(result);
							    	var json1 = json.toString();
							    
							    	alert('ERROR while sync operation');
							    	
							    	
							    });

						    	
						    }).catch( function(result){
						    	var json = JSON.stringify(result);
						    	var json1 = json.toString();
						   
						    	alert('ERROR'+json1);
						    	
						    });
					}else{
						
					}
						
		
	};

	$scope.viewcertificate = function(){
	    $scope.loading = true;
	    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
	     $scope.decry=JSON.parse(decrypted);
	     $scope.decry["ctopicid"] = $scope.tid
	     localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
		
			$scope.Instance = $uibModal.open({
			templateUrl: 'cimage.html',
			controller: 'cimageCtrl',
			backdrop: 'static',
	        keyboard: false,
	        windowClass: 'cimagemodal',
	        scope: $scope,
	        resolve: {
	        	cimage: function(getcimage){
	                return getcimage.getcimage();
	        }
	        }
			});
			$scope.Instance.opened.then(function () {
				$scope.loading = false;
		    });
			 $scope.Instance.result.then(function (quiz) {
				 
			    }, function () {
			     // alert($scope.object);
			    });

	};


};

app.controller('nuggetCtrl', nuggetCtrl);
nuggetCtrl.$inject = ['$scope', '$http', '$location', '$window', '$uibModal', 'topic', 'config','$crypto','NgTableParams'];


app.factory("getTopic", function($window, $q, config,$crypto){
    return {
    	getTopic: function(){
    	  
            
    		if(localStorage.getItem("740a2y1e") !=undefined){
    		    
    		    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
                var decry=JSON.parse(decrypted);
    		var topicdata;
    		
    		AWSCognito.config.region =  config.reg;
    	    AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
    	        IdentityPoolId: decry.iid
    	    });
    	   
    		var poolData = { UserPoolId : decry.uid,
    		        ClientId : decry.cid
    		    };
    		
    		var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
    		 var fbuser = localStorage.getItem("fbuser");
    		var cognitoUser = userPool.getCurrentUser();
    	    if (cognitoUser != null && decry.id != null) {
    	    	topicdata = getusersession();
  
    	    } else if(fbuser != null && decry.id != null){
    	    	
    	    	topicdata = getfbusersession();

	    }else {
	       
    	    	localStorage.clear();
    	    	$window.location.href = '#page';
    	    }
    	    function getdata(accessKeyId, secretAccessKey, sessionToken, token){
    	        
    	        if(decry.api === undefined){
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                
    	            });
    	        }else{
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                invokeUrl: decry.api,
    	            });
    	        }
    	    
    	       
			  var params = {};
			    var additionalParams = {
                        headers: {Authorization : token
                        }
                  };
			  
			  var body = {
						id : decry.id,
						iid : decry.iid
						 };

			  var topics =	apigClient.getUserDataWebPost(params, body, additionalParams)
				.then(function(result){
				  
			    if(decry.otopicid == undefined)
                {
			        $window.location.href = '#page';
                 }else{
				   var json = JSON.stringify(result.data);
				    var topic = json.toString();
				
				    if(topic.substring(1, 10) == "No Topics"){
				    	var newtopic = true;
		    		 	var tp = 0;
		    			var opk = 0;
    					var op = 0;
    					var cobj = 0;
                        var cvid = 0;
                        var cdoc = 0;
                        var cfq = 0;
                        var csq = 0;                                    
                        var casmt = 0;
    					var body1 = {
    							eid: decry.email,
    							oid: decry.oid,
				    		 	topicid: decry.otopicid
						 	};
    			
							var topics1 =	apigClient.getTopicPost(params, body1, additionalParams)
								.then(function(result){
								   
								   var json = JSON.stringify(result.data);
							
								    var topicjson = json.toString();
								    topicjson = JSON.parse(topicjson);
								    topicjson.newtopic = newtopic;
								    topicjson.tp = tp;
								    topicjson.opk = opk;
								    topicjson.op = op;
								    topicjson.cobj = cobj;
                                    topicjson.cvid = cvid;
                                    topicjson.cdoc = cdoc;
                                    topicjson.cfq = cfq;
                                    topicjson.csq = csq;
                                    topicjson.casmt = casmt;
                                    topicjson.quizscores = 0;
								    topicjson = JSON.stringify(topicjson);
							
								    return $q.when(topicjson);
								    	
								    }).catch( function(result){
								        alert(JSON.stringify(result));
								    	return $q.when(false);
								    	
								    });
				    	return topics1;
				    }else{
				    	var version = false;
				    	 topic = JSON.parse(topic);
				    	 var  topicids = [];
				    	 var tp = [];
				    	 var subscribed = false;
				    	   
				    	 for(var i =0;i < topic.Records.length;i++ ){
				    		if(topic.Records[i].Key == decry.otopicid){
				    			
				    			var abc = topic.Records[i].Value;
					    		var bcd  = JSON.parse(abc);
					    		if(bcd.tp == 0){
					    			
					    			var tp = 0;
					    			var opk = 0;
			    					var op = 0;
                                    var cobj = 0;
                                    var cvid = 0;
                                    var cdoc = 0;
                                    var cfq = 0;
                                    var csq = 0;                                    
                                    var casmt = 0;
					    		}else{
					    			var op = 0;
					    			for(var k = 0; k < Object.keys(bcd).length; k++){
					    				
					    				if(Object.keys(bcd)[k] == "ver"){
					    						version = bcd.ver;
					    				}else if(Object.keys(bcd)[k] == "sd"){
					    					//do nothing
					    				}else if(Object.keys(bcd)[k] == "tp"){
					    					var tp = bcd.tp;
					    				}else if(Object.keys(bcd)[k] == "cobj"){
                                            var cobj = bcd.cobj;
                                        }else if(Object.keys(bcd)[k] == "cvid"){
                                            var cvid = bcd.cvid;
                                        }else if(Object.keys(bcd)[k] == "cdoc"){
                                            var cdoc = bcd.cdoc;
                                        }else if(Object.keys(bcd)[k] == "cfq"){
                                            var cfq = bcd.cfq;
                                        }else if(Object.keys(bcd)[k] == "csq"){
                                            var csq = bcd.csq;
                                        }else if(Object.keys(bcd)[k] == "casmt"){
                                            var casmt = bcd.casmt;
                                        }else if(Object.keys(bcd)[k].includes(decry.otopicid)){
					    					
					    					var opk = Object.keys(bcd)[k];
					    					var op = bcd[Object.keys(bcd)[k]];
					    				}
					    			}
					    		}
					    		subscribed = true;
					    		var newtopic = false;
				    		}
				    	
				    	
				    	 }
				    	 if(subscribed == false){
				    		 var newtopic = true;
				    		 	var tp = 0;
				    			var opk = 0;
		    					var op = 0;
		    					var cobj = 0;
                                var cvid = 0;
                                 var cdoc = 0;
                                var cfq = 0;
                                var csq = 0;                                    
                                var casmt = 0;
				    	 }
				    	 var body1 = {
				    			eid: decry.email,
				    		 	topicid: decry.otopicid,
				    		 	oid: decry.oid,
						 	};
				    	
							var topics1 =	apigClient.getTopicPost(params, body1, additionalParams)
								.then(function(result){
								    
								    var json = JSON.stringify(result.data);
								    var topicjson = json.toString();
								   
								    topicjson = JSON.parse(topicjson);
								    var versionchanged = false;
								    if(version != false && version != topicjson.version){
								    	if(tp.substring(0, 1) == 2 || tp.substring(0, 1) == 3){
								    			// do nothing
								    		}else{
								    			tp = 0;
								    			opk = 0;
						    					op = 0;
						    					cobj = 0;
			                                    cvid = 0;
			                                    cdoc = 0;
			                                    cfq = 0;
			                                    csq = 0;                                    
			                                    casmt = 0;
						    					versionchanged = true;
						    					updateVersionChange('a', 'a', 'a', topicjson.version, topicjson.tid,token);
								    		}
								    }
								    
								    topicjson.newtopic = newtopic;
									
								    topicjson.tp = tp;
								    topicjson.opk = opk;
								    topicjson.op = op;
								    topicjson.cobj = cobj;
								    topicjson.cvid = cvid;
								    topicjson.cdoc = cdoc;
								    topicjson.cfq = cfq;
								    topicjson.csq = csq;
								    topicjson.casmt = casmt;
								    topicjson.sversion = version;
								    topicjson.cversion = versionchanged;
								    //topicjson = JSON.stringify(topicjson);
								    var body = {
                                            oid: decry.oid,
                                            eid: decry.email
                                                };
								    var topics2 = apigClient.getAllQuizScoresPost(params, body, additionalParams)
                                    .then(function(result){
                                        if(result.data.length != 0  ){
                                            var json = JSON.stringify(result.data);
                                            topicjson.quizscores = json;
                                        }else
                                        {
                                            topicjson.quizscores = [];
                                           
                                         }
                                      
                                        topicjson = JSON.stringify(topicjson);
                                    
                                        return topicjson;
                                    }).catch( function(result){
                                        topicjson = JSON.stringify(topicjson);
                                        return $q.when(topicjson);
                                        
                                    })
                               
                                return $q.when(topics2);
								    
								    
								  
								    	
								    }).catch( function(result){
								    	return $q.when(false);
								    	
								    });
				    	return topics1;
				    }
				}
				     }).catch( function(result){
				    	
				    	return $q.when(false);
				    	
				    });
			  return $q.when(topics);
    	    }
    	    function gettoken(token, id){
    	        if(decry.api === undefined){
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                
    	            });
    	        }else{
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                invokeUrl: decry.api,
    	            });
    	        }
    				var params1 = {};
    				var body = {type: id,token : token,oid : decry.oid};
    				var additionalParams = {};
    				var abcd = apigClient.getCredentialsWebPost(params1, body, additionalParams)
    				.then(function(result1){
    				   var tjson = JSON.stringify(result1.data);
    				   tjson = tjson.toString();
    				  
    				   tjson = JSON.parse(tjson);
    				   var abc = getdata(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken); 
    				  
    				    return $q.when(abc);
    				    	
    				    }).catch( function(result){
    				    	swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
    			                allowEscapeKey:false, buttonsStyling: false});
    		        		localStorage.clear();
    		    	    	$window.location.href = '#page';
    				    	
    				    })
    				return $q.when(abcd);
    	 	   }
    	    function getusersession(){
    	    	 return new Promise((resolve, reject) => {
    	 			 cognitoUser.getSession((err, session) =>{
    	 	            if (err) {
    				    	swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
    			                allowEscapeKey:false, buttonsStyling: false});
    	 	            	localStorage.clear();
    	 	            	$window.location.href = '#page';
    	 	            }else{
    	 	            	//alert(session)
    	 	            	var token = session.idToken.jwtToken;
    	 	            	
    	 	            	//var abcc = gettoken(token, '0');
    	 	            	var abcc = getdata('a', 'a','a',token);
    	 	            	resolve(abcc)
    	 	            	return $q.when(abcc);
    	 	        
    	 	            }
    	  		  	});
    	 		})
    	    }
    	    function getfbusersession(){
    	 		  return new Promise((resolve, reject) => {
    	 			 Facebook.getLoginStatus( (response)=> {
    	 	 			if (response.status == 'connected') {
    	 	 		       var fbjson = JSON.stringify(response);
    	 	 		       fbjson = fbjson.toString();
    	 	 			   var fbtoken = fbjson.accessToken;
    	 	 			   if(fbtoken == undefined){
    	 	 				   var fbtoken1 = JSON.parse(fbjson);
    	 	 				  fbtoken = fbtoken1.authResponse.accessToken;
    	 	 			   }
    	 	 			   	var abcc = gettoken(fbtoken, '1');
    	 	 			 	resolve(abcc)
    		            	return $q.when(abcc);
    	 	 			}else{
    					    	swal({title: "Oooops!", text: "Facebook session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
    					    	localStorage.clear();
    					    	$window.location.href = '#page';
    	 	 			}
    	 	 			}); 
    	 		  })
    	 			
    	 	   }
    	    function updateVersionChange(accessKeyId, secretAccessKey, sessionToken, nversion, topicid,token){
    	        if(decry.api === undefined){
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                
    	            });
    	        }else{
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                invokeUrl: decry.api,
    	            });
    	        }
    	    
			  var params = {};
			  var additionalParams = {
                      headers: {Authorization : token
                      }
                };
    	    	var body1 = {
    	    			key: topicid,
    	    			id : decry.id,
						iid : decry.iid,
		    		 	version: nversion
				 	};
		    	
					apigClient.dataSyncTopicVersionChangeWebPost(params, body1, additionalParams)
						.then(function(result){
							
						    
						    //return $q.when(topicjson);
						    	
						    }).catch( function(result){
						    	//return $q.when(false);
						    	
						    });
  	 		}
    	    return $q.when(topicdata);
    	}else{
    	   
    		localStorage.clear();
	    	$window.location.href = '#page';
    	}
        }
    };
});
