'use strict';

var quizCtrl = function ($scope, $http, $location, $window, $uibModalInstance, quizjson, config ,$crypto) {
	$scope.defaultConfig = {
	
	};
	$scope.goTo = function (index) {

		if(index == $scope.qitems){
		
			$scope.isnext = false;
			$scope.isenable = false;
			 if($scope.jsobj.qtype == "0" && $scope.jsobj.qtype != undefined)
	            {
	                $scope.submit = false;
	           
	            }else
	                {
	                $scope.submit = true;
	                }
		}
		
        if (index > 0 && index <= $scope.qitems) {
            $scope.currentPage = index;
            $scope.inProgress = true;
            $scope.isenable = false;
        }
    };

    
      $scope.pageload = function(){
      
          var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
          $scope.decry=JSON.parse(decrypted);
          
    	  $scope.imgurl = config.url+$scope.decry.oid.toLowerCase()+"-resources/images/quiz-images/"+$scope.decry.quizid.substring(0, 6)+"/";
    	
    	  $scope.jsobj = quizjson;
    	  if($scope.jsobj == false){
			  swal({title: "Oooops!", text: "Something went wrong. Please try again!!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "orange"});
    		  $uibModalInstance.close('0');
    			
    		}else{
    		  
    	$scope.jsobj = JSON.parse(quizjson);
    
    	$scope.inProgress = true;
       	$scope.isnext = true;
       	$scope.quiz = $scope.jsobj.qid;
   
       
       	$scope.qitems = $scope.jsobj.qitems.length;
       	
       	$scope.cutoff=$scope.jsobj.cutoff != undefined ?$scope.jsobj.cutoff : 75;
       	
       	$scope.questions = $scope.jsobj.qitems;
       	 $scope.itemsPerPage = 1;
       	 $scope.currentPage = 1;
       	 $scope.score = 0;
       	 $scope.submit = false;
       	 $scope.showquestion = true;
       	 $scope.quizOver = false;
       	 $scope.isenable = false;
           
         $scope.qlen = $scope.jsobj.qitems.length;
      
         
        if(($scope.jsobj.qtype == "1" || $scope.jsobj.qtype == 1) && $scope.jsobj.qtype != undefined)
        {
            $scope.cutoff = 0;
        }
        
        if($scope.jsobj.qtype == "0" && $scope.jsobj.qtype != undefined)
        {
            $scope.submit = false; 
            $scope.issnext = true;
            $scope.isnext = false;
            }else
                {
                $scope.isnext = true;
                $scope.issnext = false;
                }
        if ($scope.qlen == '1' || $scope.qlen == 1) {
            $scope.submit = true;         
            $scope.isnext = false;
            $scope.isenable = false;
        } 
       	$scope.$watch('currentPage + itemsPerPage', function () {
			$scope.showimage = false;
       			var begin = (($scope.currentPage - 1) * $scope.itemsPerPage),
                 end = begin + $scope.itemsPerPage;
       			
			if ($scope.jsobj.qitems[begin].image == 'true' || $scope.jsobj.qitems[begin].image == true) {
            	  
            	  $scope.showimage = true;
            	  $scope.imageurl = $scope.jsobj.qitems[begin].imageurl;
			}
               $scope.filteredQuestions = $scope.questions.slice(begin, end);
               $scope.completed = (($scope.currentPage/$scope.qitems)*100);
           });
       	
       	if($window.innerWidth > 767){
    		$scope.showapppage = false;
    		$scope.showpage = true;
        	
    	}else{
    		$scope.showpage = false;
    		$scope.showapppage = true;
    		
    	}
    	$(window).resize(function() {
    		
    	      $scope.$apply(function() {
    	        $scope.windowWidth = $( window ).width();
    	        if($scope.windowWidth < 767){
    	        	$scope.showpage = false;
    	        	$scope.showapppage = true;
    	        }
    	        if($scope.windowWidth > 767	){
    	        	
    	        	$scope.showpage = true;
    	        	$scope.showapppage = false;
    	        }
    	      });
    	    });
    	}
    	  $scope.shortans = "";
    };
 
   $scope.pageload();
   $scope.checkisCorrect = function (question,index) {
       $scope.cwicon=true;
       $scope.issnext=false;
       $scope.isnext=true;
       $scope.iisenable = true; 
      
       if(index == $scope.qitems){
               $scope.submit = false;
               $scope.frsubmit=true;
               $scope.isnext=false;
           
       }
       $scope.showPopover = true;
   
     };
   
   $scope.isCorrect = function (question) {
      
       if(question.questiontype == true)
       {
     
          question.iopts.forEach(function (option, index, array) {
          
          
              if (option.content.toLowerCase() === question.shortans.toLowerCase()) {
                  option.Selected = true;
              }
         
          });
       }
       
       question.iopts.forEach(function (option, index, array) {
    	   
           if ((option.Selected) == true || (option.Selected) =='true') {
        	   $scope.score++;
        	   
           }
           option.Selected = '';
           question.shortans = '';
       });
       if($scope.jsobj.qtype == "0" && $scope.jsobj.qtype != undefined )
       {
           $scope.cwicon=false;
           $scope.issnext = true;
           $scope.isnext=false;
           $scope.iisenable = false; 
       }
   };
   
   $scope.onsubmit = function (question) {
	   
	   $scope.showquestion = false;
	   $scope.quizOver = true;
	   $scope.inProgress = false;
	   var ans = (($scope.score/$scope.qitems)*100);
		if( (ans == Math.floor(ans))  )
		{
			$scope.per = ans;
		}else
		{
			var n = ans.toFixed(2);
			$scope.per = n;
		};
		
	   //$window.location = 'quizcompletestatus:ios:'+$scope.per;
	   
   };
   $scope.reset = function() {
	   $scope.pageload();
   };
   

   $scope.change = function (question, option) {
   	$scope.isenable = true;
   	
           question.iopts.forEach(function (element, index, array) {
               if (element.content != option.content) {
                   element.Selected = false;
               }
           });
       
   };
   
   
   
	$scope.exitquiz = function(){
	    var res={rs:'0'};
		$uibModalInstance.close(res);
	};
	$scope.exitquizcmpl=function()
	{	 
	    var res={ rs:'1',qtype: $scope.jsobj.qtype};
	   $uibModalInstance.close(res);
	}
	$scope.complete = function()
	{	
	    var res={ rs:'1',score: $scope.per,qtype:$scope.jsobj.qtype };
    $uibModalInstance.close(res);		
    };
   
   };

quizCtrl.$inject = ['$scope', '$http', '$location', '$window', '$uibModalInstance', 'quizjson','config','$crypto'];
app.controller('quizCtrl', quizCtrl);


app.factory("quizService", function($window, $q, config ,$crypto){
    return {
        getquiz: function(){
            
        if(localStorage.getItem("740a2y1e") == null || localStorage.getItem("740a2y1e") ==  ''){
            
            $window.location.href = '#page';
            
        }else
            {
            
            var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
            var decry=JSON.parse(decrypted);
           
			var quizjs;
			
			var quizid = decry.quizid;
			var fbuser = localStorage.getItem("fbuser");
			AWSCognito.config.region =  config.reg;
    	    AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
    	        IdentityPoolId: decry.iid
    	    });
    	   
    		var poolData = { UserPoolId : decry.uid,
    		        ClientId : decry.cid
    		    };
    		
    		var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
    		
    		var cognitoUser = userPool.getCurrentUser();
    		
    	    if (cognitoUser != null) {
    	    	quizjs = getusersession();
    	   
    	    } else if(fbuser != null){
    	    	quizjs = getfbusersession();
    	  
        }else {
    	    	
    	    	$window.location.href = '#page';
    	    }
    	    function getquizjson(accessKeyId, secretAccessKey, sessionToken,token){
    	        if(decry.api === undefined){
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                
    	            });
    	        }else{
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                invokeUrl: decry.api,
    	            });
    	        }
		
			  var params = {};

				var body = {
						quizid : quizid,
						oid: decry.oid
						 };

				 var additionalParams = {
				           headers: {Authorization : token
				           }
				     };
				  
				var quiz =	apigClient.getQuizPost(params, body, additionalParams)
					.then(function(result){
					  
					   var json = JSON.stringify(result.data);
					    var quiz1 = json.toString();
					  
					    return $q.when(quiz1);
					    	
					    }).catch( function(result){
					    	var json = JSON.stringify(result);
					    	var json1 = json.toString();
					    	return $q.when(false);
					    	
					    	
					    });
				 return $q.when(quiz);
    	    }
    	    function gettoken(token, id){
      		  
    	        if($scope.decry.api === undefined){
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                
    	            });
    	        }else{
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                invokeUrl: $scope.decry.api,
    	            });
    	        }
   				var params1 = {};
   				var body = {type: id,token : token,oid : decry.oid};
   				var additionalParams = {};
   				var abcd = apigClient.getCredentialsWebPost(params1, body, additionalParams)
   				.then(function(result1){
   				   var tjson = JSON.stringify(result1.data);
   				   tjson = tjson.toString();
   				   
   				   tjson = JSON.parse(tjson);
   				var abc = getquizjson(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken); 
   				  
   				    return $q.when(abc);
   				    	
   				    }).catch( function(result){
   				    	swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
   		        		localStorage.clear();
   		    	    	$window.location.href = '#page';
   				    	
   				    })
   				return $q.when(abcd);
   	 	   }
   	    function getusersession(){
   	    	 return new Promise((resolve, reject) => {
   	 			 cognitoUser.getSession((err, session) =>{
   	 	            if (err) {
   				    	swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
   	 	            	localStorage.clear();
   	 	            	$window.location.href = '#page';
   	 	            }else{
   	 	            	//alert(session)
   	 	            	var token = session.idToken.jwtToken;
   	 	            	//alert(token);
   	 	            	//var abcc = gettoken(token, '0');
   	 	                var abcc = getquizjson('a', 'a', 'a',token); 
   	 	            	resolve(abcc)
   	 	            	return $q.when(abcc);
   	 	        
   	 	            }
   	  		  	});
   	 		})
   	    }
   	 function getfbusersession(){
		  return new Promise((resolve, reject) => {
			 Facebook.getLoginStatus( (response)=> {
	 			if (response.status == 'connected') {
	 		       var fbjson = JSON.stringify(response);
	 		       fbjson = fbjson.toString();
	 			   var fbtoken = fbjson.accessToken;
	 			   
	 			   if(fbtoken == undefined){
	 				   var fbtoken1 = JSON.parse(fbjson);
	 				  fbtoken = fbtoken1.authResponse.accessToken;
	 			   }
	 			   	var abcc = gettoken(fbtoken, '1');
	 			 	resolve(abcc)
	            	return $q.when(abcc);
	 			}else{
				    	swal({title: "Oooops!", text: "Facebook session has been timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
				    	localStorage.clear();
				    	$window.location.href = '#page';
	 			}
	 			}); 
		  })
			
	   }
   	 return $q.when(quizjs);
        }
    }
    };
});
