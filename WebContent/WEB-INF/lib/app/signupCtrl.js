var signupCtrl = function($scope, $uibModalInstance, $uibModal, $http, $location, $window, config) {
			
		$scope.inputType = 'password';
		
		$scope.close = function(){
			$window.location.href = '#page';
			$uibModalInstance.dismiss('cancel');
	
	};
	$scope.signupload=function()
	{
    	    $(window).resize(function() {
                $scope.$apply(function() {
                  $scope.windowWidth = $( window ).width();
                  if($scope.windowWidth < 767){
                      $uibModalInstance.close();
                  }
                  if($scope.windowWidth > 767 ){
                  }
                });
              });
          if($window.innerWidth > 460){
          $scope.showlogin = true;
    	}
          
	}
      $scope.signupload();
	
	$scope.signup = function(){
	    $scope.loading = true;
		$scope.error = false;
		$scope.error1 = false;
		$scope.error2 = 5;
		$scope.error3 = 5;
		$scope.format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
		$scope.format1 = /[ 1234567890]/;
		$scope.format2 = /[ abcdefghijklmnopqrstuvwxyz]/;
		  var usernameRegex = /^[a-zA-Z \s]+[0-9]+$/;
		  var usernameRegex1 = /^[a-zA-Z \s]+$/;
		if(  $scope.email == null || $scope.name == null || $scope.cpwd == null || $scope.pwd == null ||
			$scope.email == '' || $scope.name == '' || $scope.cpwd == '' || $scope.pwd == ''){
				
				if($scope.cpwd == '' || $scope.cpwd == null) {
					$scope.error3 = 0;
				}
				if( $scope.pwd == '' || $scope.pwd == null) {
					$scope.error2 = 0;
				}
				if($scope.name == '' || $scope.name == null) {
					$scope.error = 1;
				}
				if($scope.email == '' || $scope.email == null) {
					$scope.error1 = true;
				}
				 $scope.loading = false;
			}else if($scope.pwd != null || $scope.cpwd != null || $scope.name != null){
				$scope.sign = true;
				 
				if($scope.pwd != null){
					/*if($scope.cpwd.length < 8 || $scope.cpwd.replace(/\s/g, '').length === 0) {
						$scope.error2 = 0;
						$scope.sign = false;
					}*/
				  
					if(!$scope.format.test($scope.pwd) || !$scope.format1.test($scope.pwd) || !$scope.format2.test($scope.pwd)){
						$scope.error2 = 1;
						$scope.sign = false;
					}
				}
				if($scope.cpwd != null){
				if($scope.pwd.length < 8 || $scope.pwd.replace(/\s/g, '').length === 0) {
					$scope.error3 = 0;
					$scope.sign = false;
				}
				if(!$scope.format.test($scope.cpwd) || !$scope.format1.test($scope.cpwd) || !$scope.format2.test($scope.cpwd)){
					$scope.error3 = 1;
					$scope.sign = false;
				}
				}
				if($scope.name != null){
    				if($scope.name.replace(/\s/g, '').length === 0 ) {
    					$scope.error2 = true;
    					$scope.sign = false;
    				}
    		
    			
    				if((usernameRegex.test($scope.name) == false && usernameRegex1.test($scope.name) == false) || ( $scope.format1.test($scope.name) == true && $scope.format2.test($scope.name) == false))
                    {
    				    $scope.error = 3;
                        $scope.sign = false;
                    }
    				
				}
				if($scope.cpwd != $scope.pwd){
					$scope.error3 = 2;
					$scope.sign = false;
					}
				 $scope.loading = false;
				if($scope.sign == true){
					$scope.loading = true;
					$scope.signon();
				}
			}
		
		
		
		
		
	};
	
	$scope.signon = function(){
		
		var ts = Math.round((new Date()).getTime() / 1000);
		$scope.username = $scope.email.replace(/[^A-Z0-9]/ig, "");
	    AWSCognito.config.region = config.reg; //This is required to derive the endpoint
        
	    var poolData = { UserPoolId : config.uid,
	        ClientId : config.cid
	    };
	    var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);

	    var attributeList = [];
	    
	    var username = {
	        Name : 'name',
	        Value : $scope.name
	    };
	    
	    var useremail = {
		        Name : 'email',
		        Value : ($scope.email).toLowerCase()
		    };
	 
	    var attributename = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(username);
	    var attributeemail = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(useremail);
	   
	    attributeList.push(attributename);
	    attributeList.push(attributeemail);
	   
	    userPool.signUp(($scope.email).toLowerCase(), $scope.cpwd, attributeList, null, function(err, result){
	        if (err) {
	          
	        	$scope.loading = false;
	        	$scope.$apply();
	            if(err == 'UsernameExistsException: User already exists'){
	        		swal({title: "Oooops!", text: "User already exists with this Email ID!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});

	            }else if(err == 'UsernameExistsException: An account with the given email already exists.'){
                    swal({title: "Oooops!", text: "User already exists with this Email ID!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});

                }else if(err=="NetworkingError: Network Failure")
	                {
	                swal({title: "Oooops!", text: "Oops! Please check internet connectivity.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
	                }else
	            {
	        		swal({title: "Oooops!", text: "Something went wrong. Please try again later.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
	            }
	            return;
	        }else{
	        	$scope.loading = false;
	        	$scope.$apply();
	        	swal({title: "Mail Verification", text: "We have emailed you a link to complete the Sign up process.", type: "success", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
	        	$uibModalInstance.dismiss('cancel');
	        	$window.location.href = '#page';
	        	 //$scope.cognitoUser = result.user;
	        	/* $uibModalInstance.dismiss('cancel');
	        	 swal({
	        		  title: "",
	        		  
					  html:
						'<span style="color:black;font-family:MyWebFont1;">Please enter the verification code sent to your Email ID</span>' +
						'</br>' +'</br>' +
						'<p ng-app="" style="text-align: left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;font-family:MyWebFont1;">Verification Code</p>' +
	      				'<p style="color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;font-family:MyWebFont1;">:</p>' +
	      				'<input id="code" name="ntitle" rows="1" wrap="soft" ng-model="ntitle" style="font-family:MyWebFont1;float:left;border: 1px solid #ccc;width:65%;overflow:hidden; resize:none;line-height:25px;margin-bottom:10px;font-size:14px;" ></input>',			
	      				closeOnCancel: false,
					    allowOutsideClick: false,
					    allowEscapeKey:false,
					    showCancelButton: true, 
					    showConfirmButton: true, confirmButtonText: 'Submit', confirmButtonColor: "#f5a138",
					    width: '450px',
					  preConfirm: function () {
					    return new Promise(function (resolve) {
					    	 if ($('#code').val().replace(/\s/g, '').length === 0 || $('#code').val() === '') {
					    		 swal.showValidationError('Please enter the verification code');
					        	  resolve();
					    		 
						      
					        } else {
					        	
					        if($('#code').val().indexOf(' ') >= 0){
					        	  swal.showValidationError('Please enter the verification code');
					        	  resolve();
					        }else{
					    			
					    			 resolve([
									     $('#code').val()
									     ]);
					    		}
					        	  
					        }
					    });
					  }
					}).then(function (result) {
					    if (result.value) {
					        var json = JSON.stringify(result);
	                          $scope.ndesc = JSON.parse(json);
	                        $scope.vrcode = $scope.ndesc.value[0];
	                        $scope.register();
					    }else
					        {
					        $scope.loading = true;
					        $window.location.href = '#page';
					        $uibModalInstance.dismiss('cancel');
					        }
						
					 
					});*/
	        }
	       
	    });
	    
	
	};
	
	$scope.register = function(){
		
		var poolData = { UserPoolId : config.uid,
		        ClientId : config.cid
		    };
	    
	    var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
	    var userData = {
	    	Username : $scope.username,
	        Pool : userPool
	    };
	    
	    var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
	   
    	cognitoUser.confirmRegistration($scope.vrcode.toString(), true, function(err, result) {
        if (err) {
           // alert(err);
            if(err == 'CodeMismatchException: Invalid verification code provided, please try again.'){
            	swal({
	        		  title: "",
	        		  
					  html:
						'<span style="color:black;font-family:MyWebFont1;">Invalid verification code provided. Please enter the verification code sent to your Email ID</span>' +
						'</br>' +'</br>' +
						'<p ng-app="" style="text-align: left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;font-family:MyWebFont1;">Verification Code</p>' +
	      				'<p style="color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;font-family:MyWebFont1;">:</p>' +
	      				'<input id="code" name="ntitle" rows="1" wrap="soft" ng-model="ntitle" style="font-family:MyWebFont1;float:left;border: 1px solid #ccc;width:65%;overflow:hidden; resize:none;line-height:25px;margin-bottom:10px;font-size:14px;" ></input>',			
	      				closeOnCancel: false,
					    allowOutsideClick: false,
					    allowEscapeKey:false,
					    showCancelButton: true, 
					    showConfirmButton: true, confirmButtonText: 'Submit', confirmButtonColor: "#f5a138",
					    width: '450px',
					  preConfirm: function () {
					    return new Promise(function (resolve) {
					    	 if ($('#code').val().replace(/\s/g, '').length === 0 || $('#code').val() === '') {
					    		 swal.showValidationError('Please enter the verification code');
					        	  resolve();
					    		 
						      
					        } else {
					        	
					        if($('#code').val().indexOf(' ') >= 0){
					        	  swal.showValidationError('Please enter the verification code');
					        	  resolve();
					        }else{
					    			
					    			 resolve([
									     $('#code').val()
									     ]);
					    		}
					        	  
					        }
					    });
					  }
					}).then(function (result) {
					    if (result.value) {
    						 var json = JSON.stringify(result);
    						  $scope.ndesc = JSON.parse(json);
    						$scope.vrcode = $scope.ndesc.value[0];
    						$scope.register();
    					}else
                            {
                                $scope.loading = true;
                                $window.location.href = '#page';
                                $uibModalInstance.dismiss('cancel');
                            }
					});
            }else{
            	
            }
            
            return;
        }else{
        	swal({title: "", text: "SignUp was Completed Successfully", type: "success", width: '400px',showConfirmButton: true, confirmButtonText: 'Login Now', confirmButtonColor: "orange"});
        	$scope.login();
        	 
        }
       
    	});
	};
	
	$scope.myFunct = function(keyEvent) {
		  if (keyEvent.which === 13)
		      {
		        if($scope.loading == false || $scope.loading == undefined)
		            {
		                $scope.signup();
		            }
		      }
			 
		}
	
	
	$scope.login = function(){
		$scope.loading = true;
		$window.location.href = '#page';
		$uibModalInstance.dismiss('cancel');
	/*	$scope.loading = true;
		
		$scope.Instance = $uibModal.open({
		templateUrl: 'login.html',
		controller: 'loginCtrl',
		backdrop: 'static',
        keyboard: false,
        windowClass: 'loginmodal'

        });
		$scope.Instance.opened.then(function () {
			$scope.loading = false;
	    });
		 $scope.Instance.result.then(function (modal) {
			
		    	if(modal == true){
		    		$scope.loading = true;
		    	}else{
		    	}
		    }, function () {
		    	
		    	
		    });*/
	};
	
};

app.controller('signupCtrl', signupCtrl);
signupCtrl.$inject = ['$scope', '$uibModalInstance', '$uibModal', '$http', '$location', '$window', 'config'];