var viewnuggetCtrl = function($scope, $http, $location, $window, $uibModal, viewtopic,config,$crypto) {

	$scope.nugget = function(){

			$scope.topic = viewtopic;
			if($scope.topic == false){
        		$scope.errorpage = true;
        		
        	}else{
			$scope.topic = JSON.parse($scope.topic);
			$scope.tid = $scope.topic.tid;
			$scope.ttitle = $scope.topic.ttitle;
			$scope.tdescription = $scope.topic.tdescription;
			$scope.noofnuggets = $scope.topic.noofnuggets;
			$scope.tduration = $scope.topic.tduration;
			$scope.nuggets = $scope.topic.nuggets;

			$scope.content = true;
			$scope.overview = false;
			$scope.topicimg = config.url+$scope.tid+'/'+$scope.tid+"-lg.png";
			$scope.bimage = "url('"+$scope.topicimg+"')";
			$scope.myObj = {'background-color':'grey','overflow':'hidden','background-image':$scope.bimage,'background-size': 'cover','background-repeat': 'no-repeat'};

			if($window.innerWidth > 767){
    			$scope.showapppage = false;
    			$scope.showpage = true;
    	    	
    		}else{
    			$scope.showpage = false;
    			$scope.showapppage = true;
    			
    		}
    		
    		$(window).resize(function() {
    		      $scope.$apply(function() {
    		        $scope.windowWidth = $( window ).width();
    		        if($scope.windowWidth < 767){
    		        	$scope.showpage = false;
    		        	$scope.showapppage = true;
    		        }
    		        if($scope.windowWidth > 767	){
    		        	$scope.showpage = true;
    		        	$scope.showapppage = false;
    		        }
    		      });
    		    });
    		

			
			var apigClient = apigClientFactory.newClient({});

			var params = {};
			var body = {
					topicid: $scope.tid,
					eventtype: "Topic Viewed"
					 };
			var additionalParams = {};
			
			apigClient.analyticsWebAppPost(params, body, additionalParams)
			.then(function(result){	
			    	
			    }).catch( function(result){
			    	$scope.loading = false;
			    	$scope.$apply();
			    	
			    	
			    });
		
        	}
	};
	
	$scope.homescreen = function(){
		if($scope.online == true){
		    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
		       $scope.decry=JSON.parse(decrypted);
		       
		if($scope.decry.vsearch == true || $scope.decry.vsearch == "true"){
			$scope.loading = true;
			localStorage.removeItem("vsearch");
			$window.location.href = '#searchtopics';
		}else{
			$scope.loading = true;
			$window.location.href = '#page';
		}
		
		}
	}
	
	$scope.nugget();

	
	$scope.subscribe = function(){
		$scope.loading = true;
		localStorage.setItem("navtopic", $scope.tid)
		$scope.Instance = $uibModal.open({
		templateUrl: 'login.html',
		controller: 'loginCtrl',
		backdrop: 'static',
        keyboard: false,
        windowClass: 'loginmodal'

        });
		$scope.Instance.opened.then(function () {
			$scope.loading = false;
	    });
		 $scope.Instance.result.then(function (modal) {
			
		    	if(modal == true){
		    		$scope.loading = true;
		    	}else{
		    	}
		    }, function () {
		    	
		    	
		    });
	
		
	};


};

app.controller('viewnuggetCtrl', viewnuggetCtrl);
viewnuggetCtrl.$inject = ['$scope', '$http', '$location', '$window', '$uibModal', 'viewtopic','config','$crypto'];


app.factory("getviewTopic", function($window, $q,config, $crypto){
    return {
    	getviewTopic: function(){
			
    		if(localStorage.getItem("740a2y1e") == null || localStorage.getItem("740a2y1e") ==  ''){
    			
    			$window.location.href = '#page';
    			
    		}else{
    		    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
                var decry=JSON.parse(decrypted);
                
                    if(decry.api === undefined){
                        
                        var apigClient = apigClientFactory.newClient({
                            
                        });
                    }else{
                        
                        var apigClient = apigClientFactory.newClient({
                            invokeUrl: decry.api,
                        });
                    }
				
					  var params = {};
					  var additionalParams = {};
					 
					  var body = {
							  oid: "ENHANZED",
							  topicid : decry.vtopicid
								 };
					  
					  var viewtopic =	apigClient.getTopicPost(params, body, additionalParams)
						.then(function(result){
						   var json = JSON.stringify(result.data);
						    var topicjson = json.toString();
						    
						    return $q.when(topicjson);
						    	
						    }).catch( function(result){
						    	
						    	return $q.when(false);
						    	
						    });
				return viewtopic;
        }
    	}
    };
});
