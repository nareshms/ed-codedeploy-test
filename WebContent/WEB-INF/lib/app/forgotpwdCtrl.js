var forgotpwdCtrl = function($scope, $uibModalInstance, $http, $location, $window, config,$uibModal) {
	
	$scope.forgot = true;
	$scope.close = function(){
		$window.location.href = '#page';
		$uibModalInstance.dismiss('cancel');
	
	};
	   $scope.forgotpage=function()
	    {
	            $(window).resize(function() {
	                $scope.$apply(function() {
	                  $scope.windowWidth = $( window ).width();
	                  if($scope.windowWidth < 767){
	                      $uibModalInstance.close();
	                  }
	                  if($scope.windowWidth > 767 ){
	                  }
	                });
	              });
	          if($window.innerWidth > 460){
	         
	        }
	          window.altkey = true;
	       
	         
	    }
	      $scope.forgotpage();
	      
	$scope.myFunct = function(keyEvent) {
		  if (keyEvent.which === 13)
	      {
		
		     if( $scope.loading == false || $scope.loading == undefined)
		         {
		             $scope.loading =true;
		             $scope.next();
		         }
             
	      }
		     
		}
	$scope.next = function() {
	    $scope.loading = true;
	    window.navigating = true;
		$scope.error1 = 0;
		$scope.format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
		$scope.format1 = /[ 1234567890]/;
		$scope.format2 = /[ abcdefghijklmnopqrstuvwxyz]/;
		$scope.error = false;
		if($scope.email == undefined || $scope.email == ''){
			$scope.error = true;
			 $scope.loading = false;
		}else{
		    console.log('ok');
		$scope.loading = true;
		var poolData = { UserPoolId : config.uid,
		        ClientId : config.cid
		    };
	    
	    var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
	    var userData = {
	    	Username : $scope.email,
	        Pool : userPool
	    };
	 
	    var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
	    cognitoUser.forgotPassword({
	        onSuccess: function (result) {
	        	$uibModalInstance.dismiss('cancel');
	        	 $scope.loading = false;
	        	  window.navigating = false;
	        	swal({title: "",type:"success", text: "Password has been changed successfully.", width: '400px',showConfirmButton: true, confirmButtonText: 'Login Now', confirmButtonColor: "#f5a138"});
	        	$window.location.href = '#page';
	        },
	        onFailure: function(err) {
	           
	        	$scope.loading = false;
	        	$scope.$apply();
	        	
	        	if (err == 'LimitExceededException: Attempt limit exceeded, please try after some time.')
	        	{
	        		$scope.error1 = 1;	 
	        		$scope.$apply();
	        		}
	        	if (err == "UserNotFoundException: Username/client id combination not found.") 
	        	{
	        		$scope.error1 = 2;
	        		$scope.$apply();}
	        	if(err == 'CodeMismatchException: Invalid verification code provided, please try again.')
	        	{
	        		$scope.error1 = 3;	 
	        		$scope.$apply();}
	        	if(err == "NetworkingError: Network Failure")
	        	{
	        		$scope.error1 = 4;	
	        		$scope.$apply();}
	        	if(err == "NotAuthorizedException: User password cannot be reset in the current state.")
	        	    {	        	   
    	        	    $scope.error1 = 5;    
                        $scope.$apply();
	        	    }
	        	if(err == "NotAuthorizedException: User is disabled")
	        	    {
	        	    $scope.error1 = 6;    
                    $scope.$apply();
	        	    }
	        	 $scope.loading = false;
	        	 window.navigating = false;
	        },
	        inputVerificationCode() {
	        
	           
	        	$scope.loading = false;
	        
	        	 window.navigating = false;
	        	 localStorage.setItem("emailid",$scope.email );
	        	 $scope.$apply();
	        	 $uibModalInstance.dismiss('cancel'); 
	             $scope.Instance = $uibModal.open({
	                 templateUrl: 'passwordreset.html',
	                 controller: 'passwordresetCtrl',
	                 backdrop: 'static',
	                 keyboard: false,
	                 windowClass: 'forgotmodal',
	                
	                 });
	             $scope.Instance.opened.then(function () {
	                 $scope.loading = false;
	             });
	           
	        	 
	        	//$scope.$apply();
	        	
	        	/*swal({
	        		  title: "Password Reset",
	        		  
					  html:
						'<span style="color:black;font-family:MyWebFont1;">Please enter the Verification Code sent to your email</span>' +
						'</br>' +
						'<p ng-app="" style="text-align: left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;font-family:MyWebFont1;">Verification Code</p>' +
	      				'<p style="color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;font-family:MyWebFont1;">:</p>' +
	      				'<input id="code" name="ntitle" rows="1" wrap="soft" ng-model="ntitle" style="font-family:MyWebFont1;float:left;border: 1px solid #ccc;width:65%;overflow:hidden; resize:none;line-height:25px;margin-bottom:10px;font-size:14px;" ></input>'+				
	      				'</br>'+
	      				'<p style="text-align:left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;font-family:MyWebFont1;">Password</p>' +
	      				'<p style="color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;font-family:MyWebFont1;">:</p>' +
	      				'<input type="password" id="pwd" name="nduration" rows="1" wrap="soft" ng-model="nduration" style="font-family:MyWebFont1;border: 1px solid #ccc;float:left;width:65%;overflow:hidden;line-height:25px;margin-bottom:10px;font-size:14px;"></input>'+
	      				'</br>'+
	      				'<p style="text-align:left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;font-family:MyWebFont1;">Confirm Password</p>' +
	      				'<p style="color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;font-family:MyWebFont1;">:</p>' +
	      				'<input type="password" id="cpwd" name="nduration" rows="1" wrap="soft" ng-model="nduration" style="font-family:MyWebFont1;border: 1px solid #ccc;float:left;width:65%;overflow:hidden;line-height:25px;font-size:14px;margin-bottom:10px;"></input>'+
	      				'</br>'+
	      				'</br>'+
	      				'<span style="color:black;font-family:MyWebFont1;">* Password should be at least 8 characters, contain at least one lowercase letter, one number & one special character.</span>',
	      				closeOnCancel: false,
					    allowOutsideClick: false,
					    allowEscapeKey:false,
					    showCancelButton: true, 
					    showConfirmButton: true, confirmButtonText: 'Reset', confirmButtonColor: "#f5a138",
					    width: '450px',
					    customClass: 'swal-wide',
					    
					    preConfirm: function () {
					    	
					    return new Promise(function (resolve) {
					        
					        if($scope.online == true){
					     
					        
    					    	 if ($('#code').val().replace(/\s/g, '').length === 0 || $('#code').val() === '' ||  $('#cpwd').val().replace(/\s/g, '').length === 0 || $('#pwd').val().replace(/\s/g, '').length === 0 || $('#cpwd').val() === '' ||  $('#pwd').val() === '') {
    					    		 swal.showValidationError('Please enter the required fields');
    					        	  resolve();
    					    		 
    						      
    					        } else {
    					        	
    					        if($('#pwd').val().indexOf(' ') >= 0 ||  $('#cpwd').val().indexOf(' ') >= 0 ){
    					        	  swal.showValidationError('Password should not contain space');
    					        	  resolve();
    					        }else if(!$scope.format2.test($('#pwd').val()) || !$scope.format2.test($('#cpwd').val())  || !$scope.format1.test($('#pwd').val()) || !$scope.format1.test($('#cpwd').val())  || !$scope.format.test($('#pwd').val()) || !$scope.format.test($('#cpwd').val()) || $('#pwd').val().length < 8 || $('#cpwd').val().length < 8){
    					        	swal.showValidationError('Invalid Password Format');
    					        	  resolve();
    				    		}else if ($('#cpwd').val() ===  $('#pwd').val() ){
    					    			
    					    			 resolve([
    									     $('#code').val(),
    									     $('#pwd').val()
    									     ]);
    					    		}else {
    						        	
    						        	  swal.showValidationError('Confirm Password does not match');
    						        	  resolve();
    						        	  }
    					        	  
    					        }
					        }else
					            {
					            swal.showValidationError('Oops! Please check internet connectivity.');
					            resolve();
					            }
					    });
					  }
					}).then(function (result) {
						if(result.dismiss == 'cancel' || result.dismiss == 'close' ){
							$scope.modalclose();
					        }else{
					        	 var json = JSON.stringify(result);
								  $scope.ndesc = JSON.parse(json);
								$scope.code = $scope.ndesc.value[0];;
								$scope.npwd = $scope.ndesc.value[1];;
								$scope.confirmpassword($scope.code, $scope.npwd);
					        }
						
					 
					});*/
	        	
	        	
	        }
	    });
		
	}
	};
	
	$scope.modalclose = function(){
		$window.location.href = '#page';
	};
	
	/*$scope.confirmpassword = function(code, npwd){
		$scope.loading = true;
    	$scope.$apply();
    	var poolData = { UserPoolId : config.uid,
    		        ClientId : config.cid
    		    };
	    
	    var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
	    var userData = {
	    	Username : $scope.email,
	        Pool : userPool
	    };
	  
	    var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
	   
	    cognitoUser.confirmPassword(code, npwd, {
	        onSuccess: function (result) {
	        	$uibModalInstance.dismiss('cancel');
	        	swal({title: "",type:"success", text: "Password has been changed successfully.", width: '400px',showConfirmButton: true, confirmButtonText: 'Login Now', confirmButtonColor: "#f5a138"});
	        	$window.location.href = '#page';
	        },
	        onFailure: function(err) {
	        	
	        	if (err == 'CodeMismatchException: Invalid verification code provided, please try again.')
	        	{
	        		$scope.loading = false;
		        	$scope.$apply();
	        		swal({
		        		  title: "",
		        		  html:
							'<span style="color:red;font-family:MyWebFont1;">Invalid verification code provided, please try again</span>' +
							'</br>' +
							'</br>' +
							'<span style="color:black;font-family:MyWebFont1;">Please enter the verification code sent to your Email ID</span>' +
							'</br>' +
							'<p ng-app="" style="text-align: left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;font-family:MyWebFont1;">Verification Code</p>' +
		      				'<p style="color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;font-family:MyWebFont1;">:</p>' +
		      				'<input id="code" name="ntitle" rows="1" wrap="soft" ng-model="ntitle" style="font-family:MyWebFont1;float:left;border: 1px solid #ccc;width:65%;overflow:hidden; resize:none;line-height:25px;margin-bottom:10px;font-size:14px;" ></input>'+				
		      				'</br>'+
		      				'<p style="text-align:left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;font-family:MyWebFont1;">Password</p>' +
		      				'<p style="color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;font-family:MyWebFont1;">:</p>' +
		      				'<input type="password" id="pwd" name="nduration" rows="1" wrap="soft" ng-model="nduration" style="font-family:MyWebFont1;border: 1px solid #ccc;float:left;width:65%;overflow:hidden;line-height:25px;margin-bottom:10px;font-size:14px;"></input>'+
		      				'</br>'+
		      				'<p style="text-align:left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;font-family:MyWebFont1;">Confirm Password</p>' +
		      				'<p style="color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;font-family:MyWebFont1;">:</p>' +
		      				'<input type="password" id="cpwd" name="nduration" rows="1" wrap="soft" ng-model="nduration" style="font-family:MyWebFont1;border: 1px solid #ccc;float:left;width:65%;overflow:hidden;line-height:25px;font-size:14px;margin-bottom:10px;"></input>'+
		      				'</br>'+
		      				'</br>'+
		      				'<span style="color:black;font-family:MyWebFont1;">* Password should be at least 8 characters, contain at least one lowercase letter & one number & one special character.</span>',
		      				closeOnCancel: false,
						    allowOutsideClick: false,
						    allowEscapeKey:false,
						    showCancelButton: true, 
						    showConfirmButton: true, confirmButtonText: 'Submit', confirmButtonColor: "#f5a138",
						    width: '450px',
						  preConfirm: function () {
						    return new Promise(function (resolve) {
						    	 if ($('#code').val().replace(/\s/g, '').length === 0 || $('#code').val() === '' ||  $('#cpwd').val().replace(/\s/g, '').length === 0 || $('#pwd').val().replace(/\s/g, '').length === 0 || $('#cpwd').val() === '' ||  $('#pwd').val() === '') {
						    		 swal.showValidationError('Please enter the required fields');
						        	  resolve();
						    		 
							      
						        } else {
						        	
						        if($('#pwd').val().indexOf(' ') >= 0 ||  $('#cpwd').val().indexOf(' ') >= 0 ){
						        	  swal.showValidationError('Password should not contain space');
						        	  resolve();
						        }else if(!$scope.format2.test($('#pwd').val()) || !$scope.format2.test($('#cpwd').val())  || !$scope.format1.test($('#pwd').val()) || !$scope.format1.test($('#cpwd').val())  || !$scope.format.test($('#pwd').val()) || !$scope.format.test($('#cpwd').val()) || $('#pwd').val().length < 8 || $('#cpwd').val().length < 8){
						        	swal.showValidationError('Invalid Password format');
						        	  resolve();
					    		}else if ($('#cpwd').val() ===  $('#pwd').val() ){
						    			
						    			 resolve([
										     $('#code').val(),
										     $('#pwd').val()
										     ]);
						    		}else {
							        	
							        	  swal.showValidationError('Password do not match');
							        	  resolve();
							        	  }
						        	  
						        }
						    });
						  }
						}).then(function (result) {
							
							if(result.dismiss == 'cancel' || result.dismiss == 'close' ){
								$scope.modalclose();
						        }else{
							var json = JSON.stringify(result);
							  $scope.ndesc = JSON.parse(json);
							$scope.code = $scope.ndesc.value[0];;
							$scope.npwd = $scope.ndesc.value[1];;
							$scope.confirmpassword($scope.code, $scope.npwd);
						        }
						 
						});
		        	
	        	}else if(err="NetworkingError: Network Failure")
	        	    {
	        	    $scope.loading = false;
                    $scope.$apply();
                    swal({
                          title: "",
                          html:
                            '<span style="color:black;font-family:MyWebFont1;">Oops! Please check internet connectivity.</span>' +
                            '</br>' +
                            '</br>' +
                            '<span style="color:black;font-family:MyWebFont1;">Please enter the verification code sent to your Email ID</span>' +
                            '</br>' +
                            '<p ng-app="" style="font-family:MyWebFont1;text-align: left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;">Verification Code</p>' +
                            '<p style="font-family:MyWebFont1;color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;">:</p>' +
                            '<input id="code" name="ntitle" rows="1" wrap="soft" ng-model="ntitle" style="font-family:MyWebFont1;float:left;border: 1px solid #ccc;width:65%;overflow:hidden; resize:none;line-height:25px;margin-bottom:10px;font-size:14px;" ></input>'+              
                            '</br>'+
                            '<p style="font-family:MyWebFont1;text-align:left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;">Password</p>' +
                            '<p style="font-family:MyWebFont1;color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;">:</p>' +
                            '<input type="password" id="pwd" name="nduration" rows="1" wrap="soft" ng-model="nduration" style="font-family:MyWebFont1;border: 1px solid #ccc;float:left;width:65%;overflow:hidden;line-height:25px;margin-bottom:10px;font-size:14px;"></input>'+
                            '</br>'+
                            '<p style="font-family:MyWebFont1;text-align:left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;">Confirm Password</p>' +
                            '<p style="font-family:MyWebFont1;color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;">:</p>' +
                            '<input type="password" id="cpwd" name="nduration" rows="1" wrap="soft" ng-model="nduration" style="font-family:MyWebFont1;border: 1px solid #ccc;float:left;width:65%;overflow:hidden;line-height:25px;font-size:14px;margin-bottom:10px;"></input>',
                            closeOnCancel: false,
                            allowOutsideClick: false,
                            allowEscapeKey:false,
                            showCancelButton: true, cancelButtonText: 'Cancel',
                            showConfirmButton: true, confirmButtonText: 'Submit', confirmButtonColor: "#f5a138",
                            width: '450px',
                          preConfirm: function () {
                            return new Promise(function (resolve) {
                                 if ($('#code').val().replace(/\s/g, '').length === 0 || $('#code').val() === '' ||  $('#cpwd').val().replace(/\s/g, '').length === 0 || $('#pwd').val().replace(/\s/g, '').length === 0 || $('#cpwd').val() === '' ||  $('#pwd').val() === '') {
                                     swal.showValidationError('Please enter the required fields');
                                      resolve();
                                     
                                  
                                } else {
                                    
                                if($('#pwd').val().indexOf(' ') >= 0 ||  $('#cpwd').val().indexOf(' ') >= 0 ){
                                      swal.showValidationError('Password should not contain space');
                                      resolve();
                                }else if(!$scope.format2.test($('#pwd').val()) || !$scope.format2.test($('#cpwd').val())  || !$scope.format1.test($('#pwd').val()) || !$scope.format1.test($('#cpwd').val())  || !$scope.format.test($('#pwd').val()) || !$scope.format.test($('#cpwd').val()) || $('#pwd').val().length < 8 || $('#cpwd').val().length < 8){
                                    swal.showValidationError('Invalid Password format');
                                      resolve();
                                }else if ($('#cpwd').val() ===  $('#pwd').val() ){
                                        
                                         resolve([
                                             $('#code').val(),
                                             $('#pwd').val()
                                             ]);
                                    }else {
                                        
                                          swal.showValidationError('Password do not match');
                                          resolve();
                                          }
                                      
                                }
                            });
                          }
                        }).then(function (result) {
                            if(result.dismiss == 'cancel' || result.dismiss == 'close' ){
                                $scope.modalclose();
                                }else{
                            var json = JSON.stringify(result);
                              $scope.ndesc = JSON.parse(json);
                            $scope.code = $scope.ndesc.value[0];;
                            $scope.npwd = $scope.ndesc.value[1];;
                            $scope.confirmpassword($scope.code, $scope.npwd);
                                }
                         
                         
                        });
	        	    }
	        	else 
	        	{
	        		$scope.loading = false;
		        	$scope.$apply();
	        		swal({
		        		  title: "",
		        		  html:
							'<span style="color:black;font-family:MyWebFont1;">Invalid Password: Password length should be minimum of 8 Characters</span>' +
							'</br>' +
							'</br>' +
							'<span style="color:black;font-family:MyWebFont1;">Please enter the verification code sent to your Email ID</span>' +
							'</br>' +
							'<p ng-app="" style="font-family:MyWebFont1;text-align: left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;">Verification Code</p>' +
		      				'<p style="font-family:MyWebFont1;color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;">:</p>' +
		      				'<input id="code" name="ntitle" rows="1" wrap="soft" ng-model="ntitle" style="font-family:MyWebFont1;float:left;border: 1px solid #ccc;width:65%;overflow:hidden; resize:none;line-height:25px;margin-bottom:10px;font-size:14px;" ></input>'+				
		      				'</br>'+
		      				'<p style="font-family:MyWebFont1;text-align:left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;">Password</p>' +
		      				'<p style="font-family:MyWebFont1;color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;">:</p>' +
		      				'<input type="password" id="pwd" name="nduration" rows="1" wrap="soft" ng-model="nduration" style="font-family:MyWebFont1;border: 1px solid #ccc;float:left;width:65%;overflow:hidden;line-height:25px;margin-bottom:10px;font-size:14px;"></input>'+
		      				'</br>'+
		      				'<p style="font-family:MyWebFont1;text-align:left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;">Confirm Password</p>' +
		      				'<p style="font-family:MyWebFont1;color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;">:</p>' +
		      				'<input type="password" id="cpwd" name="nduration" rows="1" wrap="soft" ng-model="nduration" style="font-family:MyWebFont1;border: 1px solid #ccc;float:left;width:65%;overflow:hidden;line-height:25px;font-size:14px;margin-bottom:10px;"></input>',
		      				closeOnCancel: false,
						    allowOutsideClick: false,
						    allowEscapeKey:false,
						    showCancelButton: true, cancelButtonText: 'Cancel',
						    showConfirmButton: true, confirmButtonText: 'Submit', confirmButtonColor: "#f5a138",
						    width: '450px',
						  preConfirm: function () {
						    return new Promise(function (resolve) {
						    	 if ($('#code').val().replace(/\s/g, '').length === 0 || $('#code').val() === '' ||  $('#cpwd').val().replace(/\s/g, '').length === 0 || $('#pwd').val().replace(/\s/g, '').length === 0 || $('#cpwd').val() === '' ||  $('#pwd').val() === '') {
						    		 swal.showValidationError('Please enter the required fields');
						        	  resolve();
						    		 
							      
						        } else {
						        	
						        if($('#pwd').val().indexOf(' ') >= 0 ||  $('#cpwd').val().indexOf(' ') >= 0 ){
						        	  swal.showValidationError('Password should not contain space');
						        	  resolve();
						        }else if(!$scope.format2.test($('#pwd').val()) || !$scope.format2.test($('#cpwd').val())  || !$scope.format1.test($('#pwd').val()) || !$scope.format1.test($('#cpwd').val())  || !$scope.format.test($('#pwd').val()) || !$scope.format.test($('#cpwd').val()) || $('#pwd').val().length < 8 || $('#cpwd').val().length < 8){
						        	swal.showValidationError('Invalid Password format');
						        	  resolve();
					    		}else if ($('#cpwd').val() ===  $('#pwd').val() ){
						    			
						    			 resolve([
										     $('#code').val(),
										     $('#pwd').val()
										     ]);
						    		}else {
							        	
							        	  swal.showValidationError('Password do not match');
							        	  resolve();
							        	  }
						        	  
						        }
						    });
						  }
						}).then(function (result) {
							if(result.dismiss == 'cancel' || result.dismiss == 'close' ){
								$scope.modalclose();
						        }else{
							var json = JSON.stringify(result);
							  $scope.ndesc = JSON.parse(json);
							$scope.code = $scope.ndesc.value[0];;
							$scope.npwd = $scope.ndesc.value[1];;
							$scope.confirmpassword($scope.code, $scope.npwd);
						        }
						 
						 
						});
	        	}
	        }
	    });
	
	};*/
	
};


app.controller('forgotpwdCtrl', forgotpwdCtrl);
forgotpwdCtrl.$inject = ['$scope', '$uibModalInstance', '$http', '$location', '$window', 'config','$uibModal'];