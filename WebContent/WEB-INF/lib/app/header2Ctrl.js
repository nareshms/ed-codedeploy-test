var header2Ctrl = function ($scope, $http, $location, $window, $cookies,$uibModal, config,$crypto) {
	
	

$scope.header = function(){
    if(localStorage.getItem("740a2y1e")!=null)
    {
    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
    $scope.decry=JSON.parse(decrypted);
    }else
        {
        $window.location.href = '#page';
        }
	$scope.username = $scope.decry.username;	
	
	var apigClient = apigClientFactory.newClient({
	});

  var params = {};
 
  var body = {
			oid: "ENHANZED",
			};
	
	var additionalParams = {};
	
	categories = apigClient.getCategoriesPost(params, body, additionalParams)
		.then(function(result){
		    	
		
		   var json = JSON.stringify(result.data);
		   $scope.cats = json.toString();
		   
		   $scope.cats = JSON.parse($scope.cats); 
		  
		    $scope.cats = $scope.cats.Categories;
		   $scope.$apply();
		    }).catch( function(result){
		    	
		    	return $q.when(false);
		    	
		    });
	
	/*$scope.cats = [{"category":'Anesthesiology'},{"category":'Biochemistry'},{"category":'Cardiology'},
		{"category":'Critical Care'},{"category":'Dermatology'},{"category":'Emergency Medicine'},{"category":'Gastroenterology'},
		{"category":'Head and Neck'},{"category":'Infectious Diseases'},{"category":'OB-Gyn'},
		{"category":'Pulmonology'},{"category":'Internal Medicine'},{"category":'Neurology'},
		{"category":'Oncology'},{"category":'Orthopedics'},{"category":'Pediatrics'},
		{"category":'Radiology'},{"category":'Urology'}];*/
	
};

$scope.myFunct = function(keyEvent, searchterm) {
	  if (keyEvent.which === 13)
		 if(searchterm == undefined || searchterm == ''){
     		swal({title: "Please enter a search term!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "orange"});
		 }else{
			 $scope.loading = true;
				//localStorage.setItem("search", searchterm);
				var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
			     $scope.decry=JSON.parse(decrypted);
			     $scope.decry["search"] = searchterm;
			   
			     localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
			     
				$window.location.href = '#searchtopics';
		 }
	};
	
	$scope.login = function(){
		$scope.loading = true;
		
		$scope.Instance = $uibModal.open({
		templateUrl: 'login.html',
		controller: 'loginCtrl',
		backdrop: 'static',
        keyboard: false,
        windowClass: 'loginmodal'

        });
		$scope.Instance.opened.then(function () {
			$scope.loading = false;
	    });
		 $scope.Instance.result.then(function (modal) {
			
		    	if(modal == true){
		    		$scope.loading = true;
		    	}else{
		    	}
		    }, function () {
		    	
		    	
		    });
	};
	$scope.header();
	
	$scope.homepage = function(){
		if($scope.online == true){
			$scope.loading = true;
		$window.location.href = '#page';}
	};
	
	$scope.search = function(searchterm){
		$scope.loading = true;
	/*	localStorage.setItem("search", searchterm.category);
		localStorage.setItem("sid", searchterm.id);*/
		var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
        $scope.decry=JSON.parse(decrypted);
        $scope.decry["search"] = searchterm.category;
        $scope.decry["sid"] =searchterm.id;
        localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
		$window.location.href = '#searchtopics';
	};
	
	$scope.test = function(){
		alert();
	}
};
app.controller('header2Ctrl', header2Ctrl);
header2Ctrl.$inject = ['$scope', '$http', '$location', '$window','$cookies', '$uibModal','config','$crypto'];

