var homeCtrl = function ($scope, $http, $location, $window, $cookies, topics, config,$crypto) {

	
$scope.header = {name: "header.html", url: "header.html"};



$scope.home = function(){
    
    swal.close();
    //$window.location.href = '#network';
  
    //location.reload(false);

	$scope.alltopics = topics;
	if($scope.alltopics == false){
	   
	   
		$scope.errorpage = true;
		$scope.othererrmsg = true;
        if($scope.online == false){
            $scope.othererrmsg = false;
        }
	}else{
	if($window.innerWidth > 767){
		$scope.showapppage = false;
		$scope.showpage = true;
    	
	}else{
		$scope.showpage = false;
		$scope.showapppage = true;
		
	}
	if($window.innerWidth > 1024){
		$scope.nooftopics = 4;
    	
	}else{
		$scope.nooftopics = 3;
		
	}
	$(window).resize(function() {
	   
	      $scope.$apply(function() {
	        $scope.windowWidth = $( window ).width();
	        if($scope.windowWidth < 767){
	        	$scope.showpage = false;
	        	$scope.showapppage = true;
	        }
	        if($scope.windowWidth > 767	){
	        	$scope.showpage = true;
	        	$scope.showapppage = false;
	        }
	        if($window.innerWidth > 1024){
	        	$scope.test1($window.innerWidth);
		    	
			}else{
				$scope.test1($window.innerWidth);
				
			}
	      });
	    });
	
	

	
	$scope.iframeHeight = $window.innerHeight;

	$scope.alltopics = JSON.parse($scope.alltopics);
    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
    $scope.decrypt=JSON.parse(decrypted);
    $scope.notstart = false;
	if($scope.alltopics.myTopics == false){
		$scope.showtopics = false;
		
		//$scope.toptopics = $scope.alltopics.toptopics;
		$scope.toptopics = $scope.alltopics.toptopics.filter(function(item) {
		    return item.publishunpublish == 1 || item.publishunpublish == undefined;
		});
		
		/*$scope.assignedtopics = $scope.alltopics.assignedtopics;
		if($scope.assignedtopics == false || $scope.assignedtopics == 'false'){
			$scope.noassignedtopics = false;
			
		}else{
			$scope.noassignedtopics = true;
			for(var n=0; n < $scope.assignedtopics.length; n++){
	    		$scope.topicimg = config.url+$scope.decrypt.oid.toLowerCase()+"-resources/images/topic-images/"+$scope.assignedtopics[n].tid+".png";
	    		$scope.assignedtopics[n].topicimg = $scope.topicimg;
	    		if($scope.assignedtopics[n].duedate == undefined){
    				
    			}else{
    				
    				var diff = moment.utc($scope.assignedtopics[n].duedate).fromNow();
    				if(diff.includes("ago")){
    					diff = 'Overdue';
    				}
    				$scope.assignedtopics[n].duedate = diff;
    			}
	    	}
		}*/
		$scope.notstarttab();
    	if($scope.toptopics == '')
    		{
    			$scope.notoptopics = true;
    		}
    	for(var m=0; m < $scope.toptopics.length; m++){
    		//$scope.toptopics[m].tid = $scope.toptopics[m].id;
    		$scope.toptopicimg = config.url+$scope.decrypt.oid.toLowerCase()+"-resources/images/topic-images/"+$scope.toptopics[m].tid+".png";
    		$scope.toptopics[m].toptopicimg = $scope.toptopicimg;
    	}
		
	}else{
	   
		$scope.showtopics = true;
		$scope.topics = $scope.alltopics.myTopics;
		$scope.topics =	$scope.topics.sort(function (x, y) {
            return y.sd - x.sd;
         });
		
		var filtered = $scope.topics.filter(function(item) { 
		    return item.tp.charAt(0) == 1;  
		 });		
	
		   if(filtered != undefined && filtered !='')
		       {
    		       $scope.topics = filtered;
    		       $scope.inprogress = true;
		       }else if($scope.alltopics.assignedtopics == false)
		           {
		           var filtered = $scope.topics.filter(function(item) { 
		               return item.tp.charAt(0) == 2 || item.tp.charAt(0) == 3;  
		            });
		           
    		           if(filtered != undefined && filtered != '' )
    		               {
    		                  $scope.tcompleted = true;
    		                  $scope.topics = filtered;
    		                 
    		               }  
		           }else
		               {
		                   $scope.topics = $scope.alltopics.assignedtopics; 
		                   $scope.notstart = true;
		               }
		 
    	$scope.sessionexpired = false;
    	for(var i=0; i < $scope.topics.length; i++){
    	   
    		if($scope.topics[i].tp == 0 || $scope.topics[i].tp == undefined){
    			$scope.topics[i].completed = 0;
    			
    			if($scope.topics[i].duedate == undefined){
    			
    			}else{
    				var diff = moment.utc($scope.topics[i].duedate).fromNow();
    				if(diff.includes("ago")){
    					diff = 'Overdue';
    				}
    				$scope.topics[i].duedate = diff;
    			}
    		}else{
    		  
    			$scope.noofnuggets = ($scope.topics[i].tp.match(/-/g)||[]).length;
    			$scope.comp = 0;
    			
    			for (var k = 2; k < $scope.topics[i].tp.length; k++){
    				//alert($scope.topics[i].tp.substring(k, (+k + +1)));
    				if($scope.topics[i].tp.substring(k, (+k + +1)) == 2){
    					$scope.comp++;
    				}
    				
    				k++;
    			}
    			
    			$scope.completed = (($scope.comp/$scope.noofnuggets)*100);
    			
    			if( ($scope.completed == Math.floor($scope.completed))  )
    			{
    				$scope.completed = $scope.completed;
    			}else
    			{
    				var n = $scope.completed.toFixed(2);
    				$scope.completed = n;
    			};
    			if($scope.completed == 100){
    				$scope.topics[i].duedate = undefined;
    			}
    			$scope.topics[i].completed = $scope.completed;
    			if($scope.topics[i].duedate == undefined){
    				
    			}else{
    				
    				var diff = moment.utc($scope.topics[i].duedate).fromNow();
    				if(diff.includes("ago")){
    					diff = 'Overdue';
    				}
    				$scope.topics[i].duedate = diff;
    			}
    		}
    		$scope.topicimg = config.url+$scope.decrypt.oid.toLowerCase()+"-resources/images/topic-images/"+$scope.topics[i].tid+".png";
    		$scope.topics[i].topicimg = $scope.topicimg;
    	}
    	
    	/*$scope.assignedtopics = $scope.alltopics.assignedtopics;
    	
    	if($scope.assignedtopics == false || $scope.assignedtopics == 'false'){
    		$scope.noassignedtopics = false;
			
		}else{
			for(var n=0; n < $scope.assignedtopics.length; n++){
	    		$scope.topicimg = config.url+$scope.decrypt.oid.toLowerCase()+"-resources/images/topic-images/"+$scope.assignedtopics[n].tid+".png";
	    		$scope.assignedtopics[n].topicimg = $scope.topicimg;
	    		if($scope.assignedtopics[n].duedate == undefined){
    				
    			}else{
    				
    				var diff = moment.utc($scope.assignedtopics[n].duedate).fromNow();
    				if(diff.includes("ago")){
    					diff = 'Overdue';
    				}
    				$scope.assignedtopics[n].duedate = diff;
    			}
	    	}
			$scope.noassignedtopics = true;
		}
		*/
    	
    	$scope.toptopics = $scope.alltopics.toptopics.filter(function(item) {
            return item.publishunpublish == 1 || item.publishunpublish == undefined;
        });
    	
    	for(var m=0; m < $scope.toptopics.length; m++){
    		//$scope.toptopics[m].tid = $scope.toptopics[m].id;
    		$scope.toptopicimg = config.url+$scope.decrypt.oid.toLowerCase()+"-resources/images/topic-images/"+$scope.toptopics[m].tid+".png";
    		$scope.toptopics[m].toptopicimg = $scope.toptopicimg;
    	}
    	
	}
	}
	
	$scope.$watch('online', function() {
        if($scope.online == true){          
            if($scope.errorpage == true){
                $scope.loading = true;
                $window.location.href = '#topic';
            }
        }else{
            $scope.loading = false;
        }
     });
	 window.navigating = false;
};
$scope.scrollTo = function (target){
};


$scope.completedtab = function()
{
    $scope.notstart = false;
    $scope.tcompleted = true;
    $scope.inprogress = false;
   if($scope.alltopics.myTopics != false)
     {
        var filtered = $scope.alltopics.myTopics.filter(function(item) { 
            return item.tp.charAt(0) == 2 || item.tp.charAt(0) == 3;  
         });
    
        if(filtered != undefined && filtered != '' && filtered != null)
            {
               
                $scope.topics = filtered; 
                for(var i=0; i < $scope.topics.length; i++){
                    $scope.topicimg = config.url+$scope.decrypt.oid.toLowerCase()+"-resources/images/topic-images/"+$scope.topics[i].tid+".png";
                    $scope.topics[i].topicimg = $scope.topicimg;
                    $scope.topics[i].duedate = undefined;
                }
                $scope.showtopics = true;
                for(var i=0; i < $scope.topics.length; i++){
                    if($scope.topics[i].tp == 0 || $scope.topics[i].tp == undefined){
                        $scope.topics[i].completed = 0;
                   
                    }else{
                        
                        $scope.noofnuggets = ($scope.topics[i].tp.match(/-/g)||[]).length;
                        $scope.comp = 0;
                      
                        for (var k = 2; k < $scope.topics[i].tp.length; k++){
                           
                            if($scope.topics[i].tp.substring(k, (+k + +1)) == 2){
                                $scope.comp++;
                            }
                            
                            k++;
                        }
                        
                        $scope.completed = (($scope.comp/$scope.noofnuggets)*100);
                        
                        if( ($scope.completed == Math.floor($scope.completed))  )
                        {
                            $scope.completed = $scope.completed;
                        }else
                        {
                            var n = $scope.completed.toFixed(2);
                            $scope.completed = n;
                        };
                      
                        $scope.topics[i].completed = $scope.completed;
                     
                    }
                }
            } else{ 
                $scope.showtopics = false;
                }
     }else
         {
         $scope.showtopics = false;
         }

}
$scope.inprogresstab = function()
{
    $scope.notstart = false;
    $scope.tcompleted = false;
    $scope.inprogress = true;
    if($scope.alltopics.myTopics != false)
    {
        var filtered = $scope.alltopics.myTopics.filter(function(item) { 
            return item.tp.charAt(0) == 1 ;  
         });
      
        if(filtered != undefined && filtered != '' && filtered != null)
            {
        
            $scope.topics = filtered; 
            $scope.showtopics = true;
     
            } else{ $scope.showtopics = false;}
    }else
        {
        $scope.showtopics = false;
        }

}
$scope.notstarttab = function()
{
    $scope.notstart = true;
    $scope.tcompleted = false;
    $scope.inprogress = false;
  
  
    if($scope.alltopics.assignedtopics != false )
        {
    
            $scope.topics =JSON.parse(topics).assignedtopics; 
         
            for(var i=0; i < $scope.topics.length; i++){
                $scope.topicimg = config.url+$scope.decrypt.oid.toLowerCase()+"-resources/images/topic-images/"+$scope.topics[i].tid+".png";
                $scope.topics[i].topicimg = $scope.topicimg;
                    if($scope.topics[i].duedate == undefined){
                    
                    }else{
                    
                    var diff = moment.utc($scope.topics[i].duedate).fromNow();
                    if(diff.includes("ago")){
                        diff = 'Overdue';
                    }
                    $scope.topics[i].duedate = diff;
                }
            }
          
            $scope.showtopics = true;
 
        }else{
            $scope.showtopics = false;
        }

}
/*if (performance.navigation.type == 1) {
    performance.navigation.type = 0;
    if(window.navigating == true)
        {  
       
            $window.location.href = '#home';
           
        }
  
  }*/
$scope.dashboard = function()
{
    if($scope.online == true){
        $scope.loading = true;
    $window.location.href = '#dashboard';
    
    }
}
$scope.myprogress = function()
{
    if($scope.online == true){
        $scope.loading = true;
    $window.location.href = '#myprogress';
    
    }
}

$(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
       alert()
    }
});
$scope.home();	

$scope.test1 = function(width){

	if(width > 1024){
		$scope.nooftopics = 4;
		$scope.$apply();
	}else{
		$scope.nooftopics = 3;
		$scope.$apply();
	}
};
$scope.opentopic = function(topic){
	$scope.loading = true;
	//localStorage.setItem("otopicid", topic.tid);
	 var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
     $scope.decry=JSON.parse(decrypted);
     $scope.decry["otopicid"] = topic.tid;
     localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
	 window.navigating = true;
	$window.location.href = '#topic';
};

$scope.explore = function(){
	$scope.loading = true;
	$window.location.href = '#explore';
}

$scope.notification = function(){
	$scope.loading = true;
	$window.location.href = '#notification';
}

$scope.badges = function(){
	$scope.loading = true;
	$window.location.href = '#badges';
}
	
};
app.controller('homeCtrl', homeCtrl);
homeCtrl.$inject = ['$scope', '$http', '$location', '$window','$cookies','topics', 'config','$crypto'];


app.factory("getTopics", function($window, $q, config,$crypto,$rootScope){
    return {
        
    	getTopics: function(){
    	    window.navigating = true;
    	    //alert(localStorage.getItem("740a2y1e"));
    	   
    		if(localStorage.getItem("740a2y1e") !=undefined  ){
    		    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
                var decry=JSON.parse(decrypted);
               
    		var topics;
			AWSCognito.config.region =  config.reg;
			AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
    	        IdentityPoolId: decry.iid
    	    });
    	   var fbuser = localStorage.getItem("fbuser");
    		var poolData = { UserPoolId : decry.uid,
    		        ClientId : decry.cid
    		    };
    		
    		var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
    		
    		var cognitoUser = userPool.getCurrentUser();
    		
    	   if (cognitoUser != null && decry.id != null) {
    		   topics = getusersession();
    		 
    		  
    	    }else if(fbuser != null && decry.id != null){
    	    	
    	    	topics = getfbusersession();
    	    }
    	    else {
    	     
    	    	localStorage.clear();
    	    	$window.location.href = '#page';
    	    }
    	   function getalltopics(accessKeyId, secretAccessKey, sessionToken, token){

    			
    			var topics;
    			
    			if(decry.api === undefined){
    			   
    			    var apigClient = apigClientFactory.newClient({
                        
                    });
    			}else{
    			    
    			    var apigClient = apigClientFactory.newClient({
                        invokeUrl: decry.api,
                    });
    			}
    			
    			var params1 = {};
    			
    			var id = decry.id;
    			
    				var body = { 
    						id : id,
    						iid : decry.iid
    						};
    				
    				var additionalParams = {
                            headers: {Authorization : token
                            }
                      };
    			  
    			
    				var to = apigClient.getUserDataWebPost(params1, body, additionalParams)
    				.then(function(result){
    				 
    				   var json = JSON.stringify(result.data);
    				   var topic = json.toString();
    				
    				    if(topic.substring(1, 10) == "No Topics"){
    				    	
    				    	var body1 = {
    				    			 topicids : [],
    				    			 oid: decry.oid,
    				    			 eid: decry.email
    									 };
    				    	
    				    	topic = apigClient.getMyTopicsPost(params1, body1, additionalParams)
    							.then(function(result1){
    								 	
    								
    							   var json1 = JSON.stringify(result1.data);
    							   json1 = json1.toString();
    							   json1 = JSON.parse(json1);
    							   json1.myTopics = false;
    							   json1 = JSON.stringify(json1);
    							
    							    return $q.when(json1);
    							    	
    							    }).catch( function(result){
    							    	return $q.when(false);
    							    	
    							    })
    				    	return topic;
    				    }else{
    				     
    				    	 topic = JSON.parse(topic);
    				    	 var  topicids = [];
    				    	 var tp = [];
    				    
    				    	 for(var i =0;i < topic.Records.length;i++ ){
    				    		topicids.push(topic.Records[i].Key);
    				    		var abc = topic.Records[i].Value;
    				    		var bcd  = JSON.parse(abc);
    				    		var obj = {
    				    				tid: topic.Records[i].Key,
    				    				tp : bcd.tp,
    				    				sd : bcd.sd
    				    		}
    				    		tp.push(obj);
    				    	 }
    				  
    				    	 var body = {
    				    			 topicids : topicids,
    				    			 oid: decry.oid,
    				    			 eid: decry.email
    									 };
    				    	
    				    	var topics1 = apigClient.getMyTopicsPost(params1, body, additionalParams)
    							.then(function(result){
    							
    							   var json = JSON.stringify(result.data);
    							    var topic1 = json.toString();
    							  
    							    var topic2 = JSON.parse(topic1);
    						
    							    for(var k=0; k < topic2.myTopics.length;k++){
    							    	for(var m=0; m < tp.length ;m++){
    							    	   
    							    		if(topic2.myTopics[k].tid == tp[m].tid){
    							    			
    							    			topic2.myTopics[k].tp =  tp[m].tp;
    							    			topic2.myTopics[k].sd =  tp[m].sd;
    							    		}
    							    	}
    							    }
    							  
    							    topic2 = JSON.stringify(topic2);
    						
    							 
    							    return $q.when(topic2);
    							    	
    							    }).catch( function(result){
    							    	return $q.when(false);
    							    	
    							    })
    				    	return topics1;
    				    }
    				    
    				 
    				    
    				    	
    				    }).catch( function(result){
    				    	
    				        var json = JSON.stringify(result.data);
                            var topic1 = json.toString();
                            var topic2 = JSON.parse(topic1);
    				    	 
    						    
    				    	return $q.when(topic2);
    				    	
    				    });
    				return $q.when(to);
    		
    	   }
    	   function gettoken(token, id){
    		  
    	     
    	       if(decry.api === undefined){
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                
    	            });
    	        }else{
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                invokeUrl: decry.api,
    	            });
    	        }
 				var params1 = {};
 				var body = {type: id,token : token,oid : decry.oid};
 				var additionalParams = {};
 				var abcd = apigClient.getCredentialsWebPost(params1, body, additionalParams)
 				.then(function(result1){
 				   var tjson = JSON.stringify(result1.data);
 				   tjson = tjson.toString();
 				   
 				   tjson = JSON.parse(tjson);
 				  var abc = getalltopics(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken); 
 				  
 				    return $q.when(abc);
 				    	
 				    }).catch( function(result){
 				       if($rootScope.online == false)
 				          {		
 				           			            
 				               $window.location.href = '#network';   		                      
 				          }else
 				              {
     				            swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
     				                allowEscapeKey:false, buttonsStyling: false});
     	                        localStorage.clear();
     	                        $window.location.href = '#page';
 				              }
 				    	
 				    	
 				    })
 				return $q.when(abcd);
 	 	   }
 	    function getusersession(){
 	    	 return new Promise((resolve, reject) => {
 	 			 cognitoUser.getSession((err, session) =>{
 	 	            if (err) {
     	 	              if($rootScope.online == false)
                          {
     	 	                 $window.location.href = '#network';
                          }else
                              {
         				    	swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
         			                allowEscapeKey:false, buttonsStyling: false});
         	 	            	localStorage.clear();
         	 	            	$window.location.href = '#page';
                              }
 	 	            }else{
 	 	            	var token = session.idToken.jwtToken;
 	 	            	//var abcc = gettoken(token, '0');
 	 	            	var abcc = getalltopics('a', 'a', 'a', token); 
 	 	             
 	 	            	resolve(abcc)
 	 	            	return $q.when(abcc);
 	 	        
 	 	            }
 	  		  	});
 	 		})
 	    }
 	   function getfbusersession(){
 		  return new Promise((resolve, reject) => {
 			 Facebook.getLoginStatus( (response)=> {
 	 			if (response.status == 'connected') {
 	 		       var fbjson = JSON.stringify(response);
 	 		       fbjson = fbjson.toString();
 	 			   var fbtoken = fbjson.accessToken;
 	 			   
 	 			   if(fbtoken == undefined){
 	 				   var fbtoken1 = JSON.parse(fbjson);
 	 				  fbtoken = fbtoken1.authResponse.accessToken;
 	 			   }
 	 			  
 	 			   var abcc = gettoken(fbtoken, '1');
	            	resolve(abcc)
	            	return $q.when(abcc);
 	 			}else{
				    	swal({title: "Oooops!", text: "Facebook session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
				    	localStorage.clear();
				    	$window.location.href = '#page';
 	 			}
 	 			}); 
 		  })
 			
 	   }
    	   return $q.when(topics);
    	}else{
    	  
    		localStorage.clear();
	    	$window.location.href = '#page';
    	}
	}

    };
});


app.directive('setClassWhenAtTop', function ($window) {
    var $win = angular.element($window); // wrap window object as jQuery object

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var topClass = attrs.setClassWhenAtTop, // get CSS class from directive's attribute value
                offsetTop = element.offset().top; // get element's top relative to the document

            $win.on('scroll', function (e) {
                if ($win.scrollTop() >= offsetTop) {
                    element.addClass(topClass);
                } else {
                    element.removeClass(topClass);
                }
            });
        }
    };
});
