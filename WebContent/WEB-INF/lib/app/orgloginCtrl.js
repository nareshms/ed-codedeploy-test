var orgloginCtrl = function ($scope, $http, $location, $window, $timeout, $cookies, $uibModal, $uibModalInstance, jwtHelper, config,$crypto) {

    $scope.login = function(){
        $(window).resize(function() {
              $scope.$apply(function() {
                $scope.windowWidth = $( window ).width();
                if($scope.windowWidth < 767){
                    $uibModalInstance.close();
                }
                if($scope.windowWidth > 767 ){
                }
              });
            });
        if($window.innerWidth > 460){
        $scope.showlogin = true;
        $scope.inputType = 'password';
        $scope.isVisible2 = true;
        var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
        $scope.decry=JSON.parse(decrypted);
        $scope.orgid = $scope.decry.oid;
        $scope.orgimg = config.url+$scope.orgid.toLowerCase()+"-resources/images/org-images/"+$scope.orgid+".png";
        
        }else{
            $scope.showlogin = false;
        }
        
    };
    
    $scope.login();
    

    $scope.logon = function(){
         $scope.error = false;
         $scope.error1 = false;
         if( $scope.email == null || $scope.pwd == null || $scope.email == '' || $scope.pwd == '' || $scope.pwd.length < 8){
             if($scope.email == null || $scope.email == ''){
                 $scope.error = 1;
                 
             }
             if($scope.pwd == null || $scope.pwd == '' || $scope.pwd.length < 8){
                 $scope.error1 = 2;
             }
            
        }
        else{
            
            $scope.loading = true;
           
        if(localStorage.getItem("isUser") == undefined || localStorage.getItem("isUser") == null || localStorage.getItem("isUser") == false){
            
            if(localStorage.getItem("740a2y1e") != null){
                    localStorage.setItem("isUser", true);            
                    
                    
                    AWSCognito.config.region = config.reg;
                    AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
                        IdentityPoolId: $scope.decry.iid
                    });
                   
                    var poolData = { UserPoolId : $scope.decry.uid,
                            ClientId : $scope.decry.cid
                        };
                    var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
                    
                    var cognitoUser = userPool.getCurrentUser();
                    
                    if (cognitoUser != null && $scope.decry.email != null) {
                        
                          cognitoUser.getSession(function(err, session) {
                                if (err) {
                                    $scope.seucrelogin();
                                }else{
                                    $scope.loading = true;
                                    $window.location.href = '#home';
                                    $uibModalInstance.close();
                                    }
                          });
                    
                        
                    }else {
                       
                        $scope.seucrelogin();
                        
                    }
            }else
                {
                    $window.location.href = '#page';
                }
        }else{
           
            $timeout( function(){$scope.alreadyUser() }, 3000); 
        }
        }
        
    }
    
    $scope.alreadyUser = function(){
        
        var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
        $scope.decry=JSON.parse(decrypted);
        
        AWSCognito.config.region = config.reg;
        AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId: $scope.decry.iid
        });
       
        var poolData = { UserPoolId : $scope.decry.uid,
                ClientId : $scope.decry.cid
            };
        var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
        
        var cognitoUser = userPool.getCurrentUser();
        
        if (cognitoUser != null && localStorage.getItem("740a2y1e") != null) {
            
              cognitoUser.getSession(function(err, session) {
                    if (err) {
                        $scope.seucrelogin();
                    }else{
                        $scope.loading = true;
                        $window.location.href = '#home';
                        $uibModalInstance.close();
                        }
              });
            
        } else {
            $scope.seucrelogin();
            
        }
        
    }
    
    $scope.seucrelogin = function(){
        $scope.loading = true;
             AWSCognito.config.region = config.reg;
                AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
                    IdentityPoolId: $scope.decry.iid
                });

            var authenticationData = {
                Username : $scope.email,
                Password : $scope.pwd
            };
           
            var authenticationDetails = new AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);
            var poolData = { UserPoolId : $scope.decry.uid,
                ClientId : $scope.decry.cid
            };
            var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
            
            var userData = {
                Username : $scope.email,
                Pool :  userPool
            };
            
            var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);

            cognitoUser.authenticateUser(authenticationDetails, {
                
                onSuccess: function (result) {
                    
                    if ($scope.pwdreset == true){
                    	localStorage.removeItem('isUser');
                        $scope.loading = false;
                        $scope.pwd = '';
                        $scope.pwdreset = false;
                        $scope.$apply();
                        swal({title: "", text: "Password reset completed.", width: '400px',showConfirmButton: true, confirmButtonText: 'Login Now', confirmButtonColor: "orange"});
                    }else{
                    $scope.result = result;
                    
                    cognitoUser.getUserAttributes(function(err, result1) {
                        if (err) {
                            //swal({title: "Oooops!", text: "Something went wrong. Please try to logging again!!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "orange"});
                            //localStorage.clear();
                            //$window.location.href = '#page';
                            //return;
                            $scope.error = 5;
                            $scope.$apply();
                            localStorage.clear();
                        }
                        
                        else{
                        	
                            for (var i = 0; i < result1.length; i++) {
                                if (result1[i].getName() === 'name'){
                                    $scope.username = result1[i].getValue();
                                    $scope.decry["username"] = $scope.username;
                                    //localStorage.setItem("username", $scope.username);
                                    
                                    }
                                if (result1[i].getName() === 'sub'){
                                    $scope.useremail = result1[i].getValue();        
                                    
                                    }
                            }
                                if (result1 != null){
                                    
                                    AWS.config.region = config.reg;
                                    var cognitoidentity = new AWS.CognitoIdentity();
                                    
                                    
                                    var decoded = jwtHelper.decodeToken(result.getIdToken().getJwtToken());
                                    var userrole = decoded['cognito:groups'];
                                    
                                    if (decoded['cognito:groups'] == null) {
                                        localStorage.setItem("JwtToken", result.getIdToken().getJwtToken());
                                        var logins = 'cognito-idp.us-east-1.amazonaws.com/'+$scope.decry.uid;
                                        $scope.logins = {};
                                        $scope.logins[logins] = result.getIdToken().getJwtToken();
                                        var params = {
                                                  IdentityPoolId: $scope.decry.iid, 
                                                 Logins: $scope.logins
                                                };
                                                cognitoidentity.getId(params, function(err, data) {
                                                  if (err)
                                                      { 
                                                      
                                                      //swal({title: "Oooops!", text: "Something went wrong. Please try to logging again!!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "orange"});
                                                      $scope.error = 5;
                                                    $scope.$apply();
                                                    localStorage.clear();
                                                    }
                                                  else    { 
                                                      
                                                	  
                                                        //localStorage.setItem("id", data.IdentityId);
                                                        $scope.decry["id"] = data.IdentityId;
                                                        $scope.decry["email"] = $scope.useremail;
                                                        $scope.decry["emailid"] = $scope.email;
                                                        $scope.updatelogin('1');
                                                              localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
                                                              
                                                              $window.location.href = '#home';
                                                          
                                                          $uibModalInstance.close(true);
                                                    
                                                        
                                                      }         // successful response
                                                });
                                        
                                    }
                                    else {
                                        
                                    	
                                        localStorage.setItem("JwtToken", result.getIdToken().getJwtToken());
                                        var logins = 'cognito-idp.us-east-1.amazonaws.com/'+$scope.decry.uid;
                                        $scope.logins = {};
                                        $scope.logins[logins] = result.getIdToken().getJwtToken();
                                        var params = {
                                                  IdentityPoolId: $scope.decry.iid, 
                                                 Logins: $scope.logins
                                                };
                                                cognitoidentity.getId(params, function(err, data) {
                                                  if (err)
                                                      { 
                                                      
                                                       //swal({title: "Oooops!", text: "Something went wrong. Please try to logging again!!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "orange"});
                                                      $scope.error = 5;
                                                    $scope.$apply();
                                                    localStorage.clear();
                                                    }// an error occurred
                                                  else    { 
                                                	
                                                     /* localStorage.setItem("id", data.IdentityId);
                                                      localStorage.setItem("email", $scope.org.email);*/
                                                      $scope.decry["id"] = data.IdentityId;
                                                      $scope.decry["email"] = $scope.useremail;
                                                      $scope.decry["emailid"] = $scope.email;
                                                      
                                                          $scope.updatelogin('1');
                                                          localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
                                                         
                                                          $window.location.href = '#dashboard';
                                                      
                                                      $uibModalInstance.close(true);
                                                    
                                                        
                                                      }         // successful response
                                                });
                                        
                                    /*
                                        $scope.userrole = [];
                                        for (var i =0; i < decoded['cognito:groups'].length; i++) {
                                            var role = decoded['cognito:groups'][i];
                                            var value1 = role.indexOf("-");
                                            $scope.role = role.substring((value1+1), (role.length));
                                            alert($scope.role)
                                            if ($scope.role == "Admin" || $scope.role == "Instructors" || $scope.role == "Users") {
                                                if ($scope.role == "Admin"){
                                                    $scope.obj = {role: "Admin"};
                                                    $scope.userrole.push($scope.obj);
                                                }
                                                if ($scope.role == "Instructors"){
                                                    $scope.obj = {role: "Instructors"};
                                                    $scope.userrole.push($scope.obj);
                                                }
                                                if ($scope.role == "Users"){
                                                    $scope.obj = {role: "Instructors"};
                                                    $scope.userrole.push($scope.obj);
                                                }
                                            }
                                            $scope.userrole = [];
                                            $scope.loading = false;
                                            $scope.$apply();
                                            swal({title: "Oooops!", text: "You are not Authorized to Enter!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "orange"});
                                        }
                                        
                                        if ($scope.userrole.length > 0){
                                            $scope.updatelogin();
                                            
                                             localStorage.setItem("JwtToken", result.getIdToken().getJwtToken());
                                            
                                                var params = {
                                                          IdentityPoolId: $scope.IdentityPoolId, 
                                                         Logins: {
                                                              'cognito-idp.us-east-1.amazonaws.com/us-east-1_EK31UTYeY': result.getIdToken().getJwtToken()
                                                            
                                                          }
                                                        };
                                                        cognitoidentity.getId(params, function(err, data) {
                                                          if (err)
                                                              { 
                                                              
                                                              alert("Cognito Error"+err);
                                                              console.log(err, err.stack); }// an error occurred
                                                          else    { 
                                                              
                                                                AWS.config.region = $scope.region;
                                                                var cognitoidentity = new AWS.CognitoIdentity();
                                                                 
                                                                var params2 = {
                                                                          IdentityId: data.IdentityId,
                                                                          
                                                                          Logins: {
                                                                              'cognito-idp.us-east-1.amazonaws.com/us-east-1_EK31UTYeY' : result.getIdToken().getJwtToken()
                                                                          }
                                                                        };
                                                                
                                                                cognitoidentity.getCredentialsForIdentity(params2, function(err, data2) {
                                                                      if (err) {
                                                                          alert("Cognito Error"+err);
                                                                      } // an error occurred
                                                                      else   {  
                                                                          $uibModalInstance.dismiss('cancel');
                                                                          var accessKeyId = data2.Credentials.AccessKeyId;
                                                                          var secretAccessKey = data2.Credentials.SecretKey;
                                                                          var sessionToken = data2.Credentials.SessionToken;
                                                                          localStorage.setItem("email", $scope.email);
                                                                          //$window.location.href = '#dashboard';
                                                                          
                                                                                
                                                                      };
                                                                });
                                                            
                                                                
                                                              }         // successful response
                                                        });
                                        
                                        }
                                        else {
                                            $scope.loading = false;
                                            $scope.$apply();
                                            swal({title: "Oooops!", text: "1You are not Authorized to Enter!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "orange"});
                                        }
                                    */  
                                    }
                                    
                                }
                          }
                        
                    });
                    }       
                    //console.log('access token + ' + result.getAccessToken().getJwtToken());
                    // Use the idToken for Logins Map when Federating User Pools with Cognito Identity or when passing through an Authorization Header to an API Gateway Authorizer
                    //console.log('idToken + ' + result.idToken.jwtToken);
                },

                onFailure: function(err) {
                    
                    $scope.loading = false;
                    $scope.$apply();
                    if (err == "UserNotFoundException: User does not exist.") 
                    {
                        $scope.error = 3;
                        $scope.$apply();
                        //swal({title: "Oooops!", text: "Invalid Usermail or password!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                    }
                    else if(err == "NetworkingError: Network Failure")
                    {
                        $scope.error = 6;
                        $scope.$apply();
                        //swal({title: "Oooops!", text: "Please check your internet connection!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                    }else if(err == "NotAuthorizedException: Incorrect username or password."){
                        $scope.error = 4;
                        $scope.$apply();
                    }else {
                        $scope.error = 5;
                        $scope.$apply();
                        //swal({title: "Oooops!", text: "Invalid Usermail or password!", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                    }
                    
                },
                
                newPasswordRequired: function(userAttributes, requiredAttributes) {
                    
                    $scope.loading = false;
                    $scope.$apply();
                    $scope.obj = this;
                    swal({
                          title: "Change Password",
                          html:
                           
                            '<p style="text-align:left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;">Password</p>' +
                            '<p style="color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;">:</p>' +
                            '<input type="password" id="pwd" name="nduration" rows="1" wrap="soft" ng-model="nduration" style="border: 1px solid #ccc;float:left;width:65%;overflow:hidden;line-height:25px;margin-bottom:10px;font-size:14px;"></input>'+
                            '</br>'+
                            '<p style="text-align:left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;">Confirm Password</p>' +
                            '<p style="color:#767676;width:3%;float:left;font-size: 14px;margin:4px 4px 0 0;">:</p>' +
                            '<input type="password" id="cpwd" name="nduration" rows="1" wrap="soft" ng-model="nduration" style="border: 1px solid #ccc;float:left;width:65%;overflow:hidden;line-height:25px;font-size:14px;margin-bottom:10px;"></input>'
                            +'</br>'+
                            '</br>'+
                            '<span style="width:100%;color:black;font-family:MyWebFont1;">* Password should be at least 8 characters, contain at least one lowercase letter & one number & one special character.</span>' +
                            '</br>',

                            closeOnCancel: false,
                            allowOutsideClick: false,
                            allowEscapeKey:false,
                            showCancelButton: true, cancelButtonText: 'Cancel',
                            showConfirmButton: true, confirmButtonText: 'Next', confirmButtonColor: "orange",
                            width: '450px',
                            height: '450px',
                            preConfirm: function () {
                            return new Promise(function (resolve) {

                                 if (  $('#cpwd').val().replace(/\s/g, '').length === 0 || $('#pwd').val().replace(/\s/g, '').length === 0 || $('#cpwd').val() === '' ||  $('#pwd').val() === '') {
                                     swal.showValidationError('Please enter the required fields');
                                      resolve();
                                     
                                  
                                } else {
                                    
                                if($('#cpwd').val().indexOf(' ') >= 0 ||  $('#cpwd').val().indexOf(' ') >= 0 ){
                                      swal.showValidationError('Password should not contain space');
                                      resolve();
                                }else if ($('#cpwd').val() ===  $('#pwd').val() ){
                                        
                                         resolve([
                                             $('#name').val(),
                                             $('#pwd').val()
                                             ]);
                                    }else {
                                        
                                          swal.showValidationError('Password do not match');
                                          resolve();
                                          }
                                      
                                }
                            
                            });
                          }
                        }).then(function (result) {
                            
                            if(result.dismiss == 'cancel' || result.dismiss == 'close' ){
                                $scope.modalclose();
                                }else{
                            var json = JSON.stringify(result);
                            $scope.ndesc = JSON.parse(json);
                            $scope.code = $scope.ndesc.value[0];;
                            $scope.npwd = $scope.ndesc.value[1];;
                            $scope.pwdreset = true;
                            $scope.$apply();
                            var attributesData = {
                                   
                                };
                            
                            cognitoUser.completeNewPasswordChallenge($scope.npwd, attributesData, $scope.obj);
                                }
                         
                        });
                    
                    
                },

            }); 
        
      };
    
    
    
    $scope.modalclose = function(){
       // $window.location.href = '#page';
    };
    
$scope.forgotpwd = function(){
	$uibModalInstance.close('ofpwd');
   /* $uibModalInstance.dismiss('cancel');
    $scope.Instance = $uibModal.open({
    templateUrl: 'orgforgotpwd.html',
    controller: 'orgforgotpwdCtrl',
    backdrop: 'static',
    windowClass: 'forgotmodal',
        keyboard: false
        });*/
    
};

$scope.updatelogin = function(id){
    $scope.getcredentials();
};

$scope.getcredentials = function(){
    var fbuser = localStorage.getItem("fbuser");
   AWSCognito.config.region =  config.reg;
   AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
       IdentityPoolId: $scope.decry.iid
   });
  
   var poolData = { UserPoolId : $scope.decry.uid,
           ClientId : $scope.decry.cid
       };
   
   var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
   
   var cognitoUser = userPool.getCurrentUser();
   
   if (cognitoUser != null && $scope.decry.id != null) {
       $scope.getsession(cognitoUser);

   } else if(fbuser != null){
       $scope.getfbsession();

}else {
       localStorage.clear();
       $window.location.href = '#page';
   }
   
   
};

$scope.getsession = function(cognitoUser){
   
   return new Promise((resolve, reject) => {
          cognitoUser.getSession((err, session) =>{
             if (err) {
                 swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                 localStorage.clear();
                 $window.location.href = '#page';
             }else{
                 
                 var token = session.idToken.jwtToken;
                 $scope.Credentialsweb('a', 'a', 'a',token); 
                 resolve();
                /* var apigClient = apigClientFactory.newClient({});
                 var params1 = {};
                 var body = {type: '0',token : token,oid : $scope.decry.oid};
                 var additionalParams = {};
                 
                 apigClient.getCredentialsWebPost(params1, body, additionalParams)
                 .then(function(result1){
                    var tjson = JSON.stringify(result1.data);
                    tjson = tjson.toString();
                    
                    tjson = JSON.parse(tjson);
                   
                    $scope.Credentialsweb(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken); 
                    resolve();   
                     }).catch( function(result){
                         swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                         localStorage.clear();
                         $window.location.href = '#page';
                     })*/
         
             }
         });
     })
}

$scope.Credentialsweb = function(accessKeyId, secretAccessKey, sessionToken,token){
  
    if($scope.decry.api === undefined){
        
        var apigClient = apigClientFactory.newClient({
            
        });
    }else{
        
        var apigClient = apigClientFactory.newClient({
            invokeUrl: $scope.decry.api,
        });
    }
   var params = {};
   var body = {
           oid: $scope.decry.oid,
           id: $scope.decry.id,
           eventtype: "AuthenticatedViaCognito",
           email: $scope.useremail,
           emailid: $scope.email,
           name: $scope.username,
            };
   var additionalParams = {
           headers: {Authorization : token
           }
     };
   
   apigClient.analyticsWebAppPost(params, body, additionalParams)
   .then(function(result){ 
           
       }).catch( function(result){
           $scope.loading = false;
           $scope.$apply();
           var json = JSON.stringify(result);
           var json1 = json.toString();
           alert('ERROR while updatelogin');
           
           
       });
   
};

$scope.myFunct = function(keyEvent) {
      if (keyEvent.which === 13)
         {
              $scope.logon();
          }
          
    }

$scope.orglogin = function(){
    $uibModalInstance.close('orgid');
}


$scope.close = function(){
    
    $uibModalInstance.dismiss('cancel');
    $window.location.href = '#page';
 
   
};


};
app.directive('autoFocus', function($timeout) {
    return {
        link: function (scope, element, attrs) {
            attrs.$observe("autoFocus", function(newValue){
                if (newValue === "true")
                    $timeout(function(){element.focus()});
            });
        }
    };
});
app.controller('orgloginCtrl', orgloginCtrl);
orgloginCtrl.$inject = ['$scope', '$http', '$location', '$window', '$timeout', '$cookies','$uibModal','$uibModalInstance','jwtHelper','config','$crypto'];


