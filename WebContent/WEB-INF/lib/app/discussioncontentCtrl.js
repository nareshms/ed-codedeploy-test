 "use strict";
var discussioncontentCtrl = function ($scope, $http, $location, $window, $cookies, config ,$crypto,discussions,NgTableParams) {
    
    
    $scope.discussion = function()
    {
       $scope.showpage = true;
       $scope.cats = config.category;
       $scope.showlist = true;
       $scope.showreport = false;
       $scope.myapplication = discussions;
      
       $scope.applist = true;
       $scope.myapplication = JSON.parse($scope.myapplication);
       $scope.applications = [];
       $scope.imgurl = config.url;
       var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
       $scope.decrypt=JSON.parse(decrypted);
       $scope.oid = $scope.decrypt.oid.toLowerCase();
      
       
        if($window.innerWidth > 767){
    		$scope.showapppage = false;
    		$scope.showpage = true;
    		             
       
        	
    	} else{
    		$scope.showpage = false;
    		$scope.showapppage = true;
    		
    	}
        $(window).resize(function() {
     	   
  	      $scope.$apply(function() {
  	        $scope.windowWidth = $( window ).width();
  	        if($scope.windowWidth < 767){
  	        	$scope.showpage = false;
  	        	$scope.showapppage = true;
  	        }
  	        if($scope.windowWidth > 767	){
  	        	$scope.showpage = true;
  	        	$scope.showapppage = false;
  	        }
  	       /* if($window.innerWidth > 1024){
  	        	$scope.test1($window.innerWidth);
  	        	
  		    	
  			}else{
  			  
  				$scope.test1($window.innerWidth);
  				
  			}*/
  	      });
  	    });
        $scope.iframeHeight = $window.innerHeight;
    }
    $scope.discussion();
    
   
    $scope.myprograms = function()
    {
        if($scope.online == true){
            $scope.loading = true;
        $window.location.href = '#mytopics';
        
        }
    }
   
    $scope.dashboard = function()
    {
        if($scope.online == true){
            $scope.loading = true;
        $window.location.href = '#dashboard';
        
        }
    }
    $scope.myprogress = function()
    {
        if($scope.online == true){
            $scope.loading = true;
        $window.location.href = '#myprogress';
        
        }
    }
    $scope.getQuestion=function(){
        $scope.decry = JSON.parse($crypto.decrypt(localStorage.getItem("740a2y1e"), config.key));
            swal({
                title: "Post Question",
                html:
                    
                 /* '<p style="text-align:left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;">Question</p>' +*/              
                  '<textarea id="txtques" name="nduration" rows="6" cols="10" wrap="soft" placeholder="Type your question here..." ng-model="nduration" style="padding: 5px;border: 1px solid #ccc;float:left;width:83%;overflow:hidden;line-height:25px;font-size:14px;font-family: MyWebFont1;margin-left: 40px;resize: none;"></textarea>'
                    +'</br>'+'</br>'+'</br>'+'</br>'+'</br>'+'</br>'+'</br>'+'</br>'+'</br>'+'</br>'+'</br>',

                  closeOnCancel: false,
                  allowOutsideClick: false,
                  allowEscapeKey:false,
                  showCancelButton: true, cancelButtonText: 'Cancel',
                  showConfirmButton: true, confirmButtonText: 'Submit', confirmButtonColor: "orange",
                  width: '450px',
                  height: '450px',
                  customClass: 'sweetalertevent',
    			  buttonsStyling: false,
    			  confirmButtonClass: 'button1',
                  cancelButtonClass: 'button2',
                 
                  preConfirm: function () {
                  return new Promise(function (resolve) {
                        
                      if($('#txtques').val().trim().split(" ").length > 80)
                      {
                          swal.showValidationError('The Maximum number of words is 80 ');
                          resolve();
                          
                      }
                       if (  $('#txtques').val().replace(/\s/g, '').length === 0 || $('#txtques').val() === '' ) {
                           swal.showValidationError('Question cannot be empty');
                            resolve();
                            
                       }else
                  {
                           resolve([
                               $('#txtques').val()
                               
                               ]);
                  }        
                  }
                  );
                }
              }).then(function (result) {
                  var json = JSON.stringify(result);
                  var result = JSON.parse(json);
              
                  var discusobj={} ;
                  discusobj.qid = ($scope.program.discussion  != undefined && $scope.program.discussion.length !=0 )?  $scope.program.discussion[$scope.program.discussion.length - 1].qid + 1   : 1;
                  discusobj.question = result.value[0];
                  discusobj.emailId = $scope.decry.email;
                  discusobj.username = $scope.decry.username;
                  discusobj.answers = [];
                  $scope.getJsonFile(discusobj,0);
                  
              }).catch(function(result){
               
              });
        }
        $scope.myAns=function(ind){
            $scope.decry = JSON.parse($crypto.decrypt(localStorage.getItem("740a2y1e"), config.key));
            swal({
                    title: "Post Answer",
                    html:
                    
                     /* '<p style="text-align:left;color:#767676;width:30%;float:left;font-size: 14px;margin-top:4px;">Answer</p>' +*/
                      
                      '<textarea id="txtans" name="nduration" rows="6" cols="10" wrap="soft" placeholder="Type your answer here..."  ng-model="nduration" style="border: 1px solid #ccc;float:left;width:83%;overflow:hidden;line-height:25px;font-size:14px;font-family: MyWebFont1;margin-left: 40px;padding: 5px;resize:none"></textarea>'
                        +'</br>'+'</br>'+'</br>'+'</br>'+'</br>'+'</br>'+'</br>'+'</br>'+'</br>'+'</br>'+'</br>',
                      
                      closeOnCancel: false,
                      allowOutsideClick: false,
                      allowEscapeKey:false,
                      showCancelButton: true, cancelButtonText: 'Cancel',
                      showConfirmButton: true, confirmButtonText: 'Submit', confirmButtonColor: "orange",
                      width: '450px',
                      height: '450px',
                      customClass: 'sweetalertevent',
        			  buttonsStyling: false,
                      cancelButtonClass: 'button2',
                      confirmButtonClass: 'button1',
                      preConfirm: function () {
                      return new Promise(function (resolve) {

                           if (  $('#txtans').val().replace(/\s/g, '').length === 0 || $('#txtans').val() === '' ) {
                               swal.showValidationError('Answer cannot be empty');
                                resolve();
                               
                            
                           }else
                      {
                               resolve([
                                   $('#txtans').val()
                                   
                                   ]);
                      }        
                      }
                      );
                    }
                  }).then(function (result) {
                      var json = JSON.stringify(result);
                      var result = JSON.parse(json);
               
                      var discusobj={} ;
                      discusobj.aid =  ($scope.program.discussion[ind].answers  != undefined && $scope.program.discussion[ind].answers.length !=0 )?  $scope.program.discussion[ind].answers[$scope.program.discussion[ind].answers.length - 1].aid + 1   : 1;
                      discusobj.answer = result.value[0];
                      discusobj.emailId = $scope.decry.email;
                      discusobj.username = $scope.decry.username;

                      $scope.getJsonFile(discusobj,1,ind);
                     
                      $('#'+$scope.program.discussion[0].Questions[index].qid).collapse('show');
                  }).catch(function(result){
                      
                  });
        }
       
        
        $scope.deleteQuetion=function(question){
        	
        	 swal({
        		  title: "Are you sure?",
                  text: "You will not be able to recover this question!",
                  type: "warning",
                  closeOnCancel: false,
                  allowOutsideClick: false,
                  allowEscapeKey:false,
                  showCancelButton: true, cancelButtonText: 'Cancel',
                  showConfirmButton: true, confirmButtonText: 'Confirm', confirmButtonColor: "orange",
                  width: '450px',
                  height: '450px',
                  customClass: 'sweetalert-lgs',
    			  buttonsStyling: false,
                  cancelButtonClass: 'cancelClass',
                  confirmButtonClass: 'confirmClass',
               }).then(function (result) {
         
            	   if(result.value==true){
        	      //$scope.program.discussion[0].Questions.splice(index,1);

            	       var index = $scope.program.discussion.indexOf(question);
                       
                       $scope.getJsonFile(0,2,index);
         }   
           }).catch(function(result){
               
           });
        	 
       
         }
    $scope.deleteAnswer=function(qind,aind){
    	
    	swal({
    		
    		title: "Are you sure?",
            text: "You will not be able to recover this answer!",
            type: "warning",
            closeOnCancel: false,
            allowOutsideClick: false,
            allowEscapeKey:false,
            showCancelButton: true, cancelButtonText: 'Cancel',
            showConfirmButton: true, confirmButtonText: 'Confirm', confirmButtonColor: "orange",
            width: '450px',
            height: '450px',
            customClass: 'sweetalert-lgs',
    		buttonsStyling: false,
            cancelButtonClass: 'cancelClass',
            confirmButtonClass: 'confirmClass',
         }).then(function (result) {

      	   if(result.value==true){
      		 //$scope.program.discussion[0].Questions[ind].answers.splice(index,1);

      		      $scope.getJsonFile(aind,3,qind);
    			}   
    			 }).catch(function(result){
    			     
    		});
    	
      }
};
app.controller('discussioncontentCtrl', discussioncontentCtrl);
discussioncontentCtrl.$inject = ['$scope', '$http', '$location', '$window','$cookies', 'config','$crypto','discussions','NgTableParams'];

app.factory("getdiscussions", function($window, $q, config,$crypto){
    return {
    	getdiscussions: function(){
    	    window.navigating = true;
    	    //alert(localStorage.getItem("740a2y1e"));
    	
    		if(localStorage.getItem("740a2y1e") !=undefined  ){
    		    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
                var decry=JSON.parse(decrypted);
               
    		var myprogress;
			AWSCognito.config.region =  config.reg;
			AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
    	        IdentityPoolId: decry.iid
    	    });
    	   var fbuser = localStorage.getItem("fbuser");
    		var poolData = { UserPoolId : decry.uid,
    		        ClientId : decry.cid
    		    };
    		
    		var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
    		
    		var cognitoUser = userPool.getCurrentUser();
    		
    	   if (cognitoUser != null && decry.id != null) {
    		   myprogress = getusersession();
    		 
    		  
    	    }else if(fbuser != null && decry.id != null){
    	    	
    	    	myprogress = getfbusersession();
    	    }
    	    else {
    	     
    	    	localStorage.clear();
    	    	$window.location.href = '#page';
    	    }
    	   function getalltopics(accessKeyId, secretAccessKey, sessionToken, token){

    			
    			var topics;
    			
    			if(decry.api === undefined){
    			   
    			    var apigClient = apigClientFactory.newClient({
                        
                    });
    			}else{
    			    
    			    var apigClient = apigClientFactory.newClient({
                        invokeUrl: decry.api,
                    });
    			}
    			
    			var params1 = {};
    			
    			var id = decry.id;
    			
    				var body = { 
    						id : id,
    						iid : decry.iid
    						};
    				
    				var additionalParams = {
                            headers: {Authorization : token
                            }
                      };
    			  
    			
    				var to = apigClient.getUserDataWebPost(params1, body, additionalParams)
    				.then(function(result){
    				 
    				   var json = JSON.stringify(result.data);
    				   var topic = json.toString();
    				  
    				    if(topic.substring(1, 10) == "No Topics"){				    	
    				    	var obj = {myTopics: []}
    				    	obj = JSON.stringify(obj);
    				    	return obj;
    				    }else{
    				   
    				    	 topic = JSON.parse(topic);
    				    	 var  topicids = [];
    				    	 var tp = [];
    				    	 var otopics = [];
    				    	 for(var i =0;i < topic.Records.length;i++ ){
    				    			
    				    		var abc = topic.Records[i].Value;
    				    		var bcd  = JSON.parse(abc);
    				    
    				   
    				    		
    				    			topicids.push(topic.Records[i].Key);
    				    			var obj = {
        				    				tid: topic.Records[i].Key,
        				    				tp : bcd.tp,
        				    				sd : bcd.sd,
        				    				tobj : bcd.tobj,
        				    				tvid : bcd.tvid,
        				    				tdoc : bcd.tdoc,
        				    				tfq : bcd.tfq,
        				    				tsq : bcd.tsq,
        				    				tasmt : bcd.tasmt,
        				    				cobj : bcd.cobj,
        				    				cvid : bcd.cvid,
        				    				cdoc : bcd.cdoc,
        				    				cfq : bcd.cfq,
        				    				csq : bcd.csq,
        				    				casmt : bcd.casmt,
        				    				lact :bcd.lact
        				    		}
    				    			
    				    			tp.push(obj);
    				    		
    				    		
    				    	
    				    		//tp.push(obj);
    				    	 }
    				    
    				    	 if(!topicids.length > 0){
    				    	    
    				    		 var obj = {myTopics: []}
    				    		 obj = JSON.stringify(obj);
    	    				    	return obj;
    				    	 }else{
    				    	 var body = {
    				    			 topicids : topicids,
    				    			 oid: decry.oid,
    				    			 eid: decry.email
    									 };
    				   
    				    	var topics1 = apigClient.getMyTopicsPost(params1, body, additionalParams)
    							.then(function(result){
    							
    							   var json = JSON.stringify(result.data);
    							    var topic1 = json.toString();
    						
    							    var topic2 = JSON.parse(topic1);
    							
    							    for(var k=0; k < topic2.myTopics.length;k++){
    							    	for(var m=0; m < tp.length ;m++){
    							    		if(topic2.myTopics[k].tid == tp[m].tid){
    							    			
    							    			topic2.myTopics[k].tp =  tp[m].tp;
    							    			topic2.myTopics[k].sd =  tp[m].sd;
    							    			topic2.myTopics[k].tobj =  tp[m].tobj;
    							    			topic2.myTopics[k].tvid =  tp[m].tvid;
    							    			topic2.myTopics[k].tdoc =  tp[m].tdoc;  
    							    			topic2.myTopics[k].tfq = tp[m].tfq; 
    							    			topic2.myTopics[k].tsq = tp[m].tsq; 							    			
    							    			topic2.myTopics[k].tasmt =  tp[m].tasmt;
    							    			topic2.myTopics[k].cobj =  tp[m].cobj;
    							    			topic2.myTopics[k].cdoc =  tp[m].cdoc;
    							    			topic2.myTopics[k].cvid =  tp[m].cvid;
    							    			topic2.myTopics[k].csq =  tp[m].csq;
    							    			topic2.myTopics[k].cfq =  tp[m].cfq;
    							    			topic2.myTopics[k].lact =  tp[m].lact;
    							    		}
    							    	}
    							    }
    							    topic2.otopics = otopics;
    							   
    							    var body = {
    	    				    			 oid: decry.oid,
    	    				    			 eid: decry.email
    	    									 };
    							
    	    				    	var topics2 = apigClient.getAllQuizScoresPost(params1, body, additionalParams)
    	    							.then(function(result){
    	    							 
    	    							   if(result.data.length != 0  ){
    	    							       
    	    							       var json = JSON.stringify(result.data);
                                               topic2.quizscores = json;
    	    							   }else
    	    							       {
    	    							          topic2.quizscores = [];
    	    							         
    	    							       }
    	    								
    	    								topic2 = JSON.stringify(topic2);
    	    								
    	    								return topic2;
    	    							}).catch( function(result){
        							   
        							    	return $q.when(false);
        							    	
        							    })
    							   
    							    return $q.when(topics2);
    							    	
    							    }).catch( function(result){
    							    	
    							    	return $q.when(false);
    							    	
    							    })
    				    	return topics1;
    				    }
    				    }
    				    
    				 
    				    
    				    	
    				    }).catch( function(result){
    				    	
    				        var json = JSON.stringify(result.data);
                            var topic1 = json.toString();
                            var topic2 = JSON.parse(topic1);
    				    	 
    						    
    				    	return $q.when(topic2);
    				    	
    				    });
    				return $q.when(to);
    		
    	   }
    	   function gettoken(token, id){
    		  
    	     
    	       if(decry.api === undefined){
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                
    	            });
    	        }else{
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                invokeUrl: decry.api,
    	            });
    	        }
 				var params1 = {};
 				var body = {type: id,token : token,oid : decry.oid};
 				var additionalParams = {};
 				var abcd = apigClient.getCredentialsWebPost(params1, body, additionalParams)
 				.then(function(result1){
 				   var tjson = JSON.stringify(result1.data);
 				   tjson = tjson.toString();
 				   
 				   tjson = JSON.parse(tjson);
 				  var abc = getalltopics(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken); 
 				  
 				    return $q.when(abc);
 				    	
 				    }).catch( function(result){
 				       if($rootScope.online == false)
 				          {		
 				           			            
 				               $window.location.href = '#network';   		                      
 				          }else
 				              {
     				            swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
     				                allowEscapeKey:false, buttonsStyling: false});
     	                        localStorage.clear();
     	                        $window.location.href = '#page';
 				              }
 				    	
 				    	
 				    })
 				return $q.when(abcd);
 	 	   }
 	    function getusersession(){
 	    	 return new Promise((resolve, reject) => {
 	 			 cognitoUser.getSession((err, session) =>{
 	 	            if (err) {
     	 	              if($rootScope.online == false)
                          {
     	 	                 $window.location.href = '#network';
                          }else
                              {
         				    	swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
         			                allowEscapeKey:false, buttonsStyling: false});
         	 	            	localStorage.clear();
         	 	            	$window.location.href = '#page';
                              }
 	 	            }else{
 	 	            	var token = session.idToken.jwtToken;
 	 	            	//var abcc = gettoken(token, '0');
 	 	            	var abcc = getalltopics('a', 'a', 'a', token); 
 	 	             
 	 	            	resolve(abcc)
 	 	            	return $q.when(abcc);
 	 	        
 	 	            }
 	  		  	});
 	 		})
 	    }
 	   function getfbusersession(){
 		  return new Promise((resolve, reject) => {
 			 Facebook.getLoginStatus( (response)=> {
 	 			if (response.status == 'connected') {
 	 		       var fbjson = JSON.stringify(response);
 	 		       fbjson = fbjson.toString();
 	 			   var fbtoken = fbjson.accessToken;
 	 			   
 	 			   if(fbtoken == undefined){
 	 				   var fbtoken1 = JSON.parse(fbjson);
 	 				  fbtoken = fbtoken1.authResponse.accessToken;
 	 			   }
 	 			  
 	 			   var abcc = gettoken(fbtoken, '1');
	            	resolve(abcc)
	            	return $q.when(abcc);
 	 			}else{
				    	swal({title: "Oooops!", text: "Facebook session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
				    	localStorage.clear();
				    	$window.location.href = '#page';
 	 			}
 	 			}); 
 		  })
 			
 	   }
    	   return $q.when(myprogress);
    	}else{
    	  
    		localStorage.clear();
	    	$window.location.href = '#page';
    	}
	}
    };
});
