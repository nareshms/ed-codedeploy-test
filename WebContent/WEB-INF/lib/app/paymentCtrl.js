var paymentCtrl = function($scope, $http, $location, $window, $uibModal, config,$crypto,NgTableParams) {
	
	$scope.pay = function(){

	   $scope.options = {

               'key': 'rzp_test_RVamYi3E1ZD5lQ',
               'amount': '100',
               "currency": "USD",
               'name': 'Enhanzed Education Private Limited.',
               'description': 'Pay',
               'image': '',
               'handler': function (response) {
                 //$scope.transactionHandler(transaction);
             	 // alert(JSON.stringify(response));
             	  $scope.response = response;
             	  if($scope.response.razorpay_payment_id === undefined){
             		  swal({title: "Payment Failed",type:"error", text: "Payment Failed",allowOutsideClick: false,
                           allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', confirmButtonColor: "#f5a138",customClass: 'swal-wide',allowOutsideClick: false,buttonsStyling: false,
                           cancelButtonClass: 'cancelClass',
                           confirmButtonClass: 'confirmClass'});
             		  $scope.loading = false;
                 	  $scope.$apply();
             	  }else{
             		  swal({title: "Payment Success",type:"success",allowOutsideClick: false,
                           allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', confirmButtonColor: "#f5a138",customClass: 'swal-wide',allowOutsideClick: false,buttonsStyling: false,
                           cancelButtonClass: 'cancelClass',
                           confirmButtonClass: 'confirmClass'});
             		  $scope.startprogramon();
             	  }
                   //alert(response.JSON.stringify(response));
                  
               },
               'prefill': {
                 'name': 'Babugoud',
                 'email': 'babugoud@enhanzed.com',
                 'contact': '8123946523'
               },
               theme:{
                 color: '#3399FF'
               },
               'modal': {
                   'ondismiss': function(){
                 	  $scope.loading = false;
                 	  $scope.$apply();
                   }
               }
         };
      
      $.getScript('https://checkout.razorpay.com/v1/checkout.js', function() {
          var rzp1 = new Razorpay($scope.options);
        rzp1.open();

        });
	}
	$scope.paypal = function(){
		
		$scope.Buttons({

            // Set up the transaction
            createOrder: function(data, actions) {
                return actions.order.create({
                    purchase_units: [{
                        amount: {
                            value: '0.01'
                        }
                    }]
                });
            },

            // Finalize the transaction
            onApprove: function(data, actions) {
                return actions.order.capture().then(function(details) {
                    // Show a success message to the buyer
                    alert('Transaction completed by ' + details.payer.name.given_name + '!');
                });
            }


        }).render('#paypal-button-container');
		
		$.getScript('https://www.paypal.com/sdk/js?client-id=sb&currency=USD', function() {
	          var rzp1 = new paypal(paypal.Buttons);
	        rzp1.open();

	        });
	}
};
app.controller('paymentCtrl', paymentCtrl);
paymentCtrl.$inject = ['$scope', '$http', '$location', '$window', '$uibModal', 'config','$crypto','NgTableParams'];