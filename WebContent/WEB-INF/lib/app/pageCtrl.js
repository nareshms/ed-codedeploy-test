var pageCtrl = function ($scope, $http, $location, $window, $cookies, config, $uibModal,$crypto) {


$scope.home = function(){
  
    console.log('page loading again');
	if (localStorage.getItem("740a2y1e") == null) {
	    
		localStorage.clear();
		
		$scope.pageload();
	}else{
	  
	
	    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
        $scope.decry=JSON.parse(decrypted);  
	  
		AWSCognito.config.region = config.reg;
	    AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
	        IdentityPoolId: $scope.decry.iid
	    });
	  
		var poolData = { UserPoolId : $scope.decry.uid,
		        ClientId : $scope.decry.cid
		    };
		var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
	
		var cognitoUser = userPool.getCurrentUser();
		
		  
		if (cognitoUser != null && $scope.decry.oid != null) {
	    	
	    	  cognitoUser.getSession(function(err, session) {
		            if (err) {
		            	$scope.pageload();
		            }else{
		            	$scope.loading = true;
		            	
		            	$window.location.href = '#dashboard';}
	    	  });
	    	
	    } else {
	    	/*$scope.$watch(
					  function() {
						  
					    return Facebook.isReady();
					  },
					  function(newVal) {
						 
					    if (newVal){
					   
					    	  $scope.facebookReady = true;
					    	  $scope.fbcheck();
					    }
					    
					  }
					);*/
	    	if(localStorage.getItem("fbuser") == null||localStorage.getItem("fbuser") ==undefined)
	    	    {
	    	        $scope.pageload();
	    	    }
	    	
	    	
	    }
		
		
	}
	
};

$scope.login = function(){
    
    $scope.Instance = $uibModal.open({
          templateUrl: 'login.html',
          controller: 'loginCtrl',
          backdrop: 'static',
          keyboard: false,
          windowClass: 'loginmodal',
          scope: $scope,

          });
    $scope.Instance.result.then(function (modal) {
       
          if(modal == true){
              $scope.loading = true;
              
          }else if(modal == 'org'){
              $scope.orgid();
          }else if(modal == 'signup'){
              $scope.signup();
          }else{
             
          }
      }, function () {
          
          
      });
};


$scope.pageload = function(){
  
	$scope.errorpage = false;
	$scope.myObj = {'background-color':'grey','overflow':'hidden','background-image':'url(./lib/images/homepage.jpg)','background-size': 'cover','background-repeat': 'no-repeat'};
	if($window.innerWidth > 767){
		$scope.showapppage = false;
		$scope.showpage = true;
		$scope.login();
		 
	}else{
		$scope.showpage = false;
		$scope.showapppage = true;
		
	}
	$scope.height = $window.innerHeight;
	$scope.img = {'height': $scope.height};
	// $scope.login();
	$(window).resize(function() {
		
	      $scope.$apply(function() {
	    	  $scope.height = $window.innerHeight;
	    		$scope.img = {'height': $scope.height};
	        $scope.windowWidth = $( window ).width();
	        if($scope.windowWidth < 767){
	        	$scope.showpage = false;
	        	$scope.showapppage = true;
	        }
	        if($scope.windowWidth > 767	){
	            
	            if($scope.showpage==false)
	            {
	              /* if($scope.Instances!=undefined)
	                   {
	                   $scope.signup();
	                   }else if($scope.Instanceo!=undefined)
                       {
	                       $scope.orgid();
                       }else
	                       {
	                       $scope.login();
	                       }*/
	               $scope.login();
	            }
	            
	        	$scope.showpage = true;
	        	$scope.showapppage = false;
	        }
	      });
	    });
	
}





$scope.signup = function(){

	$scope.loading = true;
	
	$scope.Instance = $uibModal.open({
	templateUrl: 'signup.html',
	controller: 'signupCtrl',
	backdrop: 'static',
    keyboard: false,
    windowClass: 'signupmodal',
    scope: $scope,
    });
	$scope.Instance.opened.then(function () {
		$scope.loading = false;
    });
	 $scope.Instance.result.then(function (modal) {
	     
	    	if(modal == true){
	    		//$scope.loading = true;
	    	}else {
	    	    $uibModalInstance.close();
	    	
	    	    
	    	    }
	    }, function () {
	    	
	    	
	    });

}


$scope.orgforgotpwd = function(){
	
	$scope.loading = true;
	
	$scope.Instance = $uibModal.open({
	templateUrl: 'orgforgotpwd.html',
	controller: 'orgforgotpwdCtrl',
	backdrop: 'static',
    keyboard: false,
    windowClass: 'loginmodal',
    scope: $scope,
    
    });
	$scope.Instance.opened.then(function () {
		$scope.loading = false;
    });
	 $scope.Instance.result.then(function (modal) {
	    	if(modal == true){
	    		$scope.loading = true;
	    	}else if(modal == 'orglogin'){
	    		$scope.orglogin();
	    	}else{
	    		
	    	}
	    }, function () {
	    	
	    	
	    });
}

$scope.orglogin = function(){

	$scope.loading = true;
	
	$scope.Instance = $uibModal.open({
	templateUrl: 'orglogin.html',
	controller: 'orgloginCtrl',
	backdrop: 'static',
    keyboard: false,
    windowClass: 'loginmodal',
    scope: $scope,
    
    });
	$scope.Instance.opened.then(function () {
		$scope.loading = false;
    });
	 $scope.Instance.result.then(function (modal) {
	    	if(modal == true){
	    		$scope.loading = true;
	    	}else if(modal == 'orgid'){
	    		$scope.orgid();
	    	}else if(modal == 'ofpwd'){
	    		$scope.orgforgotpwd();
	    	}else{
	    		
	    	}
	    }, function () {
	    	
	    	
	    });

}

$scope.orgid = function(){

	$scope.loading = true;
	
	$scope.Instance = $uibModal.open({
	templateUrl: 'orgid.html',
	controller: 'orgidCtrl',
	backdrop: 'static',
    keyboard: false,
    windowClass: 'loginmodal',
    scope: $scope,
    
    });
	$scope.Instance.opened.then(function () {
		$scope.loading = false;
    });
	 $scope.Instance.result.then(function (modal) {
	    	if(modal == true){
	    		$scope.loading = true;
	    	}else if(modal == 'orglogin'){
	    		$scope.orglogin();
	    	}else{
	    		
	    	}
	    }, function () {
	    	
	    	
	    });

}

$scope.home();


$scope.fbcheck = function(){
	var userIsConnected = false;
	Facebook.getLoginStatus(function(response) {
		
		  if (response.status == 'connected') {
		   if(localStorage.getItem("id") != null){
			   $scope.loading = true;
			   var fbtoken = JSON.stringify(response);
	  			fbtoken = fbtoken.toString();
	  			
	  			fbtoken = JSON.parse(fbtoken);
	  			fbtoken = fbtoken.authResponse.accessToken;
			   localStorage.setItem("fbuser", fbtoken);
			 
			  	$window.location.href = '#home';
	           
		   }
		  }else{
			  
		  }
		});

}

};
app.controller('pageCtrl', pageCtrl);
pageCtrl.$inject = ['$scope', '$http', '$location', '$window','$cookies', 'config', '$uibModal','$crypto'];


