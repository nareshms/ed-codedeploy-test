

var app = angular.module('webview', ['ngRoute', 'ngSanitize', 'ngCookies','angular-jwt','ui.bootstrap', 'angular-jwplayer','ngFileSaver','slick','com.2fdevs.videogular.plugins.buffering','com.2fdevs.videogular','com.2fdevs.videogular.plugins.controls','com.2fdevs.videogular.plugins.poster','mdo-angular-cryptography','ui.calendar','ngTable','nvd3'])

app.constant('config', {
   reg: 'us-east-1',
   iid: "us-east-1:4458bc34-e420-46ac-b133-228397d49abe",
   uid: "us-east-1_H0X0N1Itb",
   cid: "7tpdec3r4qum634mvnbq1p8a59",
   url: "https://d93u1ve2a5jxt.cloudfront.net/",
   key: "1234567890qwertyuiopasdfghjklzxcvbnm",
   ctype: 0
});

/*app.constant('config', {
   reg: 'us-east-1',
   iid: "us-east-1:2591d3ef-a244-4d71-8288-b601d0f738da",
   uid: "us-east-1_fjVOtcCbS",
   cid: "mk8c7l570sogcn6lnk0jlqgij",
   url: "https://d3ho8qluix6upg.cloudfront.net/",
   key: "1234567890kuhsgadiuyhablsjdhgflskdjfh",
   ctype: 0
});
*/
// Routing has been added to keep flexibility in mind. This will be used in future.
angular.module('webview')
.config(['$routeProvider', '$cookiesProvider',
  function ($routeProvider,$cookiesProvider) {
	$cookiesProvider.defaults.path = '/';
      var routes = [
         
          {
              url: '/login',
              template: 'login.html',
              controller: 'loginCtrl',
          },
          {
              url: '/mytopics',
              template: 'home.html',
              controller: 'homeCtrl',
              resolve: {
                
                  topics: function(getTopics){
                      return getTopics.getTopics();
              }
              }
          },{

          url: '/network',
          template: 'nonetwork.html',
          controller: 'nonetworkCtrl'
         
      },
          {
              url: '/page',
              template: 'page.html',
              controller: 'pageCtrl',
            /*  resolve: {
                  toptopics: function(gettopTopics){
                      return gettopTopics.gettopTopics();
              }
              }*/
          },
          {
              url: '/searchtopics',
              template: 'searchresult1.html',
              controller: 'searchresult1Ctrl',
              resolve: {
            	  searchresult: function(searchresult1){
                      return searchresult1.searchresult1();
              }
              }
          },
          {
              url: '/searchtopic',
              template: 'searchresult.html',
              controller: 'searchresultCtrl',
              resolve: {
            	  searchresult: function(searchresult){
                      return searchresult.searchresult();
              }
              }
          },
          {
              url: '/topic',
              template: 'nugget.html',
              controller: 'nuggetCtrl',
              resolve: {
                  topic: function(getTopic){
                      return getTopic.getTopic();
              }
              }
            	 
          },
          {
              url: '/dashboard',
              template: 'dashboard.html',
              controller: 'dashboardCtrl',
              resolve: {
                
                  userdetails: function(getUserdetails){
                      return getUserdetails.getUserdetails();
              }
              }
          },{
              url: '/discussion',
              template: 'discussioncontent.html',
              controller: 'discussioncontentCtrl',
              resolve: {
                
            	  discussions: function(getdiscussions){
                      return getdiscussions.getdiscussions();
              }
              }
          },{
              url: '/payment',
              template: 'payment.html',
              controller: 'paymentCtrl'
          },{

              url: '/myprogress',
              template: 'myprogress.html',
              controller: 'myprogressCtrl',
              resolve: {
                  myprogress: function(getmyprogress){
                      return getmyprogress.getmyprogress();
              }
             }
             
          },
          {
              url: '/viewtopic',
              template: 'viewnugget.html',
              controller: 'viewnuggetCtrl',
              resolve: {
                  viewtopic: function(getviewTopic){
                      return getviewTopic.getviewTopic();
              }
              }
            	 
          },
          {
              url: '/player',
              template: 'nuggetPlayer.html',
              controller: 'nuggetplayerCtrl',
              resolve: {
            	  nuggetjson: function(nuggetService){
                      return nuggetService.getNugget();
              }
              }
            	 
          },
          {
              url: '/quiz',
              template: 'quiz.html',
              controller: 'quizCtrl',
              resolve: {
                  quizjson: function(quizService){
                      return quizService.getquiz();
              }
              }
            	 
          },
          {
              url: '/profile',
              template: 'profile.html',
              controller: 'profileCtrl',
              resolve: {
            	  certs: function(getcerts){
                      return getcerts.getcerts();
              }
              }
          },
          {
              url: '/viewcategory',
              template: 'viewcategory.html',
              controller: 'viewcategoryCtrl'
          },
          {
              url: '/privacypolicy',
              template: 'privacypolicy.html',
              controller: 'privacypolicyCtrl',
          },
          {
              url: '/T&C',
              template: 'terms.html',
              controller: 'termsCtrl',
          }
          
      ];
      
      routes.forEach(function (r, index) {
          $routeProvider.when(r.url, { templateUrl: r.template, controller: r.controller, resolve: r.resolve });
      });
      
      $routeProvider.otherwise({ redirectTo: '/page' });
  }]);

app.run(function($window, $rootScope) {
    $rootScope.online = navigator.onLine;
    $window.addEventListener("offline", function() {
      $rootScope.$apply(function() {
        $rootScope.online = false;
      });
    }, false);

    $window.addEventListener("online", function() {
      $rootScope.$apply(function() {
        $rootScope.online = true;
      });
    }, false);
});




