var searchresult1Ctrl = function ($scope, $http, $location, $window, $cookies, searchresult,config,$crypto) {


$scope.home = function(){
    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
    $scope.decry=JSON.parse(decrypted);
            	$scope.exploreresult = searchresult;
            	
            	if($scope.decry.sid != undefined){
            		$scope.topicimg = config.url+$scope.decry.oid+"/IMAGES/CATEGORIES/"+$scope.decry.sid+".png";
            		$scope.topicimg = "url('"+$scope.topicimg+"')";
            	}else{
            		$scope.topicimg = 'url(lib/images/homepage.jpg)';
            	}
            	$scope.myObj = {'background-color':'grey','overflow':'hidden','background-image': $scope.topicimg,'background-size': 'cover','background-repeat': 'no-repeat'};
            	if($scope.exploreresult == false){
            		$scope.errorpage = true;
            		
            	}else{
            		$scope.errorpage = false;
            		$scope.topics = [];
            		$scope.exploreresult = JSON.parse($scope.exploreresult);
        			$scope.exploreresult = $scope.exploreresult.hits.hit;
                	$scope.sessionexpired = false;
                	$scope.search = $scope.decry.search;
                	if($scope.exploreresult.length > 0){
                		$scope.showtopics = true;
                		$scope.topics = [];
                		for(var i=0; i < $scope.exploreresult.length; i++){
                			$scope.topicimg = config.url+"ENHANZED/IMAGES/"+$scope.exploreresult[i].id+'/'+$scope.exploreresult[i].id+"-lg.png";
                    		$scope.tduration = $scope.exploreresult[i].fields.tduration[0];
                    		$scope.ttitle = $scope.exploreresult[i].fields.ttitle[0];
                    		$scope.noofnuggets = $scope.exploreresult[i].fields.noofnuggets[0];
                    		$scope.tcertauth = $scope.exploreresult[i].fields.tcertauth[0];
                    		$scope.tnoofcredits = $scope.exploreresult[i].fields.tnoofcredits[0];
                    		$scope.freenavigation = $scope.exploreresult[i].fields.freenavigation[0];
                    		var obj = {
                    				topicid: $scope.exploreresult[i].id,
                    				topicimg: $scope.topicimg,
                    				tduration: $scope.tduration,
                    				ttitle: $scope.ttitle,
                    				noofnuggets: $scope.noofnuggets,
                    				tcertauth: $scope.tcertauth,
                    				tnoofcredits: $scope.tnoofcredits,
                    				freenavigation: $scope.freenavigation
                    		}
                    		$scope.topics.push(obj);
                    	}
                	}else{
                		$scope.showtopics = false;
                	}
                	
                	if($window.innerWidth > 767){
            			$scope.showapppage = false;
            			$scope.showpage = true;
            	    	
            		}else{
            			$scope.showpage = false;
            			$scope.showapppage = true;
            			
            		}
            		
            		$(window).resize(function() {
            		      $scope.$apply(function() {
            		        $scope.windowWidth = $( window ).width();
            		        if($scope.windowWidth < 767){
            		        	$scope.showpage = false;
            		        	$scope.showapppage = true;
            		        }
            		        if($scope.windowWidth > 767	){
            		        	$scope.showpage = true;
            		        	$scope.showapppage = false;
            		        }
            		      });
            		    });
            	}
            	 window.navigating = false;	
            	
		};

$scope.home();	

$scope.viewtopic = function(topicid){
	$scope.loading = true;
	/*localStorage.setItem("vsearch", true);
	localStorage.setItem("vtopicid", topicid);*/
	 var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
     $scope.decry=JSON.parse(decrypted);
     $scope.decry["vsearch"] = true;
     $scope.decry["vtopicid"] = topicid;
     localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
	$window.location.href = '#viewtopic';
};

$scope.back = function(){
	$scope.loading = true;
	$window.location.href = '#explore';
}
	
};
app.controller('searchresult1Ctrl', searchresult1Ctrl);
searchresult1Ctrl.$inject = ['$scope', '$http', '$location', '$window','$cookies','searchresult','config','$crypto'];

app.factory("searchresult1", function($window, $q,config,$crypto){
    return {
    	searchresult1: function(){
    	  
   if(localStorage.getItem("740a2y1e") == null || localStorage.getItem("740a2y1e") ==  ''){
                
                $window.location.href = '#page';
                
            }else{
                        var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
                        var decry=JSON.parse(decrypted);
            	    window.navigating = true;
            		if(decry.search == null || decry.search ==  ''){
            			
            			$window.location.href = '#page';
            			
            		}else{
            		    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
                        var decry=JSON.parse(decrypted);
                            if(decry.api === undefined){
                                
                                var apigClient = apigClientFactory.newClient({
                                    
                                });
                            }else{
                                
                                var apigClient = apigClientFactory.newClient({
                                    invokeUrl: decry.api,
                                });
                            }
        				
        					  var params = {};
        					 
        						var body = {
        								searchterm: decry.search,
        								oid: decry.oid
        								 };
        						
        						var additionalParams = {};
        						
        						var searchresult = apigClient.searchTopicsPost(params, body, additionalParams)
        							.then(function(result){
        							    	
        							
        							   var json = JSON.stringify(result.data);
        							    var result = json.toString();
        							  
        							    return $q.when(result);
        							    	
        							    }).catch( function(result){
        							    	
        							    	return $q.when(false);
        							    	
        							    });
        		   
                   return searchresult;
                }
            }
    		
    	}
    };
});
