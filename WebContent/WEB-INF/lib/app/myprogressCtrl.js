 "use strict";
var myprogressCtrl = function ($scope, $http, $location, $window, $cookies, config ,$crypto,myprogress,NgTableParams) {
    
    
    $scope.myapplication = function()
    {
       $scope.showpage = true;
       $scope.cats = config.category;
       $scope.showlist = true;
       $scope.showreport = false;
       $scope.myapplication = myprogress;
      
       $scope.applist = true;
       $scope.myapplication = JSON.parse($scope.myapplication);
       $scope.applications = [];
       $scope.imgurl = config.url;
       var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
       $scope.decrypt=JSON.parse(decrypted);
       $scope.oid = $scope.decrypt.oid.toLowerCase();
      
       if($scope.myapplication.myTopics != undefined && $scope.myapplication.myTopics.length > 0){

    	   for (var i=0;i < $scope.myapplication.myTopics.length;i++) {
    		   
    		   var progress = 0, ocomp = 0, ovid = 0, vprog = 0, sqquiz = 0, sqprog = 0, odoc = 0, dprog = 0, fqquiz = 0, fqprog = 0, asmt = 0, asmtprog = 0;
    		 
    		   var lastact;
    		   var tprogress = [];
    		   var qscores = [];
    		   $scope.quizscores = $scope.myapplication.quizscores.length == 0 ? [] : JSON.parse($scope.myapplication.quizscores);
    		  
    		   if($scope.quizscores.length > 0){
    			
    			 
    			   for(var l=0;l < $scope.quizscores.length;l++){
    			     
    			       Object.keys($scope.quizscores[l]).forEach(function(prop) {
    			                    if(prop.includes($scope.myapplication.myTopics[i].tid))
                                    {
                                      qscores.push($scope.quizscores[l])
                                    }
                              });
    			 
    				 
    			   }
    		   }
    		
    		   if($scope.myapplication.myTopics[i] !== undefined){
    			
    			 
    		      /* $scope.noofnuggets = ($scope.myapplication.myTopics[i].tp.match(/-/g)||[]).length;
                   $scope.comp = 0;
               
                  if(($scope.myapplication.myTopics[i].tp.charAt(0) != 1 && ($scope.myapplication.myTopics[i].freenavigation == true || $scope.myapplication.myTopics[i].freenavigation == "true")) || ($scope.myapplication.myTopics[i].freenavigation == false || $scope.myapplication.myTopics[i].freenavigation == "false"))
                      {            
                      
                               for (var k = 2; k < $scope.myapplication.myTopics[i].tp.length; k++){
                                   //alert($scope.topics[i].tp.substring(k, (+k + +1)));
                                   if($scope.myapplication.myTopics[i].tp.substring(k, (+k + +1)) == 2){
                                       $scope.comp++;
                                   }
                                  
                                   k++;
                               }
                      }*/


               if($scope.myapplication.myTopics[i].tp.charAt(0) == 1 && ($scope.myapplication.myTopics[i].freenavigation == true || $scope.myapplication.myTopics[i].freenavigation == "true")) 
                   {   
                    progress = 0;
                   }
               else
                   {
        
                     progress =  $scope.myapplication.myTopics[i].cobj == undefined || $scope.myapplication.myTopics[i].cobj == 0 ? 0 : ($scope.myapplication.myTopics[i].cobj/$scope.myapplication.myTopics[i].tobj)*100;
               
                   }
             
                   if( (progress == Math.floor(progress))  )
                   {
                       progress = progress;
                   }else
                   {
                       var n = progress.toFixed();
                       progress = n;
                   };
    		       //ocomp =  +$scope.myapplication.myTopics[k].cobj;
                // alert($scope.myapplication.myTopics[i].cdoc)
            
                   lastact = $scope.myapplication.myTopics[i].lact  ;
         
    		       if($scope.myapplication.myTopics[i].tvid == 0 || $scope.myapplication.myTopics[i].tvid == '0' || $scope.myapplication.myTopics[i].tvid == undefined){
    				   vprog = 'N/A';
    			   }else{
    				   vprog = (+$scope.myapplication.myTopics[i].cvid / +$scope.myapplication.myTopics[i].tvid)*100; 
    				   	if( (vprog == Math.floor(vprog)) )
	   	   				{
	       				   vprog = vprog;
	   	   				}else
	   	   				{
	   	   					vprog = vprog.toFixed();
	   	   				};
    			   }
           
    			   if($scope.myapplication.myTopics[i].tasmt == 0 || $scope.myapplication.myTopics[i].tasmt == '0' || $scope.myapplication.myTopics[i].tasmt == undefined){
    				   asmtprog = 'N/A';
    			   }else{
    				   asmtprog = (+$scope.myapplication.myTopics[i].casmt / +$scope.myapplication.myTopics[i].tasmt)*100; 
    				   	if( (asmtprog == Math.floor(asmtprog)) )
	   	   				{
    				   		asmtprog = asmtprog;
	   	   				}else
	   	   				{
	   	   				asmtprog = asmtprog.toFixed(2);
	   	   				};
    			   }
               
    			   if($scope.myapplication.myTopics[i].tdoc == 0 || $scope.myapplication.myTopics[i].tdoc == '0' || $scope.myapplication.myTopics[i].tdoc == undefined){
    				   dprog = 'N/A';
    			   }else{
    				   dprog = (+$scope.myapplication.myTopics[i].cdoc / +$scope.myapplication.myTopics[i].tdoc)*100; 
    				   	if( (dprog == Math.floor(dprog)) )
	   	   				{
    				   		dprog = dprog;
	   	   				}else
	   	   				{
	   	   					dprog = dprog.toFixed(2);
	   	   				};
    			   }
             
    			   if($scope.myapplication.myTopics[i].tsq == 0 || $scope.myapplication.myTopics[i].tsq == '0' || $scope.myapplication.myTopics[i].tsq == undefined){
    				   sqprog = 'N/A';
    			   }else{
    				   sqprog = (+$scope.myapplication.myTopics[i].csq / +$scope.myapplication.myTopics[i].tsq)*100; 
    				   	if( (sqprog == Math.floor(sqprog)) )
	   	   				{
    				   		sqprog = sqprog;
	   	   				}else
	   	   				{
	   	   				sqprog = sqprog.toFixed(2);
	   	   				};
    			   }
  
    			   if($scope.myapplication.myTopics[i].tfq == 0 || $scope.myapplication.myTopics[i].tfq == '0' || $scope.myapplication.myTopics[i].tfq == undefined){
    				   fqprog = 'N/A';
    			   }else{
    				   fqprog = (+$scope.myapplication.myTopics[i].cfq / +$scope.myapplication.myTopics[i].tfq)*100; 
    				   	if( (fqprog == Math.floor(fqprog)) )
	   	   				{
    				   		fqprog = fqprog;
	   	   				}else
	   	   				{
	   	   				fqprog = fqprog.toFixed(2);
	   	   				};
    			   }
    			   
    			
    		   }
    		
    		   tprogress.push({input: 'Completed',value: progress});
    		   var notcomp = 100 - +progress;
    		   tprogress.push({input: 'Not Completed',value: notcomp});
    		   if($scope.myapplication.myTopics[i].tvid == 0 || $scope.myapplication.myTopics[i].tvid == '0'){
				   vprog = 'N/A';
			   }
    		   if($scope.myapplication.myTopics[i].tdoc == 0 || $scope.myapplication.myTopics[i].tdoc == '0'){
				   dprog = 'N/A';
			   }
    		   if($scope.myapplication.myTopics[i].tsq == 0 || $scope.myapplication.myTopics[i].tsq == '0'){
    			   sqprog = 'N/A';
			   }
    		   if($scope.myapplication.myTopics[i].tfq == 0 || $scope.myapplication.myTopics[i].tfq == '0'){
				   fqprog = 'N/A';
			   }
    		   if($scope.myapplication.myTopics[i].tasmt == 0 || $scope.myapplication.myTopics[i].tasmt == '0'){
				   asmtprog = 'N/A';
			   }
    		  
    		   $scope.myapplication.myTopics[i].vprog = vprog;
    		   $scope.myapplication.myTopics[i].dprog = dprog;
    		   $scope.myapplication.myTopics[i].sqprog = sqprog;
    		   $scope.myapplication.myTopics[i].fqprog = fqprog;
    		   $scope.myapplication.myTopics[i].asmtprog = asmtprog;
    		   $scope.myapplication.myTopics[i].progress = progress;
    		   var bpid = $scope.myapplication.myTopics[i].bpid;
    		   var date = new Date($scope.myapplication.myTopics[i].sd * 1000);
    		   var monthNames = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    	            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];
        	   var year = date.getFullYear();
        	   var month = monthNames[date.getMonth()];
        	   var date1 = date.getDate();
        	   var sdate = date1+" "+month+", "+year;
        	   $scope.myapplication.myTopics[i].sdate = sdate;
        	   if(lastact == undefined){
        		   $scope.myapplication.myTopics[i].ldate = '-';
        	   }else{
        		   date = new Date(lastact * 1000);
            	   year = date.getFullYear();
            	   month = date.getMonth() + 1;
            	   date1 = date.getDate();
            	   sdate = date1+" "+month+", "+year;
            	   
            	   $scope.myapplication.myTopics[i].ldate = moment.utc(date).fromNow();;
        	   }
        	   
        	   $scope.myapplication.myTopics[i].tableParams = new NgTableParams({
                   page: 1,            // show first page
                   count: 10          // count per page
               }, {
                 dataset:   qscores
               });
        	   
        	   $scope.myapplication.myTopics[i].options2 = {
			            chart: {

			                type: 'pieChart',
			                height:190,
			                donut: true,
			                donutRatio: 0.7,
			                x: function(d){return d.key;},
			                y: function(d){return d.y;},
			                showLabels: false,
			                showLegend:false,
			                pie: {
			                    startAngle: function(d) { return d.startAngle },
			                    endAngle: function(d) { return d.endAngle }
			                },
			                duration: 500,
			                legend: {
			                	maxKeyLength: 100,
			                	width: 100,
			                    margin: {
			                        top: 3,
			                        right: 50,
			                        bottom: 0,
			                        left: 130
			                    },
			                    rightAlign: true, 
			                    align: true
			                },
			                margin: {
		                        top: 0,
		                        right: 0,
		                        bottom: 0,
		                        left: 10
		                    }, title:  progress + "%",
			                
			                labelsOutside: false,
			                padAngle: 0.01
			            }
			        };
       
        	   $scope.myapplication.myTopics[i].data2 = [{
		                key: 'Completed',
		                y: progress,
		                color: '#11316C'
		            },
		            {
		                key: 'Not Completed',
		                y: (100 - +progress),
		                color: "grey"
		            }];
        	   
        	 
        	   $scope.myapplication.myTopics[i].options = {
                       chart: {
                           type: 'multiBarHorizontalChart',
                           height: 190,
                           x: function(d){return d.label;},
                           y: function(d){return d.value;},            
                           showControls: false,
                           showValues: false,       
                           showLegend: false,
                           groupSpacing: .4,
                           yDomain: [0,100],
                           margin: {
                               left: 180,
                               bottom: 60,
                               top:0
                             },
                           xAxis: {
                               showMaxMin: false
                           },
                           yAxis: {
                            
                               showMaxMin: true
                           }
                       }
                   };
        	
        	   $scope.myapplication.myTopics[i].data  = [
                   {              
                       "color": "#B3BCC1",
                       "values": [  {
                               "label" : "Videos/Audios" ,
                               "value" : vprog
                           }
                       ]
                   
                   },  {
                      
                       "color": "#D3B0B0",
                       "values": [  {
                    	   "label" : "Documents" ,
                           "value" : dprog
                           }
                       ]
                   
                   }
                   ,  {
                      
                       "color": "#ABCCE8",
                       "values": [  {
                    	   "label" : "Knowledge Checks" ,
                           "value" : fqprog
                           }
                       ]
                   
                   }
                   ,  {
                      
                       "color": "#626262",
                       "values": [  {
                    	   "label" : "Assessments" ,
                           "value" : sqprog
                           }
                       ]
                   
                   }
                   ,  {
                      
                       "color": "#d62728",
                       "values": [  {
                    	   "label" : "Assignments" ,
                           "value" : asmtprog
                           }
                       ]
                   
                   }
                   
                   ]
        	 
			 
			 		
        	   
           } 
       }
    
        $scope.tableParams = new NgTableParams({
            page: 1,            // show first page
            count: 10          // count per page
        }, {
          dataset:   $scope.myapplication.myTopics
        });
        if($window.innerWidth > 767){
    		$scope.showapppage = false;
    		$scope.showpage = true;
    		             
       
        	
    	} else{
    		$scope.showpage = false;
    		$scope.showapppage = true;
    		
    	}
        $(window).resize(function() {
     	   
  	      $scope.$apply(function() {
  	        $scope.windowWidth = $( window ).width();
  	        if($scope.windowWidth < 767){
  	        	$scope.showpage = false;
  	        	$scope.showapppage = true;
  	        }
  	        if($scope.windowWidth > 767	){
  	        	$scope.showpage = true;
  	        	$scope.showapppage = false;
  	        }
  	       /* if($window.innerWidth > 1024){
  	        	$scope.test1($window.innerWidth);
  	        	
  		    	
  			}else{
  			  
  				$scope.test1($window.innerWidth);
  				
  			}*/
  	      });
  	    });
        $scope.iframeHeight = $window.innerHeight;
    }
    $scope.myapplication();
    
    $scope.backbtn = function()
    {
        $scope.applist = true;
        $scope.userapp = null;
    }
    $scope.viewprogram = function(program)
    {
     $scope.prog = {};
     $scope.showlist = false;
     $scope.prog = program;
     $scope.overview = true;
  
    }
    $scope.back = function()
    {
     $scope.showlist = true;
    }
    $scope.myprograms = function()
    {
        if($scope.online == true){
            $scope.loading = true;
        $window.location.href = '#mytopics';
        
        }
    }
   
    $scope.dashboard = function()
    {
        if($scope.online == true){
            $scope.loading = true;
        $window.location.href = '#dashboard';
        
        }
    }
    $scope.discussion = function()
    {
        if($scope.online == true){
            $scope.loading = true;
        $window.location.href = '#discussion';
        
        }
    }
    $scope.openprogram = function(program){
        if(program.pid != undefined)
        {
           
            $scope.loading = true;
             var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
             $scope.decry=JSON.parse(decrypted);
             $scope.decry["pid"] = program.pid;
             $scope.decry["bpid"] = program.bpid;
             localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
             window.navigating = true;
            $window.location.href = '#openprogram';
        }

    }
};
app.controller('myprogressCtrl', myprogressCtrl);
myprogressCtrl.$inject = ['$scope', '$http', '$location', '$window','$cookies', 'config','$crypto','myprogress','NgTableParams'];

app.factory("getmyprogress", function($window, $q, config,$crypto){
    return {
    	getmyprogress: function(){
    	    window.navigating = true;
    	    //alert(localStorage.getItem("740a2y1e"));
    	
    		if(localStorage.getItem("740a2y1e") !=undefined  ){
    		    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
                var decry=JSON.parse(decrypted);
               
    		var myprogress;
			AWSCognito.config.region =  config.reg;
			AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
    	        IdentityPoolId: decry.iid
    	    });
    	   var fbuser = localStorage.getItem("fbuser");
    		var poolData = { UserPoolId : decry.uid,
    		        ClientId : decry.cid
    		    };
    		
    		var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
    		
    		var cognitoUser = userPool.getCurrentUser();
    		
    	   if (cognitoUser != null && decry.id != null) {
    		   myprogress = getusersession();
    		 
    		  
    	    }else if(fbuser != null && decry.id != null){
    	    	
    	    	myprogress = getfbusersession();
    	    }
    	    else {
    	     
    	    	localStorage.clear();
    	    	$window.location.href = '#page';
    	    }
    	   function getalltopics(accessKeyId, secretAccessKey, sessionToken, token){

    			
    			var topics;
    			
    			if(decry.api === undefined){
    			   
    			    var apigClient = apigClientFactory.newClient({
                        
                    });
    			}else{
    			    
    			    var apigClient = apigClientFactory.newClient({
                        invokeUrl: decry.api,
                    });
    			}
    			
    			var params1 = {};
    			
    			var id = decry.id;
    			
    				var body = { 
    						id : id,
    						iid : decry.iid
    						};
    				
    				var additionalParams = {
                            headers: {Authorization : token
                            }
                      };
    			  
    			
    				var to = apigClient.getUserDataWebPost(params1, body, additionalParams)
    				.then(function(result){
    				 
    				   var json = JSON.stringify(result.data);
    				   var topic = json.toString();
    				  
    				    if(topic.substring(1, 10) == "No Topics"){				    	
    				    	var obj = {myTopics: []}
    				    	obj = JSON.stringify(obj);
    				    	return obj;
    				    }else{
    				   
    				    	 topic = JSON.parse(topic);
    				    	 var  topicids = [];
    				    	 var tp = [];
    				    	 var otopics = [];
    				    	 for(var i =0;i < topic.Records.length;i++ ){
    				    			
    				    		var abc = topic.Records[i].Value;
    				    		var bcd  = JSON.parse(abc);
    				    
    				   
    				    		
    				    			topicids.push(topic.Records[i].Key);
    				    			var obj = {
        				    				tid: topic.Records[i].Key,
        				    				tp : bcd.tp,
        				    				sd : bcd.sd,
        				    				tobj : bcd.tobj,
        				    				tvid : bcd.tvid,
        				    				tdoc : bcd.tdoc,
        				    				tfq : bcd.tfq,
        				    				tsq : bcd.tsq,
        				    				tasmt : bcd.tasmt,
        				    				cobj : bcd.cobj,
        				    				cvid : bcd.cvid,
        				    				cdoc : bcd.cdoc,
        				    				cfq : bcd.cfq,
        				    				csq : bcd.csq,
        				    				casmt : bcd.casmt,
        				    				lact :bcd.lact
        				    		}
    				    			
    				    			tp.push(obj);
    				    		
    				    		
    				    	
    				    		//tp.push(obj);
    				    	 }
    				    
    				    	 if(!topicids.length > 0){
    				    	    
    				    		 var obj = {myTopics: []}
    				    		 obj = JSON.stringify(obj);
    	    				    	return obj;
    				    	 }else{
    				    	 var body = {
    				    			 topicids : topicids,
    				    			 oid: decry.oid,
    				    			 eid: decry.email
    									 };
    				   
    				    	var topics1 = apigClient.getMyTopicsPost(params1, body, additionalParams)
    							.then(function(result){
    							
    							   var json = JSON.stringify(result.data);
    							    var topic1 = json.toString();
    						
    							    var topic2 = JSON.parse(topic1);
    							
    							    for(var k=0; k < topic2.myTopics.length;k++){
    							    	for(var m=0; m < tp.length ;m++){
    							    		if(topic2.myTopics[k].tid == tp[m].tid){
    							    			
    							    			topic2.myTopics[k].tp =  tp[m].tp;
    							    			topic2.myTopics[k].sd =  tp[m].sd;
    							    			topic2.myTopics[k].tobj =  tp[m].tobj;
    							    			topic2.myTopics[k].tvid =  tp[m].tvid;
    							    			topic2.myTopics[k].tdoc =  tp[m].tdoc;  
    							    			topic2.myTopics[k].tfq = tp[m].tfq; 
    							    			topic2.myTopics[k].tsq = tp[m].tsq; 							    			
    							    			topic2.myTopics[k].tasmt =  tp[m].tasmt;
    							    			topic2.myTopics[k].cobj =  tp[m].cobj;
    							    			topic2.myTopics[k].cdoc =  tp[m].cdoc;
    							    			topic2.myTopics[k].cvid =  tp[m].cvid;
    							    			topic2.myTopics[k].csq =  tp[m].csq;
    							    			topic2.myTopics[k].cfq =  tp[m].cfq;
    							    			topic2.myTopics[k].lact =  tp[m].lact;
    							    		}
    							    	}
    							    }
    							    topic2.otopics = otopics;
    							   
    							    var body = {
    	    				    			 oid: decry.oid,
    	    				    			 eid: decry.email
    	    									 };
    							
    	    				    	var topics2 = apigClient.getAllQuizScoresPost(params1, body, additionalParams)
    	    							.then(function(result){
    	    							 
    	    							   if(result.data.length != 0  ){
    	    							       
    	    							       var json = JSON.stringify(result.data);
                                               topic2.quizscores = json;
    	    							   }else
    	    							       {
    	    							          topic2.quizscores = [];
    	    							         
    	    							       }
    	    								
    	    								topic2 = JSON.stringify(topic2);
    	    								
    	    								return topic2;
    	    							}).catch( function(result){
        							   
        							    	return $q.when(false);
        							    	
        							    })
    							   
    							    return $q.when(topics2);
    							    	
    							    }).catch( function(result){
    							    	
    							    	return $q.when(false);
    							    	
    							    })
    				    	return topics1;
    				    }
    				    }
    				    
    				 
    				    
    				    	
    				    }).catch( function(result){
    				    	
    				        var json = JSON.stringify(result.data);
                            var topic1 = json.toString();
                            var topic2 = JSON.parse(topic1);
    				    	 
    						    
    				    	return $q.when(topic2);
    				    	
    				    });
    				return $q.when(to);
    		
    	   }
    	   function gettoken(token, id){
    		  
    	     
    	       if(decry.api === undefined){
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                
    	            });
    	        }else{
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                invokeUrl: decry.api,
    	            });
    	        }
 				var params1 = {};
 				var body = {type: id,token : token,oid : decry.oid};
 				var additionalParams = {};
 				var abcd = apigClient.getCredentialsWebPost(params1, body, additionalParams)
 				.then(function(result1){
 				   var tjson = JSON.stringify(result1.data);
 				   tjson = tjson.toString();
 				   
 				   tjson = JSON.parse(tjson);
 				  var abc = getalltopics(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken); 
 				  
 				    return $q.when(abc);
 				    	
 				    }).catch( function(result){
 				       if($rootScope.online == false)
 				          {		
 				           			            
 				               $window.location.href = '#network';   		                      
 				          }else
 				              {
     				            swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
     				                allowEscapeKey:false, buttonsStyling: false});
     	                        localStorage.clear();
     	                        $window.location.href = '#page';
 				              }
 				    	
 				    	
 				    })
 				return $q.when(abcd);
 	 	   }
 	    function getusersession(){
 	    	 return new Promise((resolve, reject) => {
 	 			 cognitoUser.getSession((err, session) =>{
 	 	            if (err) {
     	 	              if($rootScope.online == false)
                          {
     	 	                 $window.location.href = '#network';
                          }else
                              {
         				    	swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonClass: 'confirmClass',allowOutsideClick: false,
         			                allowEscapeKey:false, buttonsStyling: false});
         	 	            	localStorage.clear();
         	 	            	$window.location.href = '#page';
                              }
 	 	            }else{
 	 	            	var token = session.idToken.jwtToken;
 	 	            	//var abcc = gettoken(token, '0');
 	 	            	var abcc = getalltopics('a', 'a', 'a', token); 
 	 	             
 	 	            	resolve(abcc)
 	 	            	return $q.when(abcc);
 	 	        
 	 	            }
 	  		  	});
 	 		})
 	    }
 	   function getfbusersession(){
 		  return new Promise((resolve, reject) => {
 			 Facebook.getLoginStatus( (response)=> {
 	 			if (response.status == 'connected') {
 	 		       var fbjson = JSON.stringify(response);
 	 		       fbjson = fbjson.toString();
 	 			   var fbtoken = fbjson.accessToken;
 	 			   
 	 			   if(fbtoken == undefined){
 	 				   var fbtoken1 = JSON.parse(fbjson);
 	 				  fbtoken = fbtoken1.authResponse.accessToken;
 	 			   }
 	 			  
 	 			   var abcc = gettoken(fbtoken, '1');
	            	resolve(abcc)
	            	return $q.when(abcc);
 	 			}else{
				    	swal({title: "Oooops!", text: "Facebook session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
				    	localStorage.clear();
				    	$window.location.href = '#page';
 	 			}
 	 			}); 
 		  })
 			
 	   }
    	   return $q.when(myprogress);
    	}else{
    	  
    		localStorage.clear();
	    	$window.location.href = '#page';
    	}
	}
    };
});
