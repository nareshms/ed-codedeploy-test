var cimageCtrl = function ($scope, $http, $location, $window, $cookies, $uibModalInstance, cimage, config,$crypto) {
	
	
$scope.header = {name: "header.html", url: "header.html"};

$scope.home = function(){
    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
    $scope.decry=JSON.parse(decrypted);
    
	AWSCognito.config.region = config.reg;
    AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: $scope.decry.iid
    });
	var poolData = { UserPoolId : $scope.decry.uid,
	        ClientId : $scope.decry.cid
	    };
	var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
	
	var cognitoUser = userPool.getCurrentUser();
	
    if (cognitoUser != null && $scope.decry.email != null) {
    	$scope.sessionexpired = true;
        cognitoUser.getSession(function(err, session) {
            if (err) {
            	
				swal({title: "Oooops!", text: "Session has expired. Please Login again now.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "orange"});

                $window.location.href = '#login';
                return;
            }else{        	
                $scope.cimageview();   
            	$scope.sessionexpired = false;
            }
           
        });
        if($scope.sessionexpired == true && $scope.decry.email != null){
        	$scope.cimage = cimage;
			$scope.cimage = JSON.parse($scope.cimage);
        	$scope.sessionexpired = false;
        }
    }
    else {
        if(localStorage.getItem("fbuser") != null)
            {
                $scope.cimageview();  
            }else
                {
                  $window.location.href = '#login';
                }
    	
    }
	
};
$scope.cimageview = function(){
    $scope.cimage = cimage;
    $scope.cimage = JSON.parse($scope.cimage);

    
          var base64    = ''
          var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

          var bytes         = new Uint8Array($scope.cimage.data)
          var byteLength    = bytes.byteLength
          var byteRemainder = byteLength % 3
          var mainLength    = byteLength - byteRemainder

          var a, b, c, d
          var chunk

          // Main loop deals with bytes in chunks of 3
          for (var i = 0; i < mainLength; i = i + 3) {
            // Combine the three bytes into a single integer
            chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2]

            // Use bitmasks to extract 6-bit segments from the triplet
            a = (chunk & 16515072) >> 18 // 16515072 = (2^6 - 1) << 18
            b = (chunk & 258048)   >> 12 // 258048   = (2^6 - 1) << 12
            c = (chunk & 4032)     >>  6 // 4032     = (2^6 - 1) << 6
            d = chunk & 63               // 63       = 2^6 - 1

            // Convert the raw binary segments to the appropriate ASCII encoding
            base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d]
          }

          // Deal with the remaining bytes and padding
          if (byteRemainder == 1) {
            chunk = bytes[mainLength]

            a = (chunk & 252) >> 2 // 252 = (2^6 - 1) << 2

            // Set the 4 least significant bits to zero
            b = (chunk & 3)   << 4 // 3   = 2^2 - 1

            base64 += encodings[a] + encodings[b] + '=='
          } else if (byteRemainder == 2) {
            chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1]

            a = (chunk & 64512) >> 10 // 64512 = (2^6 - 1) << 10
            b = (chunk & 1008)  >>  4 // 1008  = (2^6 - 1) << 4

            // Set the 2 least significant bits to zero
            c = (chunk & 15)    <<  2 // 15    = 2^4 - 1

            base64 += encodings[a] + encodings[b] + encodings[c] + '='
          }
          
            $scope.cimage = "data:image/jpeg;base64," +  base64;
}

$scope.home();	

$scope.close = function(){
	$uibModalInstance.close();
};

	
};
app.controller('cimageCtrl', cimageCtrl);
cimageCtrl.$inject = ['$scope', '$http', '$location', '$window','$cookies', '$uibModalInstance', 'cimage','config','$crypto'];

app.factory("getcimage", function($window, $q, config,$crypto){
    return {
    	getcimage: function(){
    		

            //window.navigating = true;
            if(localStorage.getItem("740a2y1e") == null || localStorage.getItem("740a2y1e") ==  ''){
                
                $window.location.href = '#page';
                
            }else{
                var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
                var decry=JSON.parse(decrypted);
                    var cimage;
                    AWSCognito.config.region =  config.reg;
                    AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
                        IdentityPoolId: decry.iid
                    });
                   var fbuser = localStorage.getItem("fbuser");
                    var poolData = { UserPoolId : decry.uid,
                            ClientId : decry.cid
                        };
             
                    var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
                    
                    var cognitoUser = userPool.getCurrentUser();
                  
                   if (cognitoUser != null && decry.id != null) {
                      
                       cimage = getusersession();
                       
                      
                    }else if(fbuser != null && decry.id != null){
                        
                        cimage = getfbusersession();
                    }
                    else {
                        localStorage.clear();
                        $window.location.href = '#page';
                    }
                   
                   
                   
                      function Credentialsweb(accessKeyId, secretAccessKey, sessionToken,token){
                          if(decry.api === undefined){
                              
                              var apigClient = apigClientFactory.newClient({
                                  
                              });
                          }else{
                              
                              var apigClient = apigClientFactory.newClient({
                                  invokeUrl: decry.api,
                              });
                          }
                     
                     var params = {};
                        
                       var body = {
                               eid: decry.email,
                               oid: decry.oid,
                               userid : decry.id,
                               topicid: decry.ctopicid
                                };
                    
                       var additionalParams = {
                               headers: {Authorization : token
                               }
                         };
                          var searchresult1 = apigClient.getUserCertPost(params, body, additionalParams)
                           .then(function(result){
                             
                           
                              var json = JSON.stringify(result.data);
                             
                               var result = json.toString();
                               return $q.when(result);
                                   
                               }).catch( function(result){
                                   
                                   return $q.when(false);
                                   
                               });
                           return $q.when(searchresult1);
          
                    }
                    function getusersession(){
                      

                        return new Promise((resolve, reject) => {
                           cognitoUser.getSession((err, session) =>{
                              if (err) {
                                  swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                                  localStorage.clear();
                                  $window.location.href = '#page';
                              }else{
                                  
                                  var token = session.idToken.jwtToken;
                                 // var abcc = gettoken(token, '0');
                                  var abcc = Credentialsweb('a', 'a', 'a',token);
                                  resolve(abcc)
                                  return $q.when(abcc);
                          
                              }
                          });
                      })
                   }
                    function getfbusersession(){
                        return new Promise((resolve, reject) => {
                           Facebook.getLoginStatus( (response)=> {
                              if (response.status == 'connected') {
                                 var fbjson = JSON.stringify(response);
                                 fbjson = fbjson.toString();
                                 var fbtoken = fbjson.accessToken;
                                 
                                 if(fbtoken == undefined){
                                     var fbtoken1 = JSON.parse(fbjson);
                                    fbtoken = fbtoken1.authResponse.accessToken;
                                 }
                                
                                 var abcc = gettoken(fbtoken, '1');
                                  resolve(abcc)
                                  return $q.when(abcc);
                              }else{
                                      swal({title: "Oooops!", text: "Facebook session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                                      localStorage.clear();
                                      $window.location.href = '#page';
                              }
                              }); 
                        })
                          
                     }
                    function gettoken(token, id){
                        
                        if(decry.api === undefined){
                            
                            var apigClient = apigClientFactory.newClient({
                                
                            });
                        }else{
                            
                            var apigClient = apigClientFactory.newClient({
                                invokeUrl: decry.api,
                            });
                        }
                          var params1 = {};
                          var body = {type: id,token : token,oid : decry.oid};
                          var additionalParams = {};
                        
                          var abcd = apigClient.getCredentialsWebPost(params1, body, additionalParams)
                          .then(function(result1){
                             var tjson = JSON.stringify(result1.data);
                             tjson = tjson.toString();
                             
                             
                             tjson = JSON.parse(tjson);
                            var abc = Credentialsweb(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken); 
                            
                              return $q.when(abc);
                                  
                              }).catch( function(result){
                                  swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                                  localStorage.clear();
                                  $window.location.href = '#page';
                                  
                              })
                          return $q.when(abcd);
                     }
                   
                    return  cimage; 
                   
         
            
           
           
        }
        
        }
    };
});
