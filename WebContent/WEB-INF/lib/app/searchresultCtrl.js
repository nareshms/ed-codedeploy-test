var searchresultCtrl = function ($scope, $http, $location, $window, $cookies, searchresult,config,$crypto) {


$scope.home = function(){
    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
    $scope.decry=JSON.parse(decrypted);

            	$scope.exploreresult = searchresult;
            	if($scope.decry.sid != undefined){
            		$scope.topicimg = config.url+$scope.decry.oid.toLowerCase()+"-resources/images/category-images/"+$scope.decry.sid+".png";
            	
            		$scope.topicimg = "url('"+$scope.topicimg+"')";
            	}else{
            		$scope.topicimg = 'url(lib/images/homepage.jpg)';
            	}
            	$scope.myObj = {'background-color':'grey','overflow':'hidden','background-image': $scope.topicimg,'background-size': 'cover','background-repeat': 'no-repeat'};
            	if($scope.exploreresult == false){
            		$scope.errorpage = true;
            		
            	}else{
    			$scope.exploreresult = JSON.parse($scope.exploreresult);
    			$scope.exploreresult = $scope.exploreresult.hits.hit;
            	$scope.sessionexpired = false;
            	$scope.search = $scope.decry.search;
            	if($scope.exploreresult.length > 0){
            		$scope.showtopics = true;
            		$scope.topics = [];
            		for(var i=0; i < $scope.exploreresult.length; i++){
            			$scope.topicimg = config.url+$scope.decry.oid.toLowerCase()+"-resources/images/topic-images/"+$scope.exploreresult[i].id+".png";
                		$scope.tduration = $scope.exploreresult[i].fields.tduration[0];
                		$scope.ttitle = $scope.exploreresult[i].fields.ttitle[0];
                		$scope.noofnuggets = $scope.exploreresult[i].fields.noofnuggets[0];
                		$scope.tcertauth = $scope.exploreresult[i].fields.tcertauth[0];
                		$scope.tnoofcredits = $scope.exploreresult[i].fields.tnoofcredits[0];
                		$scope.freenavigation = $scope.exploreresult[i].fields.freenavigation[0];
                		var obj = {
                				topicid: $scope.exploreresult[i].id,
                				topicimg: $scope.topicimg,
                				tduration: $scope.tduration,
                				ttitle: $scope.ttitle,
                				noofnuggets: $scope.noofnuggets,
                				tcertauth: $scope.tcertauth,
                				tnoofcredits: $scope.tnoofcredits,
                				freenavigation: $scope.freenavigation
                		}
                		$scope.topics.push(obj);
                	}
            	}else{
            		$scope.showtopics = false;
            	}
            	if($window.innerWidth > 767){
        			$scope.showapppage = false;
        			$scope.showpage = true;
        	    	
        		}else{
        			$scope.showpage = false;
        			$scope.showapppage = true;
        			
        		}
        		
        		$(window).resize(function() {
        		      $scope.$apply(function() {
        		        $scope.windowWidth = $( window ).width();
        		        if($scope.windowWidth < 767){
        		        	$scope.showpage = false;
        		        	$scope.showapppage = true;
        		        }
        		        if($scope.windowWidth > 767	){
        		        	$scope.showpage = true;
        		        	$scope.showapppage = false;
        		        }
        		      });
        		    });
            	}
            	 window.navigating = false;
		};

$scope.home();	

$scope.viewtopic = function(topicid){
	$scope.loading = true;
	 var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
	    $scope.decry=JSON.parse(decrypted);
	    $scope.decry["otopicid"] = topicid;
	    $scope.decry["osearch"] = true;
	    localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
	//localStorage.setItem("osearch", true);
	//localStorage.setItem("otopicid", topicid);
	$window.location.href = '#topic';
};

$scope.back = function(){
	$scope.loading = true;
	$window.location.href = '#explore';
}
	
};
app.controller('searchresultCtrl', searchresultCtrl);
searchresultCtrl.$inject = ['$scope', '$http', '$location', '$window','$cookies','searchresult','config','$crypto'];

app.factory("searchresult", function($window, $q ,config,$crypto){
    return {
    	searchresult: function(){
    	    window.navigating = true;
    		if(localStorage.getItem("740a2y1e") == null || localStorage.getItem("740a2y1e") ==  ''){
    			
    			$window.location.href = '#page';
    			
    		}else{
    		    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
                var decry=JSON.parse(decrypted);
                
    		        var searchresult;
    	            AWSCognito.config.region =  config.reg;
    	            AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
    	                IdentityPoolId: decry.iid
    	            });
    	           var fbuser = localStorage.getItem("fbuser");
    	            var poolData = { UserPoolId : decry.uid,
    	                    ClientId : decry.cid
    	                };
    	     
    	            var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
    	            
    	            var cognitoUser = userPool.getCurrentUser();
    	          
    	           if (cognitoUser != null && decry.id != null) {
    	              
    	               searchresult = getusersession();
    	               
    	              
    	            }else if(fbuser != null && decry.id != null){
    	                
    	                searchresult = getfbusersession();
    	            }
    	            else {
    	                localStorage.clear();
    	                $window.location.href = '#page';
    	            }
    	           
    	           
    	           
                      function Credentialsweb(accessKeyId, secretAccessKey, sessionToken,token){
                          if(decry.api === undefined){
                              
                              var apigClient = apigClientFactory.newClient({
                                  
                              });
                          }else{
                              
                              var apigClient = apigClientFactory.newClient({
                                  invokeUrl: decry.api,
                              });
                          }
                     
                     var params = {};
                    
                       var body = {
                               searchterm: decry.search,
                               oid: decry.oid
                                };
                     
                       var additionalParams = {
                               headers: {Authorization : token
                               }
                         };
                          var searchresult1 = apigClient.searchTopicsPost(params, body, additionalParams)
                           .then(function(result){
                             
                           
                              var json = JSON.stringify(result.data);
                             
                               var result = json.toString();
                               return $q.when(result);
                                   
                               }).catch( function(result){
                                   
                                   return $q.when(false);
                                   
                               });
                           return $q.when(searchresult1);
          
                    }
                    function getusersession(){
                      

                        return new Promise((resolve, reject) => {
                           cognitoUser.getSession((err, session) =>{
                              if (err) {
                                  swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                                  localStorage.clear();
                                  $window.location.href = '#page';
                              }else{
                                  
                                  var token = session.idToken.jwtToken;
                                  //var abcc = gettoken(token, '0');
                                  var abcc = Credentialsweb('a','a', 'a',token); 
                                  resolve(abcc)
                                  return $q.when(abcc);
                          
                              }
                          });
                      })
                   }
                    function getfbusersession(){
                        return new Promise((resolve, reject) => {
                           Facebook.getLoginStatus( (response)=> {
                              if (response.status == 'connected') {
                                 var fbjson = JSON.stringify(response);
                                 fbjson = fbjson.toString();
                                 var fbtoken = fbjson.accessToken;
                                 
                                 if(fbtoken == undefined){
                                     var fbtoken1 = JSON.parse(fbjson);
                                    fbtoken = fbtoken1.authResponse.accessToken;
                                 }
                                
                                 var abcc = gettoken(fbtoken, '1');
                                  resolve(abcc)
                                  return $q.when(abcc);
                              }else{
                                      swal({title: "Oooops!", text: "Facebook session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                                      localStorage.clear();
                                      $window.location.href = '#page';
                              }
                              }); 
                        })
                          
                     }
                    function gettoken(token, id){
                        
                        if(decry.api === undefined){
                            
                            var apigClient = apigClientFactory.newClient({
                                
                            });
                        }else{
                            
                            var apigClient = apigClientFactory.newClient({
                                invokeUrl: decry.api,
                            });
                        }
                          var params1 = {};
                          var body = {type: id,token : token,oid : decry.oid};
                          var additionalParams = {};
                        
                          var abcd = apigClient.getCredentialsWebPost(params1, body, additionalParams)
                          .then(function(result1){
                             var tjson = JSON.stringify(result1.data);
                             tjson = tjson.toString();
                             
                             
                             tjson = JSON.parse(tjson);
                            var abc = Credentialsweb(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken); 
                            
                              return $q.when(abc);
                                  
                              }).catch( function(result){
                                  swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                                  localStorage.clear();
                                  $window.location.href = '#page';
                                  
                              })
                          return $q.when(abcd);
                     }
                   
                    return  searchresult; 
    		       
    	 
			
           
           
        }
    	}
    };
});
