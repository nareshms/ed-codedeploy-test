var dashboardCtrl = function ($scope, $http, $location, $window, $cookies, userdetails, config,$crypto,uiCalendarConfig) {

	
$scope.header = {name: "header.html", url: "header.html"};

$scope.openprogram = function(topic){
    $scope.loading = true;

     var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
     $scope.decry=JSON.parse(decrypted);
     $scope.decry["otopicid"] = topic.tid;
     localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
     window.navigating = true;
    $window.location.href = '#topic';

}
$scope.close = function(){
	
};
$scope.openmyprogram = function(program){
	
    if(program != undefined)
    {
       	$scope.loading = true;
    	 var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
         $scope.decry=JSON.parse(decrypted);
         $scope.decry["bpid"] = program.bpid;
         $scope.decry["pid"] = program.tid;
         localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
    	 window.navigating = true;
    	$window.location.href = '#openprogram';
    }

}

$scope.myprogramsfun = function()

{
    if($scope.online == true){
        $scope.loading = true;
       $window.location.href = '#mytopics';
    
    }
}

$scope.myprogress = function()
{
    if($scope.online == true){
        $scope.loading = true;
    $window.location.href = '#myprogress';
    
    }
}


$scope.home = function(){
    
    swal.close();
    //$window.location.href = '#network';
 
    //location.reload(false);

	$scope.alltopics = userdetails;
	       
     $scope.decrypt = JSON.parse($crypto.decrypt(localStorage.getItem("740a2y1e"), config.key));
	$scope.alltopics = JSON.parse($scope.alltopics);

	$scope.programs = $scope.alltopics.toptopics.filter(function(item) {
        return item.publishunpublish == 1 || item.publishunpublish == undefined;
    });
    for(var m=0; m < $scope.programs.length; m++){
        //$scope.toptopics[m].tid = $scope.toptopics[m].id;
        $scope.toptopicimg = config.url+$scope.decrypt.oid.toLowerCase()+"-resources/images/topic-images/"+$scope.programs[m].tid+".png";
        $scope.programs[m].programimg = $scope.toptopicimg;
    }

	if($scope.alltopics == false){
	   
	   
		$scope.errorpage = true;
		$scope.othererrmsg = true;
        if($scope.online == false){
            $scope.othererrmsg = false;
        }
	}else{
	if($window.innerWidth > 767){
		$scope.showapppage = false;
		$scope.showpage = true;
    	
	}else{
		$scope.showpage = false;
		$scope.showapppage = true;
		
	}
	/*if($window.innerWidth > 1024){
		$scope.nooftopics = 4;
    	
	}else{
		$scope.nooftopics = 3;
		
	}*/
	$scope.events = [];
	if($scope.alltopics.assignedtopics.length > 0){
		$scope.assignedprograms = $scope.alltopics.assignedtopics.length;
	}else {
		$scope.assignedprograms = 0;
	}
	if($scope.alltopics.myTopics == false){
		$scope.myprograms = 0;
		$scope.compprograms = 0;
	}else {
		$scope.topics = $scope.alltopics.myTopics;
		$scope.myprograms = 0;
		$scope.compprograms = 0;
		
		for(var i=0; i < $scope.topics.length; i++){
			
			if($scope.topics[i].tp.substring(0, 1) == 1)
			{
				+$scope.myprograms++;
			}else{
				+$scope.compprograms++;
			}
    	    if($scope.topics[i].assessments != undefined ){
    	    	if($scope.topics[i].assessments.length > 0 ){
    	    		for(var n=0;n < $scope.topics[i].assessments.length;n++){
    	    			if($scope.topics[i].assessments[n].date != undefined){
    	    			var obj = {};
        	    		obj.start = new Date($scope.topics[i].assessments[n].date);
        	    		obj.color = "#E77C2D";
        	    		obj.title = $scope.topics[i].assessments[n].name;
        	    		$scope.events.push(obj);
    	    			}
    	    		}
    	    		
    	    	}
    	    }
    	}
    	
		
	}
	
	$scope.SelectedEvent = null;  
    var isFirstTime = true;  
  
   /* $scope.events = [{
    	"start": "2019-06-24T07:00:00.000Z",
    	
    	"title": "Assignment",
    	"color": "#E77C2D"
    },{
    	"start": "2019-06-24T07:00:00.000Z",
    	
    	"title": "Weekly Assignment",
    	"color": "#E77C2D"
    },{
    	"start": "2019-06-24T07:00:00.000Z",
    	
    	"title": "Assignment",
    	"color": "#E77C2D"
    },{
    	"start": "2019-05-24T07:00:00.000Z",
    	
    	"title": "New Assignment",
    	"color": "#E77C2D"
    }];*/

 
    $scope.eventSources = [$scope.events];  
 // $scope.eventSources.push($scope.events)
  $scope.count=$scope.events.length;
  

    $scope.uiConfig = {  
        calendar: {  
            height: 450,  
            editable: true,  
            displayEventTime: false, 
            eventLimit:2,
            header: {  
                //left: 'month ',  
                
                right:'today prev,next'  
            },  
            dayClick: function (day) {  
            	
                $scope.SelectedEvent = $scope.events;
                $scope.SelectedEvent = $scope.SelectedEvent.filter(function(item) {
                
        		    return moment.utc(item.start).format('ddd MMM DD YYYY') == day.format('ddd MMM DD YYYY');  
        		 });
                
            },  
            eventAfterAllRender: function () {  
                if ($scope.events.length > 0 && isFirstTime) {  
                    uiCalendarConfig.calendars.myCalendar.fullCalendar('gotoDate', $scope.events[0].start);  
                    isFirstTime = false;  
                }  
            }  
        }  
    };
	
    $scope.close = function(){
    	$scope.myVar = !$scope.myVar;
    };
	$scope.nooftopics = 3;
	$(window).resize(function() {
	   
	      $scope.$apply(function() {
	        $scope.windowWidth = $( window ).width();
	        if($scope.windowWidth < 767){
	        	$scope.showpage = false;
	        	$scope.showapppage = true;
	        }
	        if($scope.windowWidth > 767	){
	        	$scope.showpage = true;
	        	$scope.showapppage = false;
	        }
	       /* if($window.innerWidth > 1024){
	        	$scope.test1($window.innerWidth);
		    	
			}else{
				$scope.test1($window.innerWidth);
				
			}*/
	      });
	    });
	
	

	
	$scope.iframeHeight = $window.innerHeight;

	
    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
    $scope.decrypt=JSON.parse(decrypted);
    
	}
    $(window).resize(function() {
        
        $scope.$apply(function() {
          $scope.windowWidth = $( window ).width();
          if($scope.windowWidth < 767){
              $scope.showpage = false;
              $scope.showapppage = true;
          }
          if($scope.windowWidth > 767 ){
              $scope.showpage = true;
              $scope.showapppage = false;
          }
        
        });
      });
	$scope.$watch('online', function() {
        if($scope.online == true){          
            if($scope.errorpage == true){
                $scope.loading = true;
                $window.location.href = '#topic';
            }
        }else{
            $scope.loading = false;
        }
     });
	 window.navigating = false;
};
$scope.scrollTo = function (target){
};


$scope.completedtab = function()
{
    $scope.notstart = false;
    $scope.tcompleted = true;
    $scope.inprogress = false;
   if($scope.alltopics.myTopics != false)
     {
        var filtered = $scope.alltopics.myTopics.filter(function(item) { 
            return item.tp.charAt(0) == 2 || item.tp.charAt(0) == 3;  
         });
    
        if(filtered != undefined && filtered != '' && filtered != null)
            {
             
                $scope.topics = filtered; 
                for(var i=0; i < $scope.topics.length; i++){
                	if($scope.topics[i].type === undefined ){
            			$scope.topicimg = config.url+$scope.decrypt.oid.toLowerCase()+"-resources/images/topic-images/"+$scope.topics[i].tid+".png";
            		}else{
            			$scope.topicimg = config.url+$scope.decrypt.oid.toLowerCase()+"-resources/images/program-images/"+$scope.topics[i].tid+".png";
            		}
                    $scope.topics[i].topicimg = $scope.topicimg;
           
                }
              
                
               
                $scope.showtopics = true;
            } else{ 
                $scope.showtopics = false;
                }
        
     }else
         {
         $scope.showtopics = false;
         }

}
$scope.inprogresstab = function()
{
    $scope.notstart = false;
    $scope.tcompleted = false;
    $scope.inprogress = true;
    if($scope.alltopics.myTopics != false)
    {
        var filtered = $scope.alltopics.myTopics.filter(function(item) { 
            return item.tp.charAt(0) == 1 ;  
         });
      
        if(filtered != undefined && filtered != '' && filtered != null)
            {
        
            $scope.topics = filtered; 
            $scope.showtopics = true;
     
            } else{ $scope.showtopics = false;}
    }else
        {
        $scope.showtopics = false;
        }

}
$scope.notstarttab = function()
{
    $scope.notstart = true;
    $scope.tcompleted = false;
    $scope.inprogress = false;
  
  
    if($scope.alltopics.assignedtopics != false )
        {
    
            $scope.topics =JSON.parse(userdetails).assignedtopics; 
         
            for(var i=0; i < $scope.topics.length; i++){
            	if($scope.topics[i].type === undefined ){
        			$scope.topicimg = config.url+$scope.decrypt.oid.toLowerCase()+"-resources/images/topic-images/"+$scope.topics[i].tid+".png";
        		}else{
        			$scope.topicimg = config.url+$scope.decrypt.oid.toLowerCase()+"-resources/images/program-images/"+$scope.topics[i].tid+".png";
        		}
                $scope.topics[i].topicimg = $scope.topicimg;
                    if($scope.topics[i].duedate == undefined){
                    
                    }else{
                    
                    var diff = moment.utc($scope.topics[i].duedate).fromNow();
                    if(diff.includes("ago")){
                        diff = 'Overdue';
                    }
                    $scope.topics[i].duedate = diff;
                }
            }
          
            $scope.showtopics = true;
 
        }else{
            $scope.showtopics = false;
        }

}
/*if (performance.navigation.type == 1) {
    performance.navigation.type = 0;
    if(window.navigating == true)
        {  
       
            $window.location.href = '#home';
           
        }
  
  }*/

$(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
       alert()
    }
});
$scope.home();	

$scope.test1 = function(width){

	if(width > 1024){
		$scope.nooftopics = 4;
		$scope.$apply();
	}else{
		$scope.nooftopics = 3;
		$scope.$apply();
	}
};
$scope.opentopic = function(topic){
	$scope.loading = true;
	//localStorage.setItem("otopicid", topic.tid);
	 var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
     $scope.decry=JSON.parse(decrypted);
     $scope.decry["otopicid"] = topic.tid;
     $scope.decry["obtopicid"] = topic.tid;
     localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));
	 window.navigating = true;
	$window.location.href = '#topic';
};

$scope.explore = function(){
	$scope.loading = true;
	$window.location.href = '#explore';
}

$scope.notification = function(){
	$scope.loading = true;
	$window.location.href = '#notification';
}

$scope.badges = function(){
	$scope.loading = true;
	$window.location.href = '#badges';
}
	
};
app.controller('dashboardCtrl', dashboardCtrl);
dashboardCtrl.$inject = ['$scope', '$http', '$location', '$window','$cookies','userdetails', 'config','$crypto','uiCalendarConfig'];

app.factory("getUserdetails", function($window, $q, config,$crypto,$rootScope){
    return {
        
    	getUserdetails: function(){
    	    window.navigating = true;
    	    //alert(localStorage.getItem("740a2y1e"));

    		if(localStorage.getItem("740a2y1e") !=undefined  ){
    		    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
                var decry=JSON.parse(decrypted);
               
    		var userdetails;
			AWSCognito.config.region =  config.reg;
			AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
    	        IdentityPoolId: decry.iid
    	    });
    	   var fbuser = localStorage.getItem("fbuser");
    		var poolData = { UserPoolId : decry.uid,
    		        ClientId : decry.cid
    		    };
    		
    		var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
    		
    		var cognitoUser = userPool.getCurrentUser();
    		
    	   if (cognitoUser != null && decry.id != null) {
    		   userdetails = getusersession();
    		  
    		  
    	    }else if(fbuser != null && decry.id != null){
    	    	
    	    	userdetails = getfbusersession();
    	    }
    	    else {
    	     
    	    	localStorage.clear();
    	    	$window.location.href = '#page';
    	    }
    	   function getalltopics(accessKeyId, secretAccessKey, sessionToken, token){

    			
    			var topics;
    			
    			if(decry.api === undefined){
    			   
    			    var apigClient = apigClientFactory.newClient({
                        
                    });
    			}else{
    			    
    			    var apigClient = apigClientFactory.newClient({
                        invokeUrl: decry.api,
                    });
    			}
    			
    			var params1 = {};
    			
    			var id = decry.id;
    			
    				var body = { 
    						id : id,
    						iid : decry.iid
    						};
    			
    				var additionalParams = {
                            headers: {Authorization : token
                            }
                      };
    			  
    			
    				var to = apigClient.getUserDataWebPost(params1, body, additionalParams)
    				.then(function(result){
    				 
    				   var json = JSON.stringify(result.data);
    				   var topic = json.toString();
    			
    				    if(topic.substring(1, 10) == "No Topics"){
    				    	
    				    	var body1 = {
    				    			 topicids : [],
    				    			 oid: decry.oid,
    				    			 eid: decry.email
    									 };
    				    	
    				    	topic = apigClient.getMyTopicsPost(params1, body1, additionalParams)
    							.then(function(result1){
    								 	
    								
    							   var json1 = JSON.stringify(result1.data);
    							   json1 = json1.toString();
    							   json1 = JSON.parse(json1);
    							   json1.myTopics = false;
    							   json1 = JSON.stringify(json1);
    							
    							    return $q.when(json1);
    							    	
    							    }).catch( function(result){
    							    	return $q.when(false);
    							    	
    							    })
    				    	return topic;
    				    }else{
    				     
    				    	 topic = JSON.parse(topic);
    				    	 var  topicids = [];
    				    	 var tp = [];
    				    	
    				    	 for(var i =0;i < topic.Records.length;i++ ){
    				    		topicids.push(topic.Records[i].Key);
    				    		
    				    		var abc = topic.Records[i].Value;
    				    		var bcd  = JSON.parse(abc);
    				    		 
    				    		/*var obj = {
    				    				tid: topic.Records[i].Key,
    				    				tp : bcd.tp,
    				    				sd : bcd.sd
    				    		}  */  				
    				    	
    				    		if(bcd.type !== undefined){
    				    			//obj.bpid = topic.Records[i].Key;
				    				//obj.pid = bcd.pid;
    				    		    var obj = {
                                            tid:  bcd.pid,
                                            tp : bcd.pp,
                                            sd : bcd.sd
                                    } 
    				    		}else
    				    		    {
        				    		   var  obj = {
                                                tid: topic.Records[i].Key,
                                                tp : bcd.tp,
                                                sd : bcd.sd
                                        } 
    				    		    }
    				    	
    				    		tp.push(obj);
    				    	 }
    				    	 var body = {
    				    			 topicids : topicids,
    				    			 oid: decry.oid,
    				    			 eid: decry.email
    									 };
    				    	 
    				    	var topics1 = apigClient.getMyTopicsPost(params1, body, additionalParams)
    							.then(function(result){
    								
    							   var json = JSON.stringify(result.data);
    							    var topic1 = json.toString();
    							    var topic2 = JSON.parse(topic1);
    							  
    							    for(var k=0; k < topic2.myTopics.length;k++){
    							      
    							    	for(var m=0; m < tp.length ;m++){
    							    	
    							    	
    							    		if(topic2.myTopics[k].tid == tp[m].tid){
    							    			
    							    			topic2.myTopics[k].tp =  tp[m].tp;
    							    			topic2.myTopics[k].sd =  tp[m].sd;
    							    		}
    							    	}
    							    }
    							 
    							    topic2 = JSON.stringify(topic2);
    							 
    							    return $q.when(topic2);
    							    	
    							    }).catch( function(result){
    							    	
    							    	return $q.when(false);
    							    	
    							    })
    				    	
    				    	return topics1;
    				    }
    				    
    				 
    				    
    				    	
    				    }).catch( function(result){
    				    	
    				        var json = JSON.stringify(result.data);
                            var topic1 = json.toString();
                            var topic2 = JSON.parse(topic1);
    				    	 
    						    
    				    	return $q.when(topic2);
    				    	
    				    });
    				return $q.when(to);
    		
    	   }
    	   function gettoken(token, id){
    		  
    	     
    	       if(decry.api === undefined){
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                
    	            });
    	        }else{
    	            
    	            var apigClient = apigClientFactory.newClient({
    	                invokeUrl: decry.api,
    	            });
    	        }
 				var params1 = {};
 				var body = {type: id,token : token,oid : decry.oid};
 				var additionalParams = {};
 				var abcd = apigClient.getCredentialsWebPost(params1, body, additionalParams)
 				.then(function(result1){
 				   var tjson = JSON.stringify(result1.data);
 				   tjson = tjson.toString();
 				   
 				   tjson = JSON.parse(tjson);
 				  var abc = getalltopics(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken); 
 				
 				    return $q.when(abc);
 				    	
 				    }).catch( function(result){
 				       if($rootScope.online == false)
 				          {		
 				           			            
 				               $window.location.href = '#network';   		                      
 				          }else
 				              {
     				            swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
     	                        localStorage.clear();
     	                        $window.location.href = '#page';
 				              }
 				    	
 				    	
 				    })
 				return $q.when(abcd);
 	 	   }
 	    function getusersession(){
 	    	 return new Promise((resolve, reject) => {
 	 			 cognitoUser.getSession((err, session) =>{
 	 	            if (err) {
     	 	              if($rootScope.online == false)
                          {
     	 	                 $window.location.href = '#network';
                          }else
                              {
         				    	swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
         	 	            	localStorage.clear();
         	 	            	$window.location.href = '#page';
                              }
 	 	            }else{
 	 	            	var token = session.idToken.jwtToken;
 	 	            	//var abcc = gettoken(token, '0');
 	 	            	var abcc = getalltopics('a', 'a', 'a', token); 
 	 	             
 	 	            	resolve(abcc)
 	 	            	return $q.when(abcc);
 	 	        
 	 	            }
 	  		  	});
 	 		})
 	    }
 	   function getfbusersession(){
 		  return new Promise((resolve, reject) => {
 			 Facebook.getLoginStatus( (response)=> {
 	 			if (response.status == 'connected') {
 	 		       var fbjson = JSON.stringify(response);
 	 		       fbjson = fbjson.toString();
 	 			   var fbtoken = fbjson.accessToken;
 	 			   
 	 			   if(fbtoken == undefined){
 	 				   var fbtoken1 = JSON.parse(fbjson);
 	 				  fbtoken = fbtoken1.authResponse.accessToken;
 	 			   }
 	 			  
 	 			   var abcc = gettoken(fbtoken, '1');
	            	resolve(abcc)
	            	return $q.when(abcc);
 	 			}else{
				    	swal({title: "Oooops!", text: "Facebook session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
				    	localStorage.clear();
				    	$window.location.href = '#page';
 	 			}
 	 			}); 
 		  })
 			
 	   }
    	   return $q.when(userdetails);
    	}else{
    	  
    		localStorage.clear();
	    	$window.location.href = '#page';
    	}
	}

    };
});



app.directive('setClassWhenAtTop', function ($window) {
    var $win = angular.element($window); // wrap window object as jQuery object

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var topClass = attrs.setClassWhenAtTop, // get CSS class from directive's attribute value
                offsetTop = element.offset().top; // get element's top relative to the document

            $win.on('scroll', function (e) {
                if ($win.scrollTop() >= offsetTop) {
                    element.addClass(topClass);
                } else {
                    element.removeClass(topClass);
                }
            });
        }
    };
});
