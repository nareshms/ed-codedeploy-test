var orgidCtrl = function ($scope, $http, $location, $window, $timeout, $cookies, $uibModal, $uibModalInstance, jwtHelper, config,$crypto) {

$scope.login = function(){
    $(window).resize(function() {
          $scope.$apply(function() {
            $scope.windowWidth = $( window ).width();
            if($scope.windowWidth < 767){
                $uibModalInstance.close();
            }
            if($scope.windowWidth > 767 ){
            }
          });
        });
  
    if($window.innerWidth > 460){
    $scope.showlogin = true;
    $scope.isVisible2 = true;
    
    }else{
        $scope.showlogin = false;
    }
    
};
    
$scope.login();
  
$scope.myFunct = function(keyEvent) {
      if (keyEvent.which === 13)
    	  {$scope.submit();}
          
		}

$scope.submit = function(){
    $scope.error = false;
    $scope.error1 = false;
    $scope.error2 = false;
    if($scope.oid == null || $scope.oid == '' || $scope.oid.replace(/\s/g, '').length === 0 ){
        $scope.error2 = true;
    }else if($scope.oid.length < 3){
        $scope.error = true;
    }else{
        
        $scope.loading = true;
        var apigClient = apigClientFactory.newClient({
        });

        var params = {};
        $scope.orgid1 = $scope.oid.toUpperCase();
        var body = {
                oid : $scope.orgid1
                 };
    
        localStorage.clear();
        $window.sessionStorage.clear();
        var additionalParams = {};
        
        apigClient.getOrgDetailsPost(params, body, additionalParams)
            .then(function(result){
                var json = JSON.stringify(result.data);
               json = json.toString();
              
                $scope.orddetails = JSON.parse(json);   
                if($scope.orddetails.oid == false || $scope.orddetails.oid == 'false'){
                    $scope.error1 = true;
                    $scope.loading = false;
                    $scope.$apply();
                }else{
                   
                    $scope.loading = false;
                    $scope.orgjson = JSON.stringify(result.data);
                    $scope.orgjson = json.toString();
                    $scope.orgjson = JSON.parse( $scope.orgjson); 
                    var data='{ "oid": "'+$scope.orgid1+'","cid": "'+$scope.orgjson.cid+'","iid": "'+$scope.orgjson.iid+'","uid": "'+$scope.orgjson.uid+'","api": "'+$scope.orgjson.api+'","ctype": "'+$scope.orgjson.ctype+'"  } ';
                    var encrypted = $crypto.encrypt(data, config.key);
                    localStorage.setItem("740a2y1e",encrypted);
                    $uibModalInstance.close('orglogin');
                }
                
            });
    }
}

$scope.close = function(){
    $uibModalInstance.dismiss('cancel');
    $window.location.href = '#page';
};

};

app.controller('orgidCtrl', orgidCtrl);
orgidCtrl.$inject = ['$scope', '$http', '$location', '$window', '$timeout', '$cookies','$uibModal','$uibModalInstance','jwtHelper','config','$crypto'];

