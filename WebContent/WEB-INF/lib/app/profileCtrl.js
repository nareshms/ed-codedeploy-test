var profileCtrl = function ($scope, $http, $location, $window, $cookies, certs,$uibModal,FileSaver, Blob,config,$crypto) {


$scope.home = function(){
  
    $scope.certptc = JSON.parse(certs).certlist;
    
    if(localStorage.getItem("740a2y1e")!=null)
        {
        var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
        $scope.decry=JSON.parse(decrypted);
        }else
            {
            $window.location.href = '#page';
            }
  

                $scope.certs = $scope.certptc;
               if($scope.certs != undefined)
                  {
                        if($scope.certs == undefined ){
                            $scope.hascertificates = false;
                        }else{
                            var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                            $scope.hascertificates = true;
                          
                            $scope.certs =	$scope.certs.sort(function (x, y) {
                                return x.CDATE - y.CDATE;
                             });
                            for(var i=0; i < $scope.certs.length; i++){
                                var date = new Date($scope.certs[i].CDATE*1000);
                                 var year = date.getFullYear();
                                  var month = months[date.getMonth()];
                                  var date1 = date.getDate();
                                  var hour = date.getHours();
                                  var min = date.getMinutes();
                                  var sec = date.getSeconds();
                                  var time = "Completed on " +  date1 +  ' ' + month +" "+ year;
                                $scope.certs[i].CDATE = time;
                            }
                        }
                   }
            
                $scope.myObj = {'overflow':'hidden','background': 'linear-gradient(to right, #333434 , #337575)'};
                if(localStorage.getItem("fbuser") != null ){
                    $scope.imgsrc =  "https://graph.facebook.com/"+localStorage.getItem("fbid")+"/picture?type=large";
                    
                }else{
                    $scope.imgsrc =  "lib/images/pic.png";
                }
                $scope.username = $scope.decry.username; 
                $scope.ctype = $scope.decry.ctype; 
               
                if($window.innerWidth > 767){
                    $scope.showapppage = false;
                    $scope.showpage = true;
                    
                }else{
                    $scope.showpage = false;
                    $scope.showapppage = true;
                    
                }
                
                $(window).resize(function() {
                      $scope.$apply(function() {
                        $scope.windowWidth = $( window ).width();
                        if($scope.windowWidth < 767){
                            $scope.showpage = false;
                            $scope.showapppage = true;
                             $scope.Instance.close(true);
                        }
                        if($scope.windowWidth > 767 ){
                            $scope.showpage = true;
                            $scope.showapppage = false;
                        }
                      });
                    });
    
    
    $scope.$watch('online', function() {
        if($scope.online == true){
            if($scope.errorpage){
                $scope.loading = true;
                $window.location.href = '#topic';
            }
        }else{
            $scope.loading = false;
        }
     });
    $scope.inprogress = 0;
    $scope.tcompleted = 0;
      
     if(JSON.parse(certs).topics != undefined)
         {
            for(var j=0; j < JSON.parse(certs).topics.Records.length; j++)
            {
                var val = JSON.parse(certs).topics.Records[j].Value;
    
                if(JSON.parse(val).tp.charAt(0) == 2  || JSON.parse(val).tp.charAt(0) == 3)
                    {
                      $scope.tcompleted++;
                    }else
                        {
                        $scope.inprogress++;
                        }
                
            }
         }
    
    window.navigating = false;  
        };

$scope.home();  


$scope.viewcertificate = function(tid){
  if($scope.ctype == 0 || $scope.ctype == '0'){
    if($scope.online == false){
        $scope.othererrmsg = false;
    }
    else{
    $scope.loading = true;
    var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
    $scope.decry=JSON.parse(decrypted);
    $scope.decry["ctopicid"] = tid;
    localStorage.setItem("740a2y1e",$crypto.encrypt(JSON.stringify( $scope.decry), config.key));

        $scope.Instance = $uibModal.open({
        templateUrl: 'cimage.html',
        controller: 'cimageCtrl',
        backdrop: 'static',
        keyboard: false,
        windowClass: 'cimagemodal',
        scope: $scope,
        resolve: {
            cimage: function(getcimage){
                return getcimage.getcimage();
        }
        }
        });
        $scope.Instance.opened.then(function () {
            $scope.loading = false;
        });
         $scope.Instance.result.then(function (quiz) {
             
            }, function () {
            });
    }
  }

};


$scope.downloadcert = function(topicname, TOPICID){
     if($scope.online == false){
        $scope.othererrmsg = false;
    }
    else{
        var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
        $scope.decry=JSON.parse(decrypted);
        
            var fbuser = localStorage.getItem("fbuser");
           AWSCognito.config.region =  config.reg;
           AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
               IdentityPoolId: $scope.decry.iid
           });
          
           var poolData = { UserPoolId : $scope.decry.uid,
                   ClientId : $scope.decry.cid
               };
           
           var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
           
           var cognitoUser = userPool.getCurrentUser();
           
           if (cognitoUser != null && $scope.decry.id != null) {
         
               $scope.getsessioncongnito(cognitoUser,topicname, TOPICID );

           } else if(fbuser != null){
               $scope.getfbsession(topicname, TOPICID);

        }else {
               localStorage.clear();
               $window.location.href = '#page';
           }
}
   
   
};

$scope.getfbsession= function(topicname, TOPICID){
    return new Promise((resolve, reject) => {
       Facebook.getLoginStatus( (response)=> {
          if (response.status == 'connected') {
             var fbjson = JSON.stringify(response);
             fbjson = fbjson.toString();
             var fbtoken = fbjson.accessToken;
             
             if(fbtoken == undefined){
                var fbtoken1 = JSON.parse(fbjson);
                fbtoken = fbtoken1.authResponse.accessToken;
             }
            
             $scope.gettoken(fbtoken, '1',topicname, TOPICID);
             resolve();   
          }else{
                  swal({title: "Oooops!", text: "Facebook session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                  localStorage.clear();
                  $window.location.href = '#page';
          }
          }); 
    })
      
 }
$scope.gettoken = function (token, id,topicname, TOPICID){
    
        if($scope.decry.api === undefined){
            
            var apigClient = apigClientFactory.newClient({
                
            });
        }else{
            
            var apigClient = apigClientFactory.newClient({
                invokeUrl: $scope.decry.api,
            });
        }
      var params1 = {};
      var body = {type: id,token : token,oid : $scope.decry.oid};
      var additionalParams = {};
    
      var abcd = apigClient.getCredentialsWebPost(params1, body, additionalParams)
      .then(function(result1){
         var tjson = JSON.stringify(result1.data);
         tjson = tjson.toString();
         
         tjson = JSON.parse(tjson);
       
      
            $scope.downloadcertficate(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken,topicname, TOPICID ); 
      
              
          }).catch( function(result){
              swal({title: "Oooops!", text: "Session has timed out, Please login again1.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
              localStorage.clear();
              $window.location.href = '#page';
              
          })
 }

$scope.getsessioncongnito = function(cognitoUser,topicname, TOPICID ){
   
   return new Promise((resolve, reject) => {
          cognitoUser.getSession((err, session) =>{
             if (err) {
                 swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                 localStorage.clear();
                 $window.location.href = '#page';
             }else{
                 
                 var token = session.idToken.jwtToken;
                 $scope.downloadcertficate('a', 'a', 'a',topicname, TOPICID ,token); 
                 resolve();  
                /* var apigClient = apigClientFactory.newClient({});
                 var params1 = {};
                 var body = {type: '0',token : token,oid : $scope.decry.oid};
                 var additionalParams = {};
                
                 apigClient.getCredentialsWebPost(params1, body, additionalParams)
                 .then(function(result1){
                    var tjson = JSON.stringify(result1.data);
                    tjson = tjson.toString();
                    
                    tjson = JSON.parse(tjson);
               
                    $scope.downloadcertficate(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken,topicname, TOPICID ); 
                    resolve();   
                     }).catch( function(result){
                         swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                         localStorage.clear();
                         $window.location.href = '#page';
                     })*/
         
             }
         });
     })
}


$scope.downloadcertficate = function(accessKeyId, secretAccessKey, sessionToken,topicname, TOPICID ,token){
    $scope.loading = true;
    if($scope.decry.api === undefined){
        
        var apigClient = apigClientFactory.newClient({
            
        });
    }else{
        
        var apigClient = apigClientFactory.newClient({
            invokeUrl: $scope.decry.api,
        });
    }

  var params = {};

    var body = {
                    eid : $scope.decry.email,
                    oid : $scope.decry.oid,
                    userid : $scope.decry.id,
                    topicid: TOPICID
                 };
   
    var additionalParams = {
            headers: {Authorization : token
            }
      };
   
    var cimage = apigClient.getUserCertPost(params, body, additionalParams)
        .then(function(result){
            
           var json = JSON.stringify(result.data);
            var image = json.toString();
          
           image = JSON.parse(image);
         var image1 = image;
        
           var base64    = ''
                  var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

                  var bytes         = new Uint8Array(image.data)
                  var byteLength    = bytes.byteLength
                  var byteRemainder = byteLength % 3
                  var mainLength    = byteLength - byteRemainder

                  var a, b, c, d
                  var chunk

                  // Main loop deals with bytes in chunks of 3
                  for (var i = 0; i < mainLength; i = i + 3) {
                    // Combine the three bytes into a single integer
                    chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2]

                    // Use bitmasks to extract 6-bit segments from the triplet
                    a = (chunk & 16515072) >> 18 // 16515072 = (2^6 - 1) << 18
                    b = (chunk & 258048)   >> 12 // 258048   = (2^6 - 1) << 12
                    c = (chunk & 4032)     >>  6 // 4032     = (2^6 - 1) << 6
                    d = chunk & 63               // 63       = 2^6 - 1

                    // Convert the raw binary segments to the appropriate ASCII encoding
                    base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d]
                  }

                  // Deal with the remaining bytes and padding
                  if (byteRemainder == 1) {
                    chunk = bytes[mainLength]

                    a = (chunk & 252) >> 2 // 252 = (2^6 - 1) << 2

                    // Set the 4 least significant bits to zero
                    b = (chunk & 3)   << 4 // 3   = 2^2 - 1

                    base64 += encodings[a] + encodings[b] + '=='
                  } else if (byteRemainder == 2) {
                    chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1]

                    a = (chunk & 64512) >> 10 // 64512 = (2^6 - 1) << 10
                    b = (chunk & 1008)  >>  4 // 1008  = (2^6 - 1) << 4

                    // Set the 2 least significant bits to zero
                    c = (chunk & 15)    <<  2 // 15    = 2^4 - 1
                    
                    base64 += encodings[a] + encodings[b] + encodings[c] + '='
                  }
                  var filename = topicname+".png";
                    var blob = b64toBlob(base64, 'image/png'); 
                  var data = new Blob([blob], { type: 'image/png' });
                  $scope.loading = false;
                  $scope.$apply();
                  FileSaver.saveAs(data, filename);
                
            }).catch( function(result){
                $scope.loading = false;
                $scope.$apply();
                var json = JSON.stringify(result);
                var json1 = json.toString();
                //$window.sessionStorage.setItem("json", JSON.stringify(result));
                //alert('ERROR'+json1);
                
                
            });
};

$scope.back = function(){
    $scope.loading = true;
    $window.location.href = '#explore';
}
    
};
app.controller('profileCtrl', profileCtrl);
profileCtrl.$inject = ['$scope', '$http', '$location', '$window','$cookies','certs','$uibModal','FileSaver', 'Blob','config','$crypto'];

app.factory("getcerts", function($window, $q , config,$crypto){
    return {
        getcerts: function(){
            window.navigating = true;
           
            if(localStorage.getItem("740a2y1e") == null || localStorage.getItem("740a2y1e") ==  ''){
                
                $window.location.href = '#page';
                
            }else{
                var decrypted =$crypto.decrypt(localStorage.getItem("740a2y1e"), config.key);               
                var decry=JSON.parse(decrypted);
             
                var certs;
                AWSCognito.config.region =  config.reg;
                AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
                    IdentityPoolId: decry.iid
                });
               var fbuser = localStorage.getItem("fbuser");
                var poolData = { UserPoolId : decry.uid,
                        ClientId : decry.cid
                    };
         
                var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
                
                var cognitoUser = userPool.getCurrentUser();
              
               if (cognitoUser != null && decry.id != null) {
                  
                   certs = getusersession();
                   
                  
                }else if(fbuser != null && decry.id != null){
                    
                    certs = getfbusersession();
                }
                else {
                    localStorage.clear();
                    $window.location.href = '#page';
                }
               
               
               
                  function Credentialsweb(accessKeyId, secretAccessKey, sessionToken,token){
                      if(decry.api === undefined){
                          
                          var apigClient = apigClientFactory.newClient({
                              
                          });
                      }else{
                          
                          var apigClient = apigClientFactory.newClient({
                              invokeUrl: decry.api,
                          });
                      }
                 
                 var params = {};
                
                 var body = {
                        eid: decry.email,
                         oid : decry.oid,
                         iid : decry.iid,
                         id : decry.id
                          };
               
                 var additionalParams = {
                         headers: {Authorization : token
                         }
                   };
               
                      var searchresult1 = apigClient.listUserCertsPost(params, body, additionalParams)
                // var searchresult1 = apigClient.getUserInfoPost(params, body, additionalParams)
                       .then(function(result){
                         
                       
                          var json = JSON.stringify(result.data);
                     
                           var result = json.toString();
                        
                           return $q.when(result);
                               
                           }).catch( function(result){
                               
                               return $q.when(false);
                               
                           });
                       return $q.when(searchresult1);
      
                }
                function getusersession(){
                  

                    return new Promise((resolve, reject) => {
                       cognitoUser.getSession((err, session) =>{
                          if (err) {
                              swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                              localStorage.clear();
                              $window.location.href = '#page';
                          }else{
                              
                              var token = session.idToken.jwtToken;
                              //var abcc = gettoken(token, '0');
                              var abcc = Credentialsweb('a', 'a', 'a',token); 
                              resolve(abcc)
                              return $q.when(abcc);
                      
                          }
                      });
                  })
               }
                function getfbusersession(){
                    return new Promise((resolve, reject) => {
                       Facebook.getLoginStatus( (response)=> {
                          if (response.status == 'connected') {
                             var fbjson = JSON.stringify(response);
                             fbjson = fbjson.toString();
                             var fbtoken = fbjson.accessToken;
                             
                             if(fbtoken == undefined){
                                 var fbtoken1 = JSON.parse(fbjson);
                                fbtoken = fbtoken1.authResponse.accessToken;
                             }
                            
                             var abcc = gettoken(fbtoken, '1');
                              resolve(abcc)
                              return $q.when(abcc);
                          }else{
                                  swal({title: "Oooops!", text: "Facebook session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                                  localStorage.clear();
                                  $window.location.href = '#page';
                          }
                          }); 
                    })
                      
                 }
                function gettoken(token, id){
                    
                    if(decry.api === undefined){
                        
                        var apigClient = apigClientFactory.newClient({
                            
                        });
                    }else{
                        
                        var apigClient = apigClientFactory.newClient({
                            invokeUrl: decry.api,
                        });
                    }
                      var params1 = {};
                      var body = {type: id,token : token,oid : decry.oid};
                      var additionalParams = {};
                    
                      var abcd = apigClient.getCredentialsWebPost(params1, body, additionalParams)
                      .then(function(result1){
                         var tjson = JSON.stringify(result1.data);
                         tjson = tjson.toString();
                         
                         
                         tjson = JSON.parse(tjson);
                        var abc = Credentialsweb(tjson.AccessKeyId, tjson.SecretKey, tjson.SessionToken); 
                        
                          return $q.when(abc);
                              
                          }).catch( function(result){
                              swal({title: "Oooops!", text: "Session has timed out, Please login again.", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fcc917"});
                              localStorage.clear();
                              $window.location.href = '#page';
                              
                          })
                      return $q.when(abcd);
                 }
               
                return  certs; 
               
     
           
           return certs;
        }
        }
    };
});
